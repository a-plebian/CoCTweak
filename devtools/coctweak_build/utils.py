import os, sys, hashlib, string
import requests

from buildtools import log
ALPHABET = string.digits + string.ascii_letters
ALPHALEN = len(ALPHABET)

def b46e(number: int, pad=2) -> str:
    """Converts an integer to a base36 string."""
    if not isinstance(number, int):
        raise TypeError('number must be an integer')

    base46 = ''
    sign = ''

    if number < 0:
        sign = '-'
        number = -number

    if 0 <= number < ALPHALEN:
        base46 = sign + ALPHABET[number]
    else:
        while number != 0:
            number, i = divmod(number, ALPHALEN)
            base46 = ALPHABET[i] + base46

    if pad > 0:
        base46 = base46.rjust(pad, ALPHABET[0])

    return sign + base46

def b46d(number: str) -> int:
    o = 0
    for c in number:
        o *= ALPHALEN
        o += ALPHABET.index(c)
    return o

def base46encode(b: bytes) -> str:
    # 14776335 e1780f ZZZZ
    # So maxlen = 2
    o = ''
    for i in range(0, len(b), 2):
        chunk = ''.join([hex(x)[2:].zfill(2) for x in b[i:i+2]])
        o += b46e(int(chunk, 16), pad=3)
    return o

def base46decode(data: str) -> bytes:
    buff = []
    b = b''
    for i in range(0, len(data), 3):
        chunk = data[i:i+3]
        print(chunk)
        a = bytes(b46d(chunk))
        print(a)
        b += a
    return b

'''
print('0x0000', b46e(0))
print('0xFFFF', b46e(0xFFFF))
print('0', hex(b46d('0')))
print('h31', hex(b46d('h31')))
print(base46encode(bytes([0,1,2,3,4,5])))
print(base46decode('00108j0gB'))

i = 0
while True:
    b46 = base46encode(i)
    h = hex(i)[2:]
    print(i, h, b46)
    if b46 == ALPHABET[-1]*4:
        break
    i += 1
'''

def downloadIfChanged(uri, cache_content=False):
    urlmd5 = base46encode(hashlib.sha256(uri.encode('utf-8')).digest())
    lmpath = os.path.join('.cache', urlmd5+'.lastmod')
    contentpath = os.path.join('.cache', urlmd5+'.content')

    lastmod = ''
    if not os.path.isdir('.cache'):
        os.mkdir('.cache')
    if os.path.isfile(lmpath) and (cache_content and os.path.isfile(contentpath)):
        with open(lmpath,'r') as f:
            lastmod = f.read()
    with log.info('GET %s', uri):
        res = requests.get(uri, headers={'If-Modified-Since':lastmod})
        #with log.info('Response headers:'):
        #    for k,v in res.headers.items():
        #        log.info('%s: %s',k,v)
        if res.status_code == 304:
            log.info('304 Not Modified')
            if not cache_content:
                return None
            with open(contentpath, 'r') as f:
                return f.read()
        else:
            res.raise_for_status()
    with open(lmpath, 'w') as f:
        f.write(res.headers.get('Last-Modified',lastmod))
    if cache_content:
        with open(contentpath, 'w') as f:
            f.write(res.text)
    return res.text
