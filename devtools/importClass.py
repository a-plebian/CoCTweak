import re, sys, os, requests, collections, datetime, yaml
from enum import IntFlag, auto
from typing import List, Callable, Union, Dict, Optional, Any

from buildtools import os_utils, http, log
from buildtools.indentation import IndentWriter
from buildtools.config import YAMLConfig

from coctweak_build.utils import downloadIfChanged

#public var analLooseness:Number = 0;
REG_VAR = re.compile(r'(public|private|protected) var ([A-Za-z0-9_]+):([A-Za-z0-9\.\*]+)(?: *= *([^;]+))?;')
# private static const SERIALIZATION_VERSION:int = 1;
REG_STATIC_CONST = re.compile(r'(public|private|protected) static const ([A-Z0-9_]+):([A-Za-z0-9\.\*]+)(?: *= *([^;]+))?;')
# public function serialize(relativeRootObject:*):void {
REG_SER_FUNC = re.compile(r'public function (serialize|saveToObject)\(([a-zA-Z0-9_]+):\*\):void \{')
# HGG/UEE (sic) public function currentSerializationVerison():int {
REG_SERVER_FUNC = re.compile(r'public function currentSerializationVerison\(\):int(?: {)?')
# UEE: public function serializationUUID():String
REG_SERUUID_FUNC = re.compile(r'public function serializationUUID\(\):String')

assert REG_VAR.match('public var analLooseness:Number = 0;') is not None
ALL_CLASSES = {}
class Variable(object):
    def __init__(self, m = None):
        self.attr: str = ''
        self.id: str = ''
        self.type: str = ''
        self.pytype: str = ''
        self.solname: str = ''
        self.default: str = ''
        self.deserializeMethod: str = ''
        self.value: str = ''
        self.ignore: bool = False
        self.enum: str = ''
        self.useenumval: bool = True
        if m is not None:
            #print(repr(m.groups()))
            self.attr = m[1]
            self.id = m[2]
            self.type = m[3]
            self.value = m[4]
            self.solname = self.id
            self.updateTypeDependants()
            if len(m.groups()) > 4:
                self.default = m[4]

    def updateTypeDependants(self):
        self.pytype = self.type
        self.default = 'None'
        self.deserializeMethod = 'data.get'

        if self.type == 'int':
            self.default = 0
            self.deserializeMethod = 'self._getInt'
        elif self.type == 'String':
            self.default = ''
            self.pytype = 'str'
            self.deserializeMethod = 'self._getStr'
        elif self.type == 'float':
            self.default = 0.
            self.pytype = 'float'
            self.deserializeMethod = 'self._getFloat'
        elif self.type == 'Number':
            self.default = 0.
            self.pytype = 'float'
            self.deserializeMethod = 'self._getFloat'
        elif self.type == 'Boolean':
            self.default = False
            self.pytype = 'bool'
            self.deserializeMethod = 'self._getBool'
        else:
            self.pytype = 'Any'

    def deserialize(self, data: dict) -> None:
        self.attr = data.get('attr', self.attr)
        self.id = data.get('id', self.id)
        self.type = data.get('type', self.type)
        if 'type' in data:
            self.updateTypeDependants()
        self.pytype = data.get('pytype', self.pytype)
        self.solname = data.get('solname', self.solname)
        self.default = data.get('default', self.default)
        self.deserializeMethod = data.get('deserializeMethod', self.deserializeMethod)
        self.value = data.get('value', self.value)
        self.ignore = data.get('ignore', self.ignore)
        self.useenumval = data.get('use-enum-value', self.useenumval)
        self.enum = data.get('enum', self.enum)

    def getInitStr(self):
        c = '#' if self.ignore else ''
        t = self.pytype
        rhs = f'{self.default!r}'
        if self.enum != '':
            t = self.enum
            rhs = f'{t}({rhs})'
        return f'{c}self.{self.id}: {t} = {rhs}'

    def getSerialize(self):
        c = '#' if self.ignore else ''
        rhs = f'self.{self.id}'
        if self.enum != '':
            if self.useenumval:
                rhs = f'{rhs}.value'
            else:
                rhs = f'{rhs}.name'
        return f'{c}data["{self.solname}"] = {rhs}'

    def getDeserialize(self):
        c = '#' if self.ignore else ''
        rhs = f'{self.deserializeMethod}("{self.solname}", {self.default!r})'
        if self.enum != '':
            if self.useenumval:
                rhs = f'{self.enum}({rhs})'
            else:
                rhs = f'{self.enum}[{rhs}]'
        return f'{c}self.{self.id} = {rhs}'

    def getValue(self) -> Any:
        if self.type == 'Number':
            return float(self.value)
        if self.type == 'int':
            return int(self.value)
        if self.type == 'float':
            return float(self.value)
        if self.type == 'String':
            return self.value.strip()[1:-1]
        return self.value


def fetchURL(url) -> requests.Response:
    log.info('Fetching %s...', url)
    res = requests.get(url)
    res.raise_for_status()
    return res

def main():
    import argparse

    cfg = YAMLConfig('class-config.yml')
    for k,v in cfg.cfg.items():
        ALL_CLASSES[k] = raw = RawFile()
        raw.deserialize(k, v)

    failed = False
    with log.info('Parsing...'):
        for raw in ALL_CLASSES.values():
            if not raw.parse():
                failed = True
    if failed:
        return

    with log.info('Writing...'):
        for raw in ALL_CLASSES.values():
            raw.write()

class ERawFlags(IntFlag):
    NONE = 0
    ENDLESS_JOURNEY = auto()
    NO_SER_STAMP = auto()
    USES_SER_VER = auto()
    USES_SER_UUID = auto()

    @staticmethod
    def getMaxLength():
        return 1

    @staticmethod
    def parselist(l: List[str]) -> 'ERawFlags':
        o = ERawFlags.NONE
        for flagname in l:
            o |= ERawFlags[flagname.replace('-','_').upper()]
        return o

    def toList(self) -> List[str]:
        o = []
        for i in range(ERawFlags.getMaxLength()):
            v = ERawFlags(1 << i)
            if (self.value & v.value) != 0:
                o.append(v.name.lower())
        return o


class RawFile(object):
    def __init__(self):
        self.id: str = ''
        self.extends: str = ''
        self.inherits_from: Optional[str] = None
        self.url: str = ''
        self.dest: str = ''
        self.imports: List[Union[str, Dict[str, str]]] = []
        self.replacements: Dict[str, str] = {}
        self.flags: ERawFlags = ERawFlags.NONE
        self.rename_vars: Dict[str, str] = collections.OrderedDict()
        self.oldest_allowable_version: int = 0

        self.inherited: List[str] = []
        self.ignore_vars: List[str] = []
        self.vars: Dict[str, Variable] = collections.OrderedDict()
        self.overwrite_vars: Dict[str, dict] = {}
        self.statics: Dict[str, Variable] = collections.OrderedDict()


    def deserialize(self, k: str, data: dict) -> None:
        self.id = k
        self.extends = data.get('extends', self.extends)
        self.inherits_from = data.get('inherits-from', None)
        self.url = data['url']
        self.imports = data.get('imports', self.imports)
        self.replacements = data.get('replacements', {})
        self.flags = ERawFlags.parselist(data.get('flags', []))
        self.inherited = data.get('inherited', [])
        self.dest = data['dest']
        self.rename_vars = data.get('rename-vars', {})
        self.oldest_allowable_version = data.get('oldest-version', 0)

        self.vars = collections.OrderedDict()
        self.overwrite_vars = data.get('vars', {})

    def parse(self) -> bool:
        url = self.url
        res = ''
        if url.startswith('http://') or url.startswith('https://'):
            res = downloadIfChanged(self.url, cache_content=True)
        else:
            with open(url, 'r') as f:
                self.searchForVars(f)
                return

        with log.info('Parsing code...'):
            self.searchForVars(res)

        success = True
        for k,v in self.overwrite_vars.items():
            if k in self.vars or v.get('new', False):
                if k not in self.vars:
                    self.vars[k] = va = Variable()
                    va.id = va.attr = k
                self.vars[k].deserialize(v)
            else:
                log.error(f'{k} not in vars. {self.vars.keys()!r}')
                success = False
        return success


    def searchForVars(self, res):
        self.vars = collections.OrderedDict()
        inSerializer = False
        inSerVersion = False
        inSerUUID = False
        REG_SER_ASSIGN = None
        def inner(line: str):
            nonlocal inSerializer, inSerUUID, inSerVersion
            nonlocal REG_SER_ASSIGN
            for needle, repl in self.replacements:
                line = line.replace(needle,repl)
            if inSerVersion:
                if 'public function' in line:
                    inSerVersion=False
                    inner(line)
                    return
                if 'return SERIALIZATION_VERSION;' in line:
                    self.flags |= ERawFlags.USES_SER_VER
                return
            elif inSerUUID:
                #print(line)
                if 'public function' in line:
                    inSerUUID=False
                    inner(line)
                    return
                if 'return SERIALIZATION_UUID;' in line:
                    self.flags |= ERawFlags.USES_SER_UUID
                return
            elif inSerializer:
                if 'public function' in line:
                    inSerializer=False
                    inner(line)
                    return
                if REG_SER_ASSIGN is None:
                    return
                m = REG_SER_ASSIGN.search(line)
                if m is not None:
                    #print(m.re.pattern)
                    #print(repr(m.groups()))
                    #print(repr(self.vars.keys()))
                    for varid in (m[2].rstrip('_'), '_'+m[2].rstrip('_')):
                        if varid in self.vars:
                            self.vars[varid].solname = m[1]
                            self.vars[varid].ignore = False
                            return
                    return
            else:
                #public function serialize(relativeRootObject:*):void {
                m = REG_SER_FUNC.search(line)
                if m is not None:
                    inSerializer = True
                    # We have a serializer, make sure all the vars we output are present in it.
                    for var in self.vars.values():
                        var.ignore = True
                    REG_SER_ASSIGN = re.compile(re.escape(m[2])+r'\.([a-zA-Z0-9_]+) *= *this\.([a-zA-Z0-9_]+);')
                    return

                m = REG_SERVER_FUNC.search(line)
                if m is not None:
                    #log.info('FOUND currentSerializationVerison()!')
                    inSerVersion = True
                    return

                #print(line)
                m = REG_SERUUID_FUNC.search(line)
                if m is not None:
                    #log.info('FOUND serializationUUID()!')
                    #log.info(line)
                    inSerUUID = True
                    return

                m = REG_VAR.search(line)
                if m is not None:
                    with log.info(f'Found {m.group(2)}'):
                        _id = origid = m.group(2)
                        if _id.startswith('_'):
                            # Get rid of leading _, since it actually means something in Python.
                            _id = _id.lstrip('_')
                        if _id in self.rename_vars.keys():
                            _id = self.rename_vars[_id]
                        if _id != origid:
                            log.info('Python ID changed to %s', _id)
                        if _id not in self.vars:
                            self.vars[_id] = Variable(m)
                            self.vars[_id].id = _id
                    return

                m = REG_STATIC_CONST.search(line)
                if m is not None:
                    with log.info(f'Found {m.group(2)} (static)'):
                        _id = m.group(2)
                        self.statics[_id]=Variable(m)
                    return
        if isinstance(res, str):
            for bline in res.splitlines():
                inner(bline)
        else:
            for line in res:
                inner(line)
    def write(self) -> None:
        if self.inherits_from is not None:
            inheritee = ALL_CLASSES[self.inherits_from]
            self.inherited += list(inheritee.vars.keys())
            self.rename_vars.update(inheritee.rename_vars)
            for old,new in self.rename_vars.items():
                if old in self.vars:
                    self.vars[new] = self.vars[old]
                    del self.vars[old]
                    self.vars[new].id = new

        with log.info('To %s...', self.dest):
            with open(self.dest, 'w') as f:
                self.toFile(f)

    def toFile(self, f) -> None:
        replacements = []
        for k,v in self.replacements.items():
            replacements += [(k, v)]

        imports = []
        if self.imports is not None:
            for import_spec in self.imports:
                if isinstance(import_spec, dict):
                    # package: target
                    package, imp = next(iter(import_spec.items()))
                    if isinstance(imp, str):
                        imp = [imp]
                    imp = ', '.join(imp)
                    imports += [f'from {package} import {imp}']
                else:
                    imports += [f'import {import_spec}']

        serialization_stamp: Optional[str] = None
        if (self.flags & ERawFlags.NO_SER_STAMP) != ERawFlags.NO_SER_STAMP:
            if self.id.startswith('UEE'):
                if 'SERIALIZATION_UUID' in self.statics and (self.flags & ERawFlags.USES_SER_UUID) == ERawFlags.USES_SER_UUID:
                    imports += [f'from coctweak.saves.uee.serialization import UEESerializationVersion']
                    uuid: str = self.statics['SERIALIZATION_UUID'].getValue()
                    version: str = self.statics['SERIALIZATION_VERSION'].getValue()
                    serialization_stamp = f"SERIALIZATION_STAMP = UEESerializationVersion('{uuid}', {self.oldest_allowable_version}, {version})"
            elif self.id.startswith('HGG'):
                if 'SERIALIZATION_VERSION' in self.statics and (self.flags & ERawFlags.USES_SER_VER) == ERawFlags.USES_SER_VER:
                    imports += [f'from coctweak.saves.hgg.serialization import HGGSerializationVersion']
                    version: int = self.statics['SERIALIZATION_VERSION'].getValue()
                    serialization_stamp = f"SERIALIZATION_STAMP = HGGSerializationVersion(({self.oldest_allowable_version}, {version}))"

        w = IndentWriter(f, indent_chars='    ')
        #w.writeline('# @GENERATED '+datetime.datetime.now().isoformat())
        w.writeline(f'# @GENERATED from {self.url}')
        #w.writeline('from enum import IntEnum')
        for import_spec in imports:
            w.writeline(import_spec)
        w.writeline('')
        w.writeline(f'__ALL__=[\'{self.id}\']')
        w.writeline('')
        extends = ', '.join(self.extends)
        with w.writeline(f'class {self.id}({extends}):'):
            if serialization_stamp is not None:
                w.writeline(serialization_stamp)
            with w.writeline('def __init__(self):'):
                w.writeline('super().__init__()')
                for var in self.vars.values():
                    if var.id not in self.inherited:
                        w.writeline(var.getInitStr())
            with w.writeline('def serialize(self) -> dict:'):
                w.writeline('data = super().serialize()')
                for var in self.vars.values():
                    if var.id not in self.inherited:
                        w.writeline(var.getSerialize())
                w.writeline('return data')
            with w.writeline('def deserialize(self, data: dict) -> None:'):
                w.writeline('super().deserialize(data)')
                for var in self.vars.values():
                    if var.id not in self.inherited:
                        w.writeline(var.getDeserialize())

if __name__ == '__main__':
    main()
