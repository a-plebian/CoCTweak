import os, yaml, json, enum, collections, datetime, requests, re
from typing import List, Any, Optional, Union
from buildtools import os_utils, log
from buildtools.maestro import BuildMaestro
from buildtools.maestro.convert_data import EDataType
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.shell import CommandBuildTarget
from buildtools.maestro.fileio import ReplaceTextTarget

from coctweak_build.utils import downloadIfChanged
class MergeStrategy(enum.IntEnum):
    ADD = enum.auto()
    REPLACE = enum.auto()


def update(d, u):
    for k, v in u.items():
        dv = d.get(k, {})
        if not isinstance(dv, collections.Mapping):
            d[k] = v
        elif isinstance(v, collections.Mapping):
            d[k] = update(dv, v)
        else:
            d[k] = v
    return d

class MergeDataTarget(SingleBuildTarget):
    BT_LABEL = 'MERGE'
    def __init__(self, target: str, files: List[str], datatype: EDataType, dependencies: List[str]=[]):
        self.type: EDataType = datatype
        super().__init__(target, files, dependencies)

    def mergeDict(self, a: dict, b: dict):
        strat = MergeStrategy[b.pop('@merge-strategy', 'add').upper()]
        if strat == MergeStrategy.REPLACE:
            return b
        return update(a, b)
        o = {}
        for k in sorted(list(set(list(a.keys())+list(b.keys())))):
            if k in b and k in a:
                if isinstance(a[k], dict) and isinstance(b[k], dict):
                    o[k] = self.mergeDict(a[k], b[k])
                    continue
                elif isinstance(a[k], list) and isinstance(b[k], list):
                    o[k] = self.mergeList(a[k], b[k])
                    continue
            elif k in a and k not in b:
                o[k]=a[k]
            elif k not in a and k in b:
                o[k]=b[k]
        return o
    def mergeList(self, a: list, b: list):
        strat = MergeStrategy.ADD
        dedupe = True
        sort = True
        newb = []
        for item in b:
            if isinstance(item, dict):
                k,v = list(item.items())[0]
                if k == '@merge-strategy':
                    strat = MergeStrategy(v.upper())
                    continue
                elif k == '@merge-duplicate':
                    dedupe = v
                    continue
                elif k == '@merge-sort':
                    sort = v
                    continue
            newb += [item]
        b = newb
        if strat == MergeStrategy.REPLACE:
            o = []
            alen = len(a)
            blen = len(b)
            maxlen = max(alen, blen)
            minlen = min(alen, blen)
            for i in range(maxlen):
                if minlen >= i:
                    break
                if isinstance(a[i], dict) and isinstance(b[i], dict):
                    o += [self.mergeDict(a[i], b[i])]
                    continue
                if isinstance(a[i], list) and isinstance(b[i], list):
                    o += [self.mergeList(a[i], b[i])]
                    continue
                o += [b[i]]
        elif  strat == MergeStrategy.ADD:
            o = (a+b)
        if dedupe:
            o = list(set(a+b))
        return sorted(o) if sort else o

    def build(self):
        o = {}
        for filename in self.files:
            data = {}
            with open(filename, 'r') as f:
                if filename.endswith('.json'):
                    data = json.load(f)
                if filename.endswith('.yml'):
                    data = yaml.safe_load(f)
            assert isinstance(data, dict), filename
            o = self.mergeDict(o, data)
        with open(self.target, 'w') as f:
            if self.type == EDataType.YAML:
                yaml.dump(o, f, default_flow_style=True, width=200)
            elif self.type == EDataType.JSON:
                json.dump(o, f, separators=(',',':'))

def get_files_in(path: str) -> List[str]:
    return sorted(os_utils.get_file_list(path, prefix=path))

def getHGGVersion():
    reg_version = re.compile(r'public static const saveVersion:String = "hgg ([^"]+)";')
    content = downloadIfChanged('https://gitgud.io/BelshazzarII/CoCAnon_mod/-/raw/master/classes/classes/CoC.as', cache_content=True)
    for line in content.splitlines():
        m = reg_version.search(line)
        if m is not None:
            return m.group(1)
    return '???'

def getUEEVersion():
    reg_version = re.compile(r'ver = "([^"]+)"; //Version NUMBER')
    content = downloadIfChanged('https://raw.githubusercontent.com/Kitteh6660/Corruption-of-Champions-Mod/master/classes/classes/CoC.as', cache_content=True)
    for line in content.splitlines():
        m = reg_version.search(line)
        if m is not None:
            return m.group(1)
    return '???'


bm = BuildMaestro()
ts = datetime.datetime.now(datetime.timezone.utc)
tssimp = ts.strftime('%d%m%Y%H%M%S')
modVersions = {
    'HGG': getHGGVersion(),
    'UEE': getUEEVersion(),
}
bm.add(ReplaceTextTarget(
        target=os.path.join('coctweak', 'consts.py'),
        filename=os.path.join('coctweak', 'consts.py.in'),
        replacements={
            '@@DTINFO@@': f'{ts!r} # {ts.isoformat()}',
            '@@DTINFOSIMP@@': f'{tssimp!r}',
            '@@MODVER@@': f'{modVersions!r}',
        }))



for modid in ['hgg','uee']:
    for subj in ('statuseffects', 'perks', 'items', 'keyitems'):
        files = []
        gamedata = os.path.join('data', modid, f'{subj}.gamedata.json')
        if os.path.isfile(gamedata):
            log.info('Merging Game Data from %s...', gamedata)
            files += [gamedata]
        coctweakdata = os.path.join('data', modid, f'{subj}.d')
        if os.path.isdir(coctweakdata):
            dfiles = get_files_in(coctweakdata)
            log.info('Merging %d files from %s...', len(dfiles), coctweakdata)
            files += dfiles
        bm.add(MergeDataTarget(
            target=os.path.join('data', modid, f'{subj}-merged.min.json'),
            files=files,
            datatype=EDataType.JSON))
bm.as_app()
