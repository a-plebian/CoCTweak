import os, sys, jinja2, pygit2, yaml, argparse, json, collections

from typing import List, Any
from buildtools import os_utils, log, utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget

_env = os_utils.ENV.clone()
cwd = os.getcwd()
SANDBOX_DIR = os.path.join('/tmp', 'coctweak-demo')
SOLLIB_DIR = os.path.join(SANDBOX_DIR, '.macromedia', 'Flash_Player', '#SharedObjects', 'AAAAAAAA', 'localhost')
FAKE_PROJ_DIR = os.path.join(SANDBOX_DIR, 'coctweak')
FAKE_BACKUP_DIR = os.path.join(FAKE_PROJ_DIR, 'backup')
COCTWEAK = './coctweak.cmd' if os_utils.is_windows() else './coctweak.sh'

HASHES={}

jenv = None

def _cmd(cmdline: List[str], echo=False, lang='', comment=None, env=_env, critical=True, globbify=False):
    o = ''
    if echo:
        if comment is not None:
            comment = f'# {comment}\n'
        else:
            comment = ''
        displayed_cmdline = []
        for i in range(len(cmdline)):
            chunk = cmdline[i]
            if chunk == COCTWEAK:
                chunk = 'coctweak'
            displayed_cmdline += [chunk]
        o += f'```shell\n{comment}$ {os_utils._args2str(displayed_cmdline)}\n```\n```{lang}\n'
    with os_utils.Chdir(FAKE_PROJ_DIR):
        o += os_utils.cmd_out(cmdline, True, env, critical, globbify)
    if echo:
        o += '```\n'
    return o

def _reset_slot(slot: Any) -> None:
    os_utils.single_copy(os.path.join(cwd, 'sandbox-seed', f'CoC_{slot}.sol'), SOLLIB_DIR, verbose=True)

def genHelpFrom(srcdir, destdir, basedir, basename):
    if isinstance(basename, list):
        for bn in basename:
            genHelpFrom(srcdir, destdir, basedir, bn)
        return
    outfile = os.path.join(destdir, basedir, f'{basename}.md')
    infile = os.path.join(srcdir, basedir, f'{basename}.template.md')
    md5 = utils.md5sum(infile)

    if os.path.isfile(outfile) and infile in HASHES.keys() and HASHES[infile] == md5:
        log.info('Skipped %s.', outfile)
        return

    with log.info('Writing %s...', outfile):
        with open(outfile, 'w') as f:
            f.write(jenv.get_template(infile).render())

    HASHES[infile] = md5

    with open('.dochashes', 'w') as f:
        yaml.dump(HASHES, f, default_flow_style=False)

class BuildItemTableTarget(SingleBuildTarget):
    BT_LABEL = 'TABLE'

    def __init__(self, target, input_json, template_file, modname, columns):
        super().__init__(target, [input_json, template_file])
        self.input_json = input_json
        self.template_file = template_file
        self.columns = columns
        self.modname = modname

    def build(self):
        data = {}
        with open(self.input_json, 'r') as f:
            data = json.load(f)

        output = {}
        for k in sorted(data.keys()):
            item = data[k]
            cat = item.get('category', 'UNKNOWN')
            if cat not in output:
                output[cat] = {}
            output[cat][k] = []
            for column, opts in self.columns.items():
                stringed = '-'
                typeid = opts.get('type', 'str')
                if typeid in ('str', 'int', 'float', 'number'):
                    stringed = str(item[column])
                elif typeid in ('obj', 'escape'):
                    stringed = json.dumps(item[column], indent=2)
                    if 'format' not in opts:
                        opts['format'] = 'pre'
                else:
                    stringed = f'Unknown column type {typeid!r}'

                if opts.get('prefix', '') != '':
                    stringed = opts['prefix']+stringed
                if opts.get('suffix', '') != '':
                    stringed = stringed+opts['prefix']

                formatting = opts.get('format', '')
                if formatting in ('pre',):
                    stringed = '<pre>'+stringed+'</pre>'
                elif formatting in ('code',):
                    stringed = '<code>'+stringed+'</code>'
                output[cat][k].append(stringed)
        jenv = jinja2.Environment(
            loader=jinja2.FileSystemLoader(['.']),
            autoescape=False,
        )
        template = jenv.get_template(self.template_file)
        os_utils.ensureDirExists(os.path.dirname(self.target))
        with open(self.target, 'w') as f:
            f.write(template.render(FILENAME=self.input_json, MODNAME=self.modname, COLUMNS=self.columns, categories=output))

def main():
    global HASHES, jenv
    argp = argparse.ArgumentParser()
    argp.add_argument('--rebuild', action='store_true', default=False)
    args = argp.parse_args()

    if args.rebuild:
        HASHES={}
    else:
        if os.path.isfile('.dochashes'):
            with open('.dochashes', 'r') as f:
                HASHES=yaml.safe_load(f)


    with log.info('Creating sandbox...'):
        _env.set('HOME', SANDBOX_DIR) #hue
        os_utils.ensureDirExists(SOLLIB_DIR, noisy=True)
        os_utils.ensureDirExists(FAKE_BACKUP_DIR, noisy=True)
        os_utils.copytree('sandbox-seed', SOLLIB_DIR, verbose=True)
        os_utils.copytree('data', os.path.join(FAKE_PROJ_DIR, 'data'), verbose=True)
        os_utils.copytree('coctweak', os.path.join(FAKE_PROJ_DIR, 'coctweak'), verbose=True)
        #os_utils.single_copy('coctweak.py', FAKE_PROJ_DIR, verbose=True)
        os_utils.single_copy('coctweak.sh', FAKE_PROJ_DIR, verbose=True)
        os_utils.single_copy('coctweak.cmd', FAKE_PROJ_DIR, verbose=True)

    jenv = jinja2.Environment(
        loader = jinja2.FileSystemLoader('.'),
        extensions=['jinja2.ext.do'],
        autoescape=False
    )

    jenv.globals.update(cmd=_cmd, reset_slot=_reset_slot, COCTWEAK=COCTWEAK)

    doclist = {}
    with open('doclist.yml', 'r') as f:
        doclist = yaml.safe_load(f)
    genHelpFrom('.', '.', '.', 'README')
    for dirname, children in doclist.items():
        genHelpFrom('docs-src', 'docs', dirname, children)
    #with open('doclist.p.yml', 'w') as f:
    #    yaml.safe_dump(doclist, f, default_flow_style=False)

    log.info('Cleaning up.')
    os_utils.safe_rmtree('/tmp/coctweak-demo')

    bm = BuildMaestro()
    bm.add(BuildItemTableTarget(
        target=os.path.join('docs', 'mods', 'hgg', 'items.md'),
        input_json=os.path.join('data', 'hgg', 'items-merged.min.json'),
        template_file=os.path.join('docs', 'templates', 'items.tmpl.md'),
        modname='HGG',
        columns=collections.OrderedDict({
            #'id': {},
            'const': {'format': 'code'},
            'shortName': {},
            'headerName': {},
            'longName': {},
            'value': {},
            'durability': {},
            'description': {'type': 'escape'},
            'degradable': {},
            'degradesInto': {},
            'bonusStats': {'type': 'obj'},
        })
        ))
    bm.add(BuildItemTableTarget(
        target=os.path.join('docs', 'mods', 'uee', 'items.md'),
        input_json=os.path.join('data', 'uee', 'items-merged.min.json'),
        template_file=os.path.join('docs', 'templates', 'items.tmpl.md'),
        modname='UEE',
        columns=collections.OrderedDict({
            #'id': {},
            'const': {'format': 'code'},
            'shortName': {},
            'longName': {},
            'value': {},
            'durability': {},
            'description': {'type': 'escape'},
            'degradable': {},
            'degradesInto': {},
        })
        ))
    bm.as_app()

if __name__ == '__main__':
    main()
