import re, sys, os, requests, collections, datetime

from typing import List, Callable

from buildtools import os_utils, http, log
from buildtools.indentation import IndentWriter
from buildtools.config import YAMLConfig

from coctweak_build.utils import downloadIfChanged

# public static const UNKNOWN_FLAG_NUMBER_00000:int                                   =    0;
REG_CONST       = re.compile(r'public static (?:const|var) ([A-Za-z0-9_]+):([A-Za-z0-9_]+) += +(\d+);')
REG_CLASSY_ENUM = re.compile(r'public static (?:const|var) ([A-Za-z0-9_]+):([A-Za-z0-9_]+) += +new +\2\(([^\)]+)\);')
REG_ENUM        = re.compile(r'public static (?:const|var) ([A-Za-z0-9_]+):([A-Za-z0-9_]+) += +([^;]+);')

assert REG_CONST.match('public static const UNKNOWN_FLAG_NUMBER_00000:int                                   =    0;') is not None
assert REG_CLASSY_ENUM.match('public static const HUMAN:CockTypesEnum = new CockTypesEnum("human");') is not None
assert REG_ENUM.match('public static const STR:String = "str";') is not None

def searchForConsts(content, args: dict) -> dict:
    flags = collections.OrderedDict()
    conflicts = {}
    ln=0
    for bline in content.splitlines():
        ln += 1
        m = REG_CONST.search(bline)
        if m is not None:
            startswith = args.get('startswith')
            if startswith is None or m.group(1).startswith(startswith):
                s = 0 if startswith is None else len(startswith)
                k = m.group(1)[s:]
                v = m.group(2)
                if k in flags.keys():
                    log.warning('searchForConsts: CONFLICT: line=%d, k=%r, v=%r', ln, k, v)
                flags[k] = v
    return flags

def searchForConstsEnum(content, args: dict) -> dict:
    flags = collections.OrderedDict()
    i = 0
    reg = REG_ENUM if not args.get('type') == 'classy-enum' else REG_CLASSY_ENUM
    for bline in content.splitlines():
        m = reg.search(bline)
        if m is not None:
            name = m[1]
            startswith = args.get('startswith')
            if startswith is not None and name.startswith(startswith):
                s = len(startswith)
                k = name[s:]
                v = str(i) if args.get('ignore-value', False) else m[3]
                if k in flags.keys():
                    log.warning('searchForConsts: CONFLICT: line=%d, k=%r, v=%r', i, k, v)
                flags[k] = v
                i+=1
                continue
            include = args.get('include')
            if include is not None and name in include:
                #print(m[1])
                k = name
                v = str(i) if args.get('ignore-value', False) else m[3]
                if k in flags.keys():
                    log.warning('searchForConsts: CONFLICT: line=%d, k=%r, v=%r', i, k, v)
                flags[k] = v
                i+=1
                continue
            include_all = args.get('include-all', False)
            if include_all:
                #print(m[1])
                k = name
                v = str(i) if args.get('ignore-value', False) else m[3]
                if k in flags.keys():
                    log.warning('searchForConsts: CONFLICT: line=%d, k=%r, v=%r', i, k, v)
                flags[k] = v
                i+=1
                continue
    return flags

def main():
    ALL_ENUMS = YAMLConfig('flag-config.yml')
    for clsID, data in ALL_ENUMS.cfg.items():
        with log.info('%s:', clsID):
            url = data['url']
            destfilename = data['dest']
            content = downloadIfChanged(url, cache_content=True)
            log.info('Generating code...')
            flags = {}
            if data.get('type') in ('classy-enum', 'enum'):
                flags = searchForConstsEnum(content, data)
            else:
                flags = searchForConsts(content, data)
            #print(repr(flags))
            for k,v in data.get('inject', {}).items():
                flags[str(k)]=str(v)
            with open(destfilename, 'w') as f:
                w = IndentWriter(f, indent_chars='    ')
                #w.writeline('# @GENERATED '+datetime.datetime.now().isoformat())
                w.writeline(f'# @GENERATED from {url}')
                superclsspec = 'enum:Enum'
                if data.get('ignore-value', False):
                    superclsspec = 'enum:IntEnum'
                superclsspec = data.get('superclass', superclsspec)
                imports = data.get('imports', [])
                for import_spec in [superclsspec]+imports:
                    entry = import_spec.split(':')
                    if len(entry) == 1:
                        imports += [f'import {entry[0]}']
                    elif len(entry) >= 2:
                        if ',' in entry[1]:
                            entry += [x.strip() for x in entry[1].split(',')]
                        if len(entry) > 2:
                            entry[1] = ', '.join(entry[1:])
                            entry[1] = f'({entry[1]})'
                        w.writeline(f'from {entry[0]} import {entry[1]}')
                w.writeline('')
                w.writeline(f"__ALL__ = ['{clsID}']")
                w.writeline('')
                supercls = superclsspec.split(':')[-1]
                with w.writeline(f'class {clsID}({supercls}):'):
                    maxlen = max([len(x) for x in flags.keys()])
                    for k, v in flags.items():
                        padding = ' ' * (maxlen + 2 - len(k))
                        w.writeline(f'{k}{padding}= {v}')

if __name__ == '__main__':
    main()
