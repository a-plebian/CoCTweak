import os, sys, re
from buildtools import os_utils, log

for entry in os_utils.get_file_list('docs'):
    if not entry.endswith('.template.md'):
        continue

    os_utils.ensureDirExists(os.path.dirname(os.path.join('docs-src', entry)), noisy=True)
    os_utils.single_copy(os.path.join('docs', entry), os.path.join('docs-src', entry), verbose=True)
    os.remove(os.path.join('docs', entry))
