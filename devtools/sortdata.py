import json, os
from collections import OrderedDict
def sortJSON(filename):
    print(f'Fixing {filename}')
    data: dict = None
    with open(filename, 'r') as f:
        data = json.load(f)
    with open(filename, 'w') as f:
        json.dump(data, f, sort_keys=True, indent=2)

def main():
    for root, _, files in os.walk('data'):
        for basefilename in files:
            if basefilename.endswith('.gamedata.json'):
                sortJSON(os.path.join(root, basefilename))
if __name__ == '__main__':
    main()
