import re, sys, os
from buildtools import cmd
def editInPlace(regex, replacement, filename):
    content = ''
    with open(filename, 'r') as f:
        content = f.read()
    content = re.sub(regex, replacement, content)
    with open(filename, 'w') as f:
        f.write(content)

NEWVERSION = sys.argv[1]
assert len(NEWVERSION) > 0

editInPlace(r"VERSION = Version\('[^']+'\)", f"VERSION = Version('{NEWVERSION}')", os.path.join('coctweak','consts.py.in'))
editInPlace(r"version='[^']+',", f"version='{NEWVERSION}',", 'setup.py')

cmd(['./BUILD.sh'], echo=True, show_output=True, critical=True)
cmd([sys.executable, 'devtools/buildDocs.py', '--rebuild'], echo=True, show_output=True, critical=True)
