import argparse
import logging

import os, sys, yaml, shutil, string

from coctweak.cmd import register_parsers
from coctweak.consts import NAME, VERSION, GENERATED_AT, GENERATED_AT_SIMPLIFIED, MOD_SUPPORT
from coctweak.logging import defaultSetup, verboseSetup

def main():
    argp = argparse.ArgumentParser('coctweak', description=f'{NAME} Save Editor v{VERSION}-{GENERATED_AT_SIMPLIFIED}')

    argp.add_argument('--quiet', '-q', action='store_true', default=False, help='Silence information-level messages.')
    argp.add_argument('--verbose', '-v', action='store_true', default=False, help='More verbose logging.')
    argp.add_argument('--debug', action='store_true', default=False, help='Display debug-level messages.')

    subp = argp.add_subparsers()

    register_parsers(subp)

    args = argp.parse_args()
    if getattr(args, 'cmd', None) is None:
        argp.print_help()
        print()
        print(f'Built at: {GENERATED_AT.isoformat()}')
        print('Against:')
        for modID, modVer in MOD_SUPPORT.items():
            print(' -',modID, modVer)
        sys.exit(1)

    if not args.verbose:
        defaultSetup(logging.DEBUG if args.debug else logging.INFO)
    else:
        verboseSetup(logging.DEBUG if args.debug else logging.INFO)
    args.cmd(args)

if __name__ == '__main__':
    main()
