from coctweak.saves._perk import Perk

class BeeOvipositorPerk(Perk):
    def displayValues(self):
        return f'Unfertilized: {self.values[0]}, Fertilized: {self.values[1]}'
