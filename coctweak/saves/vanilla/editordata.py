from coctweak.saves._coctweak_data import CoCTweakData

class VanillaCoCTweakData(CoCTweakData):
    FLAG_ID = 2999 # https://github.com/OXOIndustries/Corruption-of-Champions/blob/master/classes/classes/DefaultDict.as#L47
