# @GENERATED from https://raw.githubusercontent.com/OXOIndustries/Corruption-of-Champions/master/classes/classes/VaginaClass.as
from coctweak.saves._saveobj import SaveObject

__ALL__=['BaseVagina']

class BaseVagina(SaveObject):
    def __init__(self):
        super().__init__()
        self.vaginalWetness: float = 0.0
        self.vaginalLooseness: float = 0.0
        self.type: int = 0
        self.virgin: bool = False
        self.fullness: float = 0.0
        self.labiaPierced: float = 0.0
        self.labiaPShort: str = ''
        self.labiaPLong: str = ''
        self.clitPierced: float = 0.0
        self.clitPShort: str = ''
        self.clitPLong: str = ''
    def serialize(self) -> dict:
        data = super().serialize()
        data["vaginalWetness"] = self.vaginalWetness
        data["vaginalLooseness"] = self.vaginalLooseness
        data["type"] = self.type
        data["virgin"] = self.virgin
        data["fullness"] = self.fullness
        data["labiaPierced"] = self.labiaPierced
        data["labiaPShort"] = self.labiaPShort
        data["labiaPLong"] = self.labiaPLong
        data["clitPierced"] = self.clitPierced
        data["clitPShort"] = self.clitPShort
        data["clitPLong"] = self.clitPLong
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.vaginalWetness = self._getFloat("vaginalWetness", 0.0)
        self.vaginalLooseness = self._getFloat("vaginalLooseness", 0.0)
        self.type = self._getInt("type", 0)
        self.virgin = self._getBool("virgin", False)
        self.fullness = self._getFloat("fullness", 0.0)
        self.labiaPierced = self._getFloat("labiaPierced", 0.0)
        self.labiaPShort = self._getStr("labiaPShort", '')
        self.labiaPLong = self._getStr("labiaPLong", '')
        self.clitPierced = self._getFloat("clitPierced", 0.0)
        self.clitPShort = self._getStr("clitPShort", '')
        self.clitPLong = self._getStr("clitPLong", '')
