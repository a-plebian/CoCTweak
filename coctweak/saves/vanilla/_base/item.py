# @GENERATED from https://raw.githubusercontent.com/OXOIndustries/Corruption-of-Champions/master/classes/classes/ItemSlotClass.as
from coctweak.saves._saveobj import SaveObject

__ALL__=['BaseItem']

class BaseItem(SaveObject):
    def __init__(self):
        super().__init__()
        self.quantity: int = 0
        #self.itype: Any = 'None'
        self.unlocked: bool = False
        self.id: str = 'NOTHING!'
    def serialize(self) -> dict:
        data = super().serialize()
        data["quantity"] = self.quantity
        #data["_itype"] = self.itype
        data["unlocked"] = self.unlocked
        data["id"] = self.id
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.quantity = self._getInt("quantity", 0)
        #self.itype = data.get("_itype", 'None')
        self.unlocked = self._getBool("unlocked", False)
        self.id = self._getStr("id", 'NOTHING!')
