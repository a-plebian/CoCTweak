# @GENERATED from https://raw.githubusercontent.com/OXOIndustries/Corruption-of-Champions/master/classes/classes/AssClass.as
from coctweak.saves._saveobj import SaveObject

__ALL__=['BaseAss']

class BaseAss(SaveObject):
    def __init__(self):
        super().__init__()
        self.wetness: float = 0.0
        self.looseness: float = 0.0
        self.fullness: float = 0.0
        #self.rating: int = 0
    def serialize(self) -> dict:
        data = super().serialize()
        data["analWetness"] = self.wetness
        data["analLooseness"] = self.looseness
        data["fullness"] = self.fullness
        #data["buttRating"] = self.rating
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.wetness = self._getFloat("analWetness", 0.0)
        self.looseness = self._getFloat("analLooseness", 0.0)
        self.fullness = self._getFloat("fullness", 0.0)
        #self.rating = self._getInt("buttRating", 0)
