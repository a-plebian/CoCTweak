# @GENERATED from https://raw.githubusercontent.com/OXOIndustries/Corruption-of-Champions/master/classes/classes/Cock.as
from coctweak.saves._saveobj import SaveObject

__ALL__=['BaseCock']

class BaseCock(SaveObject):
    def __init__(self):
        super().__init__()
        self.length: float = 0.0
        self.thickness: float = 0.0
        self.type: self.COCK_TYPES = self.COCK_TYPES(0)
        self.knotMultiplier: float = 0.0
        #self.isPierced: bool = False
        self.pierced: float = 0.0
        #self.pierceType: Any = 'None'
        self.pShortDesc: str = ''
        self.pLongDesc: str = ''
        self.sock: str = ''
    def serialize(self) -> dict:
        data = super().serialize()
        data["cockLength"] = self.length
        data["cockThickness"] = self.thickness
        data["cockType"] = self.type.value
        data["knotMultiplier"] = self.knotMultiplier
        #data["_isPierced"] = self.isPierced
        data["pierced"] = self.pierced
        #data["_pierceType"] = self.pierceType
        data["pShortDesc"] = self.pShortDesc
        data["pLongDesc"] = self.pLongDesc
        data["sock"] = self.sock
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.length = self._getFloat("cockLength", 0.0)
        self.thickness = self._getFloat("cockThickness", 0.0)
        self.type = self.COCK_TYPES(self._getInt("cockType", 0))
        self.knotMultiplier = self._getFloat("knotMultiplier", 0.0)
        #self.isPierced = self._getBool("_isPierced", False)
        self.pierced = self._getFloat("pierced", 0.0)
        #self.pierceType = data.get("_pierceType", 'None')
        self.pShortDesc = self._getStr("pShortDesc", '')
        self.pLongDesc = self._getStr("pLongDesc", '')
        self.sock = self._getStr("sock", '')
