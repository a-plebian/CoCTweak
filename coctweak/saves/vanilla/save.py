from coctweak.saves._save import BaseSave
from coctweak.saves.vanilla.corestats import VanillaCoreStats
from coctweak.saves.vanilla.editordata import VanillaCoCTweakData
class VanillaSave(BaseSave):
    CORE_STATS_TYPE = VanillaCoreStats
    NAME = 'Vanilla'
    GLOBALDATA_TYPE = None
    COCTWEAKDATATYPE = VanillaCoCTweakData
    def getDifficulty(self):
        return 'N/A'

    def getMaxHP(self):
        # https://gitgud.io/BelshazzarII/CoCAnon_mod/blob/master/classes/classes/Creature.as#L4709
        # max += int(tou * 2 + 50);
        m = self.stats.toughness * 2 + 50
        #if (findPerk(PerkLib.Tank) >= 0) max += 50;
        if 'Tank' in self.perks:
            m += 50
        #if (findPerk(PerkLib.Tank2) >= 0) max += Math.round(tou);
        if 'Tank 2' in self.perks:
            m += round(self.stats.toughness)
        #if (findPerk(PerkLib.Tank3) >= 0) max += level * 5;
        if 'Tank 3' in self.perks:
            m += self.level * 5

        #max += level * 15;
        m += self.level * 15
        #max += getBonusStat(BonusDerivedStats.maxHealth);
        m += self.getBonusStat('Max Health')
		#if (findPerk(PerkLib.ChiReflowDefense) >= 0) max += UmasShop.NEEDLEWORK_DEFENSE_EXTRA_HP;
        if self.perks.get('Chi Reflow - Defense', -1) >= 0:
            m += 50

        #if (jewelryEffectId == JewelryLib.MODIFIER_HP) max += jewelryEffectMagnitude;
        # Can't directly translate ATM.

        #max *= getBonusStatMultiplicative(BonusDerivedStats.maxHealth);
        #if (hasStatusEffect(St
        return m

    def serialize(self):
        data = super().serialize()
        data['version'] = self.version
        return data

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.version = data['version']
