from coctweak.saves._statuseffect import StatusEffect

class VanillaButtStretchedEffect(StatusEffect):
    def displayValues(self):
        return f'Hrs Since Stretched: {self.values[0]}'
