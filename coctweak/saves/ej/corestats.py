from coctweak.saves._corestats import BaseCoreStats
from coctweak.saves.ej.stats import EJPrimaryStat, EJStatNames, EJBuffableStat

# TEN BILLION FUCKING STATS AND A NIGHTMARE OF OOP
class EJCoreStats(BaseCoreStats):
    def __init__(self, save):
        super().__init__(save)

    def init(self):
        # https://github.com/Oxdeception/Corruption-of-Champions/blob/master/classes/classes/Creature.as#L138
        # _stats:StatScore = new StatStore({ ...
        self.strength     = self.addPrimary(EJStatNames.STR, 'Strength')
        self.toughness    = self.addPrimary(EJStatNames.TOU, 'Toughness')
        self.speed        = self.addPrimary(EJStatNames.SPE, 'Speed')
        # These two differ from other mods.
        self.intelligence = self.addPrimary(EJStatNames.INT, 'Intelligence')
        self.wisdom       = self.addPrimary(EJStatNames.WIS, 'Wisdom')
        self.libido       = self.addPrimary(EJStatNames.LIB, 'Libido')

        # WHY.
        self.sens_min    = self.addBuffable(EJStatNames.SENS_MIN,    'Sensitivity Minimum', base=10)
        self.sens_max    = self.addBuffable(EJStatNames.SENS_MAX,    'Sensitivity Maximum')
        self.lust_min    = self.addBuffable(EJStatNames.LUST_MIN,    'Lust Minimum')
        self.lust_max    = self.addBuffable(EJStatNames.LUST_MAX,    'Lust Maximum')
        self.hp_max      = self.addBuffable(EJStatNames.HP_MAX,      'HP Maximum',          base=50)
        self.stamina_max = self.addBuffable(EJStatNames.STAMINA_MAX, 'Stamina Maximum',     base=100)
        self.ki_max      = self.addBuffable(EJStatNames.KI_MAX,      'Ki Maximum',          base=50)

        self.defense        = self.addBuffable(EJStatNames.DEFENSE,       'Defense')
        self.spellpower     = self.addBuffable(EJStatNames.SPELLPOWER,    'Spellpower',       base=1.0, min=0.0)
        self.hp_per_tou     = self.addBuffable(EJStatNames.HP_PER_TOU,    'HP Per Toughness', base=2,   min=0.1)
        self.attack_rating  = self.addBuffable(EJStatNames.ATTACK_RATING, 'Attack Rating')
        self.defense_rating = self.addBuffable(EJStatNames.ATTACK_RATING, 'Defense Rating')

    def addPrimary(self, _id: str, name: str) -> EJPrimaryStat:
        return self._add(EJPrimaryStat(_id, name))

    def addBuffable(self, _id: str, name: str, **kwargs) -> EJBuffableStat:
        return self._add(EJBuffableStat(_id, name, **kwargs))

    def deserialize(self, data: dict):
        self.raw = data

        for stat in self.all:
            stat.deserialize(data)

    def serialize(self):
        data = self.raw

        for stat in self.all:
            data = stat.serialize(data)

        return data
