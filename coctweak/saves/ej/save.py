from coctweak.saves._save import BaseSave
from coctweak.saves.ej.corestats import EJCoreStats
from coctweak.saves.ej.kflags import EJKFlags
from coctweak.saves.ej.editordata import EJCoCTweakData

class EJSave(BaseSave):
    CORE_STATS_TYPE = EJCoreStats
    NAME = 'EJ'

    ENUM_FLAGS = EJKFlags

    COCTWEAKDATATYPE = EJCoCTweakData

    def __init__(self):
        super().__init__()

        #self.hunger = 0

    def getDifficulty(self):
        difficulty = self.flags.get(EJKFlags.GAME_DIFFICULTY)
        ezmode = self.flags.get(EJKFlags.EASY_MODE_ENABLE_FLAG) == 1
        if difficulty is None or difficulty == 0:
            return 'Easy' if ezmode else 'Normal'
        elif difficulty == 1:
            return 'Hard'
        elif difficulty == 2:
            return 'Nightmare'
        elif difficulty == 3:
            return 'EXTREME'

    def getBaseStatStrs(self):
        return { k.name.upper():v.value for k,v in self.stats.allByID.items() }

    # def serialize(self):
    #     data = super().serialize()
    #     data['hunger'] = self.hunger
    #     return data
    #
    # def deserialize(self, data: dict) -> None:
    #     super().deserialize(data)
    #
    #     self.hunger = self._getInt('hunger')
