from coctweak.saves._saveobj import SaveObject
from typing import List, Any, Optional

from coctweak.logging import getLogger
log = getLogger(__name__)

class CoCTweakData(SaveObject):
    '''
    This all gets crammed into a flag.
    '''
    VERSION = 1
    FLAG_ID = 0
    def __init__(self):
        super().__init__()
        self.unlock_inventory_slots: Optional[int] = None
        self.backup_backpack_size: Optional[int] = None

    def deserialize(self, data) -> None:
        super().deserialize(data)
        self.unlock_inventory_slots = self._getInt('unlock_inventory_slots', None)
        self.backup_backpack_size = self._getInt('backup_backpack_size', None)

    def serialize(self) -> dict:
        ds = {'version':self.VERSION}
        if self.unlock_inventory_slots is not None:
            ds['unlock_inventory_slots'] = self.unlock_inventory_slots
        if self.backup_backpack_size is not None:
            ds['backup_backpack_size'] = self.backup_backpack_size
        return ds

    def show(self) -> None:
        log.info('Schema version: %d', self.VERSION)
        with log.info('Inventory hacks:'):
            log.info('Unlocked inventory slots: %s', 'None (Stock logic)' if not self.unlock_inventory_slots else self.unlock_inventory_slots)
            log.info('Backed-up backpack size: %s', 'None (Not set)' if not self.backup_backpack_size else self.backup_backpack_size)
        return
