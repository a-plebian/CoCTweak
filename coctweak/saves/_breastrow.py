from coctweak.saves._saveobj import SaveObject
from coctweak.logging import getLogger
from coctweak.saves._base.breastrow import BaseBreastRow
log = getLogger(__name__)

class BreastRow(BaseBreastRow):
    def __init__(self):
        super().__init__()

    def display(self):
        with log.info(f'{self.breasts:g}x w/ {self.nipplesPerBreast:g} nips per breast'):
            o = self.getDisplayEntries()
            o = [f'{k}: {v}' for k,v in o.items()]
            log.info(', '.join(o))

    def getDisplayEntries(self) -> dict:
        o = {'Rating': f'{self.breastRating:g}'}
        if self.fullness > 0:
            o['Fullness'] = f'{self.fullness:g}'
        if self.milkFullness > 0.0:
            o['Milk Fullness'] = f'{self.milkFullness:g}'
        if self.lactationMultiplier > 0:
            o['Lactation Mult'] = f'{self.lactationMultiplier:g}'
        if self.fuckable:
            o['Fuckable'] = 'yes'
        return o
