from typing import Type
from enum import IntEnum

from coctweak.saves._saveobj import SaveObject

from coctweak.logging import getLogger
log = getLogger(__name__)

class Vagina(SaveObject):
    '''
    https://github.com/OXOIndustries/Corruption-of-Champions/blob/master/classes/classes/VaginaClass.as

    --inherited vaginalWetness vaginalLooseness type virgin fullness labiaPierced labiaPShort labiaPLong clitPierced clitPShort clitPLong
    '''
    ENUM_LOOSENESS: Type[IntEnum] = None
    ENUM_WETNESS: Type[IntEnum] = None
    ENUM_TYPES: Type[IntEnum] = None

    def getLoosenessStr(self):
        if self.ENUM_LOOSENESS is not None:
            return self.ENUM_LOOSENESS(self.looseness).name
        else:
            return f'{self.looseness:g} looseness'

    def getWetnessStr(self):
        if self.ENUM_WETNESS is not None:
            return self.ENUM_WETNESS(self.wetness).name
        else:
            return f'{self.wetness:g} wetness'

    def display(self):
        desc = [
            self.getLoosenessStr(),
            self.getWetnessStr()
        ]
        if self.virgin:
            desc += ['virgin']
        desc = [', '.join(desc) + ' vagina']
        if self.fullness > 0.0:
            desc += [f'with fullness={self.fullness}']
        log.info(', '.join(desc))

    def __init__(self):
        super().__init__()
        self.wetness: int = 0
        self.looseness: int = 0
        self.type: int = 0
        self.virgin: bool = False
        self.fullness: float = 0.0
        self.labiaPierced: float = 0.0
        self.labiaPShort: str = ''
        self.labiaPLong: str = ''
        self.clitPierced: float = 0.0
        self.clitPShort: str = ''
        self.clitPLong: str = ''

    def serialize(self):
        data = super().serialize()
        data["vaginalWetness"] = int(self.wetness)
        data["vaginalLooseness"] = int(self.looseness)
        data["type"] = int(self.type)
        data["virgin"] = bool(self.virgin)
        data["fullness"] = float(self.fullness)
        data["labiaPierced"] = float(self.labiaPierced)
        data["labiaPShort"] = self.labiaPShort
        data["labiaPLong"] = self.labiaPLong
        data["clitPierced"] = float(self.clitPierced)
        data["clitPShort"] = self.clitPShort
        data["clitPLong"] = self.clitPLong
        return data

    def deserialize(self, data):
        super().deserialize(data)
        self.wetness = self._getInt("vaginalWetness", 0)
        self.looseness = self._getInt("vaginalLooseness", 0)
        self.type = self._getInt("type", 0)
        self.virgin = self._getBool("virgin", False)
        self.fullness = self._getFloat("fullness", 0.0)
        self.labiaPierced = self._getFloat("labiaPierced", 0.0)
        self.labiaPShort = self._getStr("labiaPShort", '')
        self.labiaPLong = self._getStr("labiaPLong", '')
        self.clitPierced = self._getFloat("clitPierced", 0.0)
        self.clitPShort = self._getStr("clitPShort", '')
        self.clitPLong = self._getStr("clitPLong", '')
