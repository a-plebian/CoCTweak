from coctweak.saves._saveobj import SaveObject
from coctweak.logging import getLogger
log = getLogger(__name__)

class BreastController(SaveObject):
    '''
    This is currently only used for Kath's boobs.

    Fen, if you're heading this: *headslap*
    '''
    READER_VERSION = 1

    def __init__(self, flag:int):
        self.flag = flag

        self.version = 1
        self.rows = -1
        self.cupSize = 0
        self.lactationLevel = 0
        self.nippleLength = 0
        self.fullness = 0
        self.timesMilked = 0
        self.preventLactationIncrease = 0
        self.preventLactationDecrease = 0

        self.__readCleanly = True

    def deserialize(self, data):
        #game.flags[_breastFlag] = BREAST_STORE_VERSION_1 + "^" + rows + "^" + cupSize + "^" + lactationLevel + "^" + nippleLength + "^" + _fullness + "^" + _timesMilked + "^" + preventLactationIncrease + "^" + preventLactationDecrease;
        # *sob*
        self.raw = data['flags'][self.flag]

        self.__readCleanly = True
        if not data.startswith(f'{self.READER_VERSION}^'):
            with log.error(f'BreastController: Failed to read flag {self.flag} (does not start with "{self.READER_VERSION}^").'):
                log.error(f'Data for this flag will not be processed or modified, in order to prevent save corruption.')
            self.__readCleanly = False
            return

        chunks = self.raw.split('^')

        self.rows = int(chunks[1])
        self.cupSize = int(chunks[2])
        self.lactationLevel = int(chunks[3])
        self.nippleLength = float(chunks[4])
        self.fullness = int(chunks[5])
        self.timesMilked = int(chunks[6])
        self.preventLactationIncrease = int(chunks[7])
        self.preventLactationDecrease = int(chunks[8])

    def serialize(self):
        if not self.__readCleanly:
            return self.raw
        else:
            o=[
                self.READER_VERSION,
                self.rows,
                self.lactationLevel,
                f'{self.nippleLength:g}',
                self.fullness,
                self.timesMilked,
                self.preventLactationIncrease,
                self.preventLactationDecrease
            ]
            return '^'.join([str(x) for x in o])
