import json
from typing import List
from coctweak.saves._quartet import QuartetWithMeta

from coctweak.logging import getLogger
log = getLogger(__name__)


class StatusEffect(QuartetWithMeta):
    ID_KEY = 'statusAffectName' #sic

    FLAG_EXPLAINS = {
        'dispellable': 'Dispellable',
        'monster-dispellable': 'Monsters can dispel',
        'merge-flags': None,
        'fully-mapped': 'Full support by CoCTweak',
        'combat': 'Combat ability - You should not see this.',
        'spell': 'Spell',
        'spell-white': 'White Magic',
        'spell-black': 'Black Magic',
        'spell-gray': 'Gray Magic',
        'spell-other': 'Unknown Magic',
        'values-unused': 'Values Unused',
        'coctweak-internal': 'COCTWEAK INTERNAL USE ONLY'
    }

    def isParasitic(self) -> bool:
        return 'parasite' in self.flags

    def isDetrimental(self) -> bool:
        for f in ('combat', 'monster-dispellable'):
            if f in self.flags:
                return True
        return False

    def __init__(self):
        super().__init__()
        self.dataStore = None

    def serialize(self) -> dict:
        o = super().serialize()
        if self.dataStore is not None:
            o['dataStore'] = self.dataStore
        return o

    def deserialize(self, data):
        super().deserialize(data)
        self.dataStore = data.get('dataStore', None)

    def preRemove(self, save) -> None:
        return

    def displayValues(self) -> str:
        o = super().displayValues()
        if self.dataStore is not None:
            o += f' {self.dataStore!r}'
        return o

    def longPrintValues(self) -> None:
        super().longPrintValues()
        if self.dataStore is not None:
            with log.info('Data Store:'):
                log.info(json.dumps(self.dataStore, indent=2))

    def __str__(self):
        o = f'{self.id!r}: {self.values!r}'
        if self.dataStore is not None:
            o += f' {self.dataStore!r}'
        return o
