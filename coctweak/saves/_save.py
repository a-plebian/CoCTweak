import json, os, miniamf, enum, yaml

from enum import IntFlag, auto

from typing import Optional, List, Dict

from coctweak.utils import sanitizeFilename, display_length

from coctweak.logging import getLogger

from coctweak.saves._ass import Ass
from coctweak.saves._bonus_derived_stats import BDSController
from coctweak.saves._breastrow import BreastRow
from coctweak.saves._cock import Cock
from coctweak.saves._coctweak_data import CoCTweakData
from coctweak.saves._combatstats import BaseCombatStats
from coctweak.saves._globaldata import GlobalData
from coctweak.saves._item import Item
from coctweak.saves._keyitem import KeyItem
from coctweak.saves._kflagstore import KFlagStore
from coctweak.saves._parasite import Parasite
from coctweak.saves._perk import Perk
from coctweak.saves._pregnancy import ButtPregnancy, VaginalPregnancy
from coctweak.saves._saveobj import SaveObject
from coctweak.saves._statuseffect import StatusEffect
from coctweak.saves._storage import GearStorage, Storage
from coctweak.saves._vagina import Vagina
from coctweak.saves._wearableslot import WearableSlot
#from coctweak.saves.inventory_wrapper import InventoryWrapper

from coctweak.saves.vanilla.corestats import VanillaCoreStats
from coctweak.saves.vanilla.kflags import VanillaKFlags

from coctweak.saves.vanilla.perks import *
from coctweak.saves.vanilla.statuseffects import *

from coctweak.logging import getLogger
log = getLogger(__name__)

class SaveCaps(IntFlag):
    NONE = 0

    HUNGER = auto()
    PARASITES = auto()

class BaseSave(SaveObject):
    CORE_STATS_TYPE = VanillaCoreStats
    COMBAT_STATS_TYPE = BaseCombatStats
    GLOBAL_DATA_TYPE = None
    GLOBAL_DATA_FILENAME = None
    NAME = '__BASE__'
    PERK_CLASSES = {
        'Bee Ovipositor': BeeOvipositorPerk,
        'Smart': SmartPerk,
    }
    STATUS_EFFECT_CLASSES = {
        'ButtStretched': VanillaButtStretchedEffect
    }
    KEYITEM_CLASSES = {
    }

    CAPABILITIES = SaveCaps.NONE

    ASS_TYPE  = Ass
    COCK_TYPE = Cock
    VAGINA_TYPE = Vagina
    #BREASTSTORE_TYPE = BreastController
    BREASTROW_TYPE = BreastRow
    ITEM_TYPE = Item
    KEYITEM_TYPE = KeyItem
    STATUSEFFECT_TYPE = StatusEffect
    WEARABLESLOT_TYPE = WearableSlot

    PREGNANCY_TYPE = VaginalPregnancy
    BUTTPREGNANCY_TYPE = ButtPregnancy

    ENUM_FLAGS = VanillaKFlags

    MAX_INVENTORY_SLOTS = 10
    SLOT_QUANTITY_HARD_CAP = 10

    ALL_ITEMS: dict = None
    ITEMTYPES_FILE = None

    ALL_PERKS: dict = None
    PERKTYPES_FILE = None

    ALL_KEYITEMS: dict = None
    KEYITEMS_FILE = None

    ALL_STATUSEFFECTS: dict = None
    STATUSEFFECTTYPES_FILE = None
    STATUSEFFECTS_DIR = None

    BDS_CONTROLLER = BDSController

    BAD_STATUSES = []

    PARASITES: List[Parasite] = []

    COCTWEAKDATATYPE = CoCTweakData

    INVENTORY_STORE = Storage
    GEAR_STORE = GearStorage

    def __init__(self):
        super().__init__()

        self._editorData: Optional[CoCTweakData] = None

        self.filename = ''
        self.host = ''
        self.id = 0

        self.version = ''

        self.saveName = ''

        self.sexOrientation = 0

        self.gems: int = 0

        self.days = 0
        self.hours = 0
        self.minutes = 0

        self.stats = self.CORE_STATS_TYPE(self)
        self.combat_stats = self.COMBAT_STATS_TYPE(self)

        self.stat_points: int = 0
        self.perk_points: int = 0

        self.notes = ''

        self.shortName = ''
        self.startingRace = 'human'

        self.flags: KFlagStore = KFlagStore(self.ENUM_FLAGS)

        self.perks = {}
        self.keyItems = {}
        self.statusEffects = {}

        self.ass = self.ASS_TYPE()
        self.breast_rows = [self.BREASTROW_TYPE()]
        self.cocks = []
        self.vaginas = []

        self.cumMultiplier = 0
        self.balls = 0
        self.ballSize = 0

        self.global_data = None

        self.pregnancy = self.PREGNANCY_TYPE()
        self.butt_pregnancy = self.BUTTPREGNANCY_TYPE()

        # https://gitgud.io/BelshazzarII/CoCAnon_mod/blob/master/classes/classes/Saves.as: //CLOTHING/ARMOR
        self.armor: WearableSlot = self.WEARABLESLOT_TYPE(self, 'armor', 'Armor', 'nothing')
        self.weapon: WearableSlot = self.WEARABLESLOT_TYPE(self, 'weapon', 'Weapon', "Fists  ")
        #self.jewelry: WearableSlot = None
        self.shield: WearableSlot = self.WEARABLESLOT_TYPE(self, 'shield', 'Shield', "noshild")
        self.upperGarment: WearableSlot = self.WEARABLESLOT_TYPE(self, 'upperGarment', 'Upper Undergarment', "nounder")
        self.lowerGarment: WearableSlot = self.WEARABLESLOT_TYPE(self, 'lowerGarment', 'Lower Undergarment', "nounder")

        self.wearableslots = [self.armor, self.weapon, self.shield, self.upperGarment, self.lowerGarment]

        self.bonuses = self.BDS_CONTROLLER(self)

        # classes/classes/Scenes/Inventory.as, armorRackDescription, etc
        # THIS IS A TERRIBLE SYSTEM.
        self.gearStorage: List[Storage] = [
            #                     title,         start, end, category
            self.GEAR_STORE(self, 'Weapon Rack', 0,     9,   'Weapons',       keys=['weaponrack', 'weapon rack', 'weapons']),
            self.GEAR_STORE(self, 'Armor Rack',  9,     18,  'Armors',        keys=['armorrack', 'armor rack', 'armor', 'armors']),
            self.GEAR_STORE(self, 'Jewelry Box', 18,    27,  'Jewelry',       keys=['jewelrybox', 'jewelrybox', 'jewelry']),
            self.GEAR_STORE(self, 'Dresser',     27,    36,  'Undergarments', keys=['dresser', 'clothes', 'clothing']),
            self.GEAR_STORE(self, 'Shield Rack', 36,    45,  'Shields',       keys=['shieldrack', 'shield rack', 'shield', 'shields']),
        ]

        self.chestStorage: Storage = self.INVENTORY_STORE(self, 'Chests', None, keys=['itemstorage', 'chest', 'chests'])
        self.chestStorage.allocate(self.MAX_INVENTORY_SLOTS)
        self.playerStorage: Storage = self.INVENTORY_STORE(self, 'Player', None, keys=['player', 'pc'])
        self.playerStorage.allocate(self.MAX_INVENTORY_SLOTS)

        self.campStorage: List[Storage] = self.gearStorage+[self.chestStorage]
        self.allStorage: List[Storage] = [self.playerStorage]+self.campStorage

        self.storageByName: Dict[str, Storage] = {s.name:s for s in self.allStorage}

    def getCoCTweakConfig(self) -> CoCTweakData:
        '''
        CoCTweak adds a hidden status effect that stores CoCTweak-related data for this save.

        As a statuseffect, the game will load it without asking too many questions.

        This method returns the parsed contents of CoCTweakData, or a creates a new one.
        '''
        if self._editorData:
            return self._editorData
        else:
            self._editorData = self.COCTWEAKDATATYPE()
            if self.flags.has(CoCTweakData.FLAG_ID):
                self._editorData.deserialize(CoCTweakData)
        return self._editorData

    @property
    def capabilities(self):
        return self.CAPABILITIES

    def hasCapability(self, cap):
        return (self.CAPABILITIES & cap) == cap

    def getFlag(self, key, default):
        log.warning("getFlag(): Deprecated")
        return self.flags.get(key, default)

    def getStatusEffect(self, seID) -> StatusEffect:
        if isinstance(seID, enum.Enum):
            seID=seID.value
        return self.statusEffects.get(seID, None)

    def stash(self, item: Item) -> GearStorage:
        for store in self.campStorage:
            if store.accepts(item):
                for slot in store.slots:
                    if slot.is_empty():
                        slot.cloneMutables(item)
                        return store
        return None

    def loadSlots(self, rawslots: List[Item]) -> None:
        [self.loadSlot(x) for x in rawslots]

    def loadSlot(self, slot: Item) -> None:
        slot.from_items_json(self.getItemMeta(slot.id))

    def cloneSlots(self, rawslots: List[Item]) -> List[Item]:
        return [self.cloneSlot(x) for x in rawslots]

    def cloneSlot(self, rawslot: Item) -> Item:
        o = self.ITEM_TYPE()
        o.deserialize(rawslot.serialize())
        o.from_items_json(self.getItemMeta(o.id))
        return o

    def display(self, **kwargs):
        filename = sanitizeFilename(self.filename)

        log.info(f'{self.host}@{self.id} - {filename}')
        log.info(f'Mod: {self.getModName()} v{self.getModVersion()}')
        log.info(f'CoC Save Version: {self.getGameSaveVersion()}')
        log.info(f'Mod Save Version: {self.getModSaveVersion()}')
        log.info(f'{self.days} days, {self.hours} hours, {self.minutes} minutes')
        self.displayPlayerLine()
        self.displayOrientation()
        self.displayBaseStats()
        self.displayCombatStats()
        self.displayAss()
        self.displayBreasts()
        self.displayCocks()
        self.displayBalls()
        self.displayVaginas()
        self.displayPregnancies()
        self.displayPerks()
        self.displayStatusEffects()
        self.displayKeyItems()
        self.displayStorage()
        #self.displayChest('Inventory', self.inventory, hide_locked=True)
        #self.displayChest('Item Storage', self.itemStorage)
        #self.displayChest('Gear Storage', self.gearStorage)
        #for title, contents in self.storageByName.items():
        #    self.displayChest(title, contents)
        #if self.global_data is not None:
        #    self.global_data.displayAchievements()
        with log.info('EditorData flag:'):
            flagID = self.COCTWEAKDATATYPE.FLAG_ID
            log.info('Flag ID: %d', flagID)
            if self.flags.has(flagID):
                edd = self.getCoCTweakConfig()
                edd.show()
                with log.info('Raw contents:'):
                    log.info(json.dumps(json.loads(self.flags.get(flagID))))
            else:
                log.info('Not present in save.')

    def displayPregnancies(self):
        if self.pregnancy.isPregnant() or self.butt_pregnancy.isPregnant():
            with log.info('Pregnancies:'):
                if self.pregnancy.isPregnant():
                    log.info('Uterus: %s', self.pregnancy.describe())
                if self.butt_pregnancy.isPregnant():
                    log.info('Ass: %s', self.butt_pregnancy.describe())

    def displayBreasts(self):
        with log.info('Breasts:'):
            i=0
            for breastrow in self.breast_rows:
                with log.info(f'Row {i+1}:'):
                    breastrow.display()

    def displayOrientation(self):
        mf = 'Males' if self.sexOrientation > 50 else 'Females'
        log.info(f'Sexual orientation: Likes {mf} ({self.sexOrientation:g})')

    def displayPerks(self):
        with log.info('Perks:'):
            maxlen = 0
            for perk in self.perks.values():
                maxlen=max(maxlen, len(perk.id))
            for perk in self.perks.values():
                log.info(f'* {perk.id:{maxlen}} = {perk.displayValues()}')

    def displayStorage(self):
        with log.info('Storage:'):
            for s in self.allStorage:
                s.display()

    def availableInventories(self) -> List[str]:
        return [s.keys for s in self.allStorage]

    def displayStatusEffects(self):
        with log.info('Status Effects:'):
            plist=[]
            maxlen = 0
            for se in self.statusEffects.values():
                maxlen=max(maxlen, len(se.id))
            for _, se in sorted(self.statusEffects.items(), key=lambda x: x[0]):
                log.info(f'* {se.id:{maxlen}} = {se.displayValues()}')

    def displayKeyItems(self):
        with log.info('Key Items:'):
            plist=[]
            maxlen = 0
            for se in self.keyItems.values():
                maxlen=max(maxlen, len(se.id))
            for _, se in sorted(self.keyItems.items(), key=lambda x: x[0]):
                log.info(f'* {se.id:{maxlen}} = {se.displayValues()}')

    def displayPlayerLine(self):
        log.info(f'Player: {self.shortName}, Level {self.combat_stats.level.value:g} {self.getAgeStr()} {self.getLongGender()}')

    def fmtDictStats(self, stats: dict):
        return ', '.join([f'{k}: {self.statVal2Str(v)}' for k,v in stats.items()])

    def statVal2Str(self, value):
        if isinstance(value, (float, int)):
            return f'{value:g}'
        elif isinstance(value, str):
            return value
        elif isinstance(value, tuple):
            return f'{self.statVal2Str(value[0])} / {self.statVal2Str(value[1])}'
        else:
            return str(value)

    def displayBaseStats(self):
        log.info(self.fmtDictStats(self.getBaseStatStrs()))

    def getBaseStatStrs(self):
        return { k.upper():v.value for k,v in self.stats.allByID.items() }

    def getCombatStatStrs(self):
        exclude = ['level']
        return { v.id:v.value for v in self.combat_stats.all if v.id not in exclude }

    def displayCombatStats(self):
        log.info(self.fmtDictStats(self.getCombatStatStrs()))

    def displayCocks(self):
        if len(self.cocks) > 0:
            with log.info('Cocks:'):
                for cock in self.cocks:
                    cock.display()

    def displayVaginas(self):
        if len(self.vaginas) > 0:
            with log.info('Vaginas:'):
                for v in self.vaginas:
                    v.display()

    def displayBalls(self):
        if self.balls > 0:
            log.info(f'{self.balls}x balls, each {display_length(self.ballSize)} around, cum multiplier of {self.cumMultiplier:g}')

    def displayAss(self):
        with log.info('Ass:'):
            self.ass.display()

    def _postItemLoad(self) -> None:
        return

    def getItemMeta(self, _id) -> dict:
        if self.ALL_ITEMS is None:
            if self.ITEMTYPES_FILE is not None:
                with open(self.ITEMTYPES_FILE, 'r') as f:
                    self.ALL_ITEMS = json.load(f)
                self._postItemLoad()
            else:
                return {} # Mod does not support metadata
        return self.ALL_ITEMS.get(_id, {})

    def postPerkLoad(self) -> None:
        return

    def getPerkMeta(self, _id) -> dict:
        if self.ALL_PERKS is None:
            if self.PERKTYPES_FILE is not None:
                with open(self.PERKTYPES_FILE, 'r') as f:
                    self.ALL_PERKS = json.load(f)
                self.postPerkLoad()
                #print(f'Loaded {len(self.ALL_PERKS)} perks.')
            else:
                return {} # Mod does not support metadata
        return self.ALL_PERKS.get(_id, {})

    def postKeyItemLoad(self) -> None:
        return

    def getKeyItemMeta(self, _id) -> dict:
        if self.ALL_KEYITEMS is None:
            if self.KEYITEMS_FILE is not None:
                with open(self.KEYITEMS_FILE, 'r') as f:
                    self.ALL_KEYITEMS = json.load(f)
                self.postKeyItemLoad()
                #print(f'Loaded {len(self.ALL_PERKS)} perks.')
            else:
                return {} # Mod does not support metadata
        return self.ALL_KEYITEMS.get(_id, {})

    def postStatusEffectLoad(self) -> None:
        return

    def getStatusEffectMeta(self, _id) -> dict:
        if self.ALL_STATUSEFFECTS is None:
            if self.STATUSEFFECTTYPES_FILE is not None:
                with open(self.STATUSEFFECTTYPES_FILE, 'r') as f:
                    self.ALL_STATUSEFFECTS = json.load(f)
                self.postStatusEffectLoad()
            else:
                return {} # Mod does not support metadata
        return self.ALL_STATUSEFFECTS.get(_id, {})

    def createKeyItem(self, _id, values: List[int] = [0, 0, 0, 0]) -> None:
        if not self.hasKeyItem(_id):
            ki = self.keyItems[_id] = KeyItem()
            ki.id = _id
            ki.values=values
    def getOrCreateKeyItem(self, _id, values: List[int] = [0, 0, 0, 0]) -> KeyItem:
        if not self.hasKeyItem(_id):
            self.createKeyItem(_id, values)
        return self.getKeyItem(_id)

    def heal(self, hp=True, fatigue=False, lust=False, hunger=False) -> dict:
        stats = []
        if hp:
            stats += ['HP']
        if fatigue:
            stats += ['fatigue']
        if lust:
            stats += ['lust']
        if hunger:
            stats += ['hunger']
        return self.combat_stats.heal(stats)

    def get_parasites(self) -> List[Parasite]:
        if not self.hasCapability(SaveCaps.PARASITES):
            log.warning(f'This save\'s mod ({self.NAME}) does not support parasites.')
            return None
        return self.doGetParasites()

    def heal_parasites(self) -> List[str]:
        if not self.hasCapability(SaveCaps.PARASITES):
            log.warning(f'Unable to remove parasites, as this save\'s mod ({self.NAME}) does not support them.')
            return None
        return self.doHealParasites()

    def doGetParasites(self) -> List[Parasite]:
        parasites = [x() for x in self.PARASITES]
        results = []
        for p in parasites:
            if p.check(self):
                results += [p]
        return results

    def doHealParasites(self) -> None:
        parasites = [x() for x in self.PARASITES]
        for p in parasites:
            if p.check(self):
                p.removeInfection(self)

    def heal_statuses(self) -> None:
        for statID in self.BAD_STATUSES:
            if statID in self.statusEffects.keys():
                with log.info('Removing status effect %r...', statID):
                    self.statusEffects[statID].preRemove()
                    del self.statusEffects[statID]

    def get_bad_statuses(self) -> List[StatusEffect]:
        o = []
        for statID in self.BAD_STATUSES:
            if statID in self.statusEffects.keys():
                o.append(statID)
        return o

    def getModName(self):
        return self.NAME

    def getModVersion(self):
        return self.version

    def getModSaveVersion(self):
        return self.flags.get(2066, -1)

    def getGameSaveVersion(self):
        # SAVE_FILE_INTEGER_FORMAT_VERSION
        return self.flags.get(1238, -1)

    def getGender(self):
        if len(self.raw['cocks']) > 0 and len(self.raw['vaginas']) > 0:
            return "H"
        elif len(self.raw['cocks']) > 0:
            return "M"
        elif len(self.raw['vaginas']) > 0:
            return "F"
        else:
            return "U"

    def getLongGender(self):
        g = self.getGender()
        if g == 'M':
            return 'Male'
        elif g == 'F':
            return 'Female'
        elif g == 'H':
            return 'Herm'
        else:
            return 'Unknown'

    def getDifficulty(self):
        return 'UNKNOWN'

    def hasPerk(self, perkID) -> bool:
        if isinstance(perkID, enum.Enum):
            perkID=perkID.value
        return perkID in self.perks

    def getPerk(self, perkID) -> Perk:
        if isinstance(perkID, enum.Enum):
            perkID=perkID.value
        if not self.hasPerk(perkID):
            return None
        p = self.perks[perkID]
        if not p._metaLoaded:
            p.load_from_meta(self.getPerkMeta(perkID))
        return p

    def hasStatusEffect(self, statusID) -> bool:
        if isinstance(statusID, enum.Enum):
            statusID=statusID.value
        return statusID in self.statusEffects

    def hasKeyItem(self, kiID) -> bool:
        if isinstance(kiID, enum.Enum):
            kiID=kiID.value
        return kiID in self.keyItems

    def getKeyItem(self, kiID) -> KeyItem:
        if isinstance(kiID, enum.Enum):
            kiID=kiID.value
        if kiID not in self.keyItems:
            return None
        ki = self.keyItems[kiID]
        if not ki._metaLoaded:
            ki.load_from_meta(self.getKeyItemMeta(kiID))
        return ki

    def getAgeStr(self):
        return 'ADULT (N/I)'


    def deserialize(self, data: miniamf.sol.SOL):
        self.raw = data

        self.saveName = data.name

        self.version = data['version']

        self.perk_points = self._getInt('perkPoints')
        self.stat_points = self._getInt('statPoints')

        self.stats = self.CORE_STATS_TYPE(self)
        self.stats.deserialize(data)

        self.flags = KFlagStore(self.ENUM_FLAGS)
        self.flags.deserialize(self.raw)

        self.perks = {}
        for pData in self.raw['perks']:
            perk = self.PERK_CLASSES.get(pData['id'], Perk)()
            perk.deserialize(pData)
            perk.load_from_meta(self.getPerkMeta(perk.id))
            self.perks[perk.id] = perk

        self.statusEffects = {}
        for seData in self.raw['statusAffects']: # sic
            se = self.STATUS_EFFECT_CLASSES.get(seData[self.STATUSEFFECT_TYPE.ID_KEY], self.STATUSEFFECT_TYPE)()
            se.deserialize(seData)
            se.load_from_meta(self.getStatusEffectMeta(se.id))
            self.statusEffects[se.id] = se

        self.keyItems = {}
        for kiData in self.raw['keyItems']:
            ki = self.KEYITEM_CLASSES.get(kiData[self.KEYITEM_TYPE.ID_KEY], self.KEYITEM_TYPE)()
            ki.deserialize(kiData)
            self.keyItems[ki.id] = ki

        self.ass = self.ASS_TYPE()
        self.ass.deserialize(self.raw['ass'])
        self.ass.rating = self._getInt('buttRating', 0)

        self.cocks = []
        for cData in self.raw['cocks']:
            cock = self.COCK_TYPE()
            cock.deserialize(cData)
            self.cocks.append(cock)

        self.vaginas = []
        for vData in self.raw['vaginas']:
            v = self.VAGINA_TYPE()
            v.deserialize(vData)
            self.vaginas.append(v)

        self.breast_rows = []
        for brData in self.raw['breastRows']:
            breastrow = self.BREASTROW_TYPE()
            breastrow.deserialize(brData)
            self.breast_rows.append(breastrow)

        self.combat_stats = self.COMBAT_STATS_TYPE(self)
        self.combat_stats.deserialize(data)

        self.sexOrientation = self._getInt('sexOrientation')

        self.gems = self._getInt('gems')

        self.days = self._getInt('days')
        self.hours = self._getInt('hours')
        self.minutes = self._getInt('minutes')

        self.notes = self._getStr('notes', 'No Notes Available')

        self.shortName = self._getStr('short', '')

        self.loadInventory()
        #print('Loading itemStorage')
        #self.itemStorage = self.loadChest('itemStorage')
        #print('Loading gearStorage')
        #self.gearStorage = self.loadChest('gearStorage')

        self.balls=self._getInt('balls')
        self.ballSize=self._getFloat('ballSize')
        self.cumMultiplier=self._getFloat('cumMultiplier')

        self.startingRace = self._getStr('startingRace')

        self.pregnancy.deserialize(data)
        self.butt_pregnancy.deserialize(data)

    def loadInventory(self):
        self.playerStorage.clear()
        for i in range(self.MAX_INVENTORY_SLOTS):
            key = f'itemSlot{i+1}'
            slot = self.ITEM_TYPE()
            if key in self.raw:
                slot.deserialize(self.raw[key])
            self.loadSlot(slot)
            self.playerStorage.slots.append(slot)

        for storage in self.gearStorage:
            storage.deserialize(self.raw['gearStorage'])
        self.chestStorage.deserialize(self.raw['itemStorage'])
        #self.playerStorage.deserialize(self.raw['items'])
        return

    def saveInventory(self, data: dict):
        for i in range(self.MAX_INVENTORY_SLOTS):
            key = f'itemSlot{i+1}'
            data[key]=self.playerStorage.slots[i].serialize()
        #data['gearStorage'] = []
        #[for s in self.gearStorage] s.serialize(data['gearStorage'])
        data['itemStorage'] = self.chestStorage.serialize()

    def loadChest(self, key):
        o = []
        if key in self.raw:
            data = self.raw[key]
            for itemdata in data:
                slot = self.ITEM_TYPE()
                #print(repr(itemdata))
                slot.deserialize(itemdata)
                o.append(slot)
        return o

    def saveChest(self, data: dict, key: str, slots: List[Item]):
        data[key] = []
        for slot in slots:
            data[key].append(slot.serialize())



    def calcSlotLocks(self) -> int:
        # OVERRIDE IN MODS.
        return 3

    def setBackpackSize(self, backsize: int) -> None:
        if backsize != 0:
            if not self.hasKeyItem('Backpack'):
                self.createKeyItem('Backpack', [backsize, 0, 0, 0])
            else:
                self.getKeyItem('Backpack').values[0] = backsize
        else:
            ki = self.getKeyItem('Backpack')
            if ki is not None:
                del self.keyItems['Backpack']

    def getBackpackSize(self) -> int:
        ki = self.getKeyItem('Backpack')
        if ki is None:
            return 0
        return ki.values[0]

    def _fixRemoveKey(self, data: dict, key: str, reason: str) -> None:
        if key in data:
            log.warning(f'Removing key {key!r} from save ({reason})')
            del data[key]

    def serializeEditorData(self, data: dict):
        edd = self.getCoCTweakConfig()
        #data['flags'][str(self.COCTWEAKDATATYPE.FLAG_ID)] = json.dumps(edd.serialize(), separators=(',', ':'))
        self.flags.set(self.COCTWEAKDATATYPE.FLAG_ID, json.dumps(edd.serialize(), separators=(',', ':')))

    def getStorageByKey(self, key: str) -> Storage:
        for s in self.allStorage:
            if key in s.keys:
                return s
        raise KeyError(key)

    def serialize(self) -> dict:

        data = super().serialize()

        # Fuckups!
        self._fixRemoveKey(data, 'flags_decoded', 'Older version of decoded flags')
        self._fixRemoveKey(data, 'flags-decoded', 'Older version of decoded flags')

        data.update(self.stats.serialize())
        data.update(self.combat_stats.serialize())

        data['saveName'] = self.saveName # non-standard

        self.serializeEditorData(data)
        data.update(self.flags.serialize())

        data['cocks'] = [c.serialize() for c in self.cocks]
        data['vaginas'] = [v.serialize() for v in self.vaginas]
        data['breastRows'] = [br.serialize() for br in self.breast_rows]
        data['ass'] = self.ass.serialize()

        data['sexOrientation'] = self.sexOrientation

        data['gems'] = self.gems

        data['days'] = self.days
        data['hours'] = self.hours
        data['minutes'] = self.minutes

        data['notes'] = self.notes

        data['short'] = self.shortName

        data['balls'] = self.balls
        data['ballSize'] = self.ballSize
        data['cumMultiplier'] = self.cumMultiplier

        data['startingRace'] = self.startingRace

        data['perkPoints'] = self.perk_points
        data['statPoints'] = self.stat_points

        selist = []
        for se in self.statusEffects.values():
            selist.append(se.serialize())
        data['statusAffects'] = selist #sic

        perks = []
        for p in self.perks.values():
            perks.append(p.serialize())
        data['perks'] = perks

        keyItems = []
        for ki in self.keyItems.values():
            keyItems.append(ki.serialize())
        data['keyItems'] = keyItems


        self.saveInventory(data)
        #self.saveChest(data, 'itemStorage', self.itemStorage)
        #self.saveChest(data, 'gearStorage', self.gearStorage)

        self.pregnancy.serializeTo(data)
        self.butt_pregnancy.serializeTo(data)

        return data
