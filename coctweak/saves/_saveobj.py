from enum import IntEnum
from typing import Optional
from coctweak.saves._serialization import SerializationVersionInfo

class SaveObject(object):
    SERIALIZATION_STAMP: Optional[SerializationVersionInfo] = None

    def __init__(self):
        self.raw={}

    def _getBool(self, key: str, default: bool=False) -> False:
        return bool(self.raw.get(key, default))

    def _getInt(self, key: str, default: int=0) -> int:
        return int(self.raw.get(key, default))

    def _getStr(self, key: str, default: str="") -> str:
        return str(self.raw.get(key, default))

    def _getFloat(self, key: str, default: float=0.0) -> float:
        return float(self.raw.get(key, default))

    def _getList(self, key: str, default: list=[]) -> list:
        return list(self.raw.get(key, default))

    def _getDict(self, key: str, default: dict=[]) -> dict:
        return dict(self.raw.get(key, default))

    def serialize(self) -> dict:
        if self.SERIALIZATION_STAMP is not None:
            return self.SERIALIZATION_STAMP.serialize(self, self.raw)
        return self.raw

    def deserialize(self, data: dict) -> None:
        self.raw = data
        if self.SERIALIZATION_STAMP is not None:
            self.SERIALIZATION_STAMP.deserialize(self, data)
