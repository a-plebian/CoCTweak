from typing import Type
from coctweak.saves.vanilla._base.ass import BaseAss

from coctweak.logging import getLogger
log = getLogger(__name__)

from enum import Enum

class Ass(BaseAss):
    ENUM_LOOSENESS: Type[Enum] = None
    ENUM_WETNESS: Type[Enum] = None
    ENUM_RATINGS: Type[Enum] = None

    def __init__(self):
        super().__init__()

    def getLoosenessStr(self):
        if self.ENUM_LOOSENESS is not None:
            return self.ENUM_LOOSENESS(self.looseness).name
        else:
            return f'{self.looseness:g} looseness'

    def getWetnessStr(self):
        if self.ENUM_WETNESS is not None:
            return self.ENUM_WETNESS(self.wetness).name
        else:
            return f'{self.wetness:g} wetness'

    def getRatingStr(self):
        if self.ENUM_RATINGS is not None:
            last = -1
            for e in self.ENUM_RATINGS:
                if self.rating <= e.value and self.rating > last:
                    return f'{e.name} ({self.rating})'
                last = e.value
        else:
            return f'{self.rating:g} rating'

    def display(self):
        desc = [
            self.getLoosenessStr(),
            self.getWetnessStr(),
            self.getRatingStr(),
        ]
        desc = [', '.join(desc) + ' ass']
        if self.fullness > 0.0:
            desc += [f'with fullness={self.fullness}']
        log.info(', '.join(desc))
