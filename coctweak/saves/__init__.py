import os, platform, sys, yaml, logging

from typing import Optional

from miniamf import sol, AMF3

from coctweak.saves.vanilla.save import VanillaSave
from coctweak.saves.hgg.save import HGGSave
from coctweak.saves.uee.save import UEESave
from coctweak.saves.ej.save import EJSave
from coctweak.saves._save import BaseSave, SaveCaps
from coctweak.saves._globaldata import GlobalData
from coctweak.utils import sanitizeFilename, _do_cleanup
from coctweak.logging import getLogger

log = getLogger(__name__)

MOD_VERSION = 2066

# Enable to spam detection stuff.
DETECTION_DEBUG = False

FLASH_BASE_DIR = ''
if platform.system() == 'Windows':
    FLASH_BASE_DIR = os.path.join(os.environ['APPDATA'], 'Macromedia', 'Flash Player', '#SharedObjects')
elif platform.system() == 'Linux':
    FLASH_BASE_DIR = os.path.expanduser('~/.macromedia/Flash_Player/#SharedObjects/')
for subdir in os.listdir(FLASH_BASE_DIR):
    if subdir not in ('.','..'):
        FLASH_BASE_DIR = os.path.join(FLASH_BASE_DIR, subdir)
        break


NSLOOKUP = {
    'ej': 'EndlessJourney'
}

def getSaveFilename(host:str, svID:int, ext:str='.sol') -> str:
    namespace = ''
    if '/' in host:
        host, namespace = host.split('/')
    basedir = os.path.join(FLASH_BASE_DIR, host)
    if namespace != '':
        subdir = NSLOOKUP[namespace]
        basedir = os.path.join(basedir, '#CoC', subdir)
    return os.path.join(basedir, 'CoC_'+str(svID)+ext)

def loadSave(data):
    saveLoader = detectModType(data)
    save = saveLoader()
    save.deserialize(data)
    return save

def saveToSOL(save, filename, slotID):
    fdata = sol.SOL(f'CoC_{slotID}')
    fdata.update(save.serialize())
    sol.save(fdata, filename, AMF3)

def loadFromSlot(hostname: str, slotID: int, quiet:bool = False, loglevel_file_not_found: int=logging.CRITICAL, for_backup:bool = False) -> BaseSave:
    namespace = ''
    orighost = hostname
    if for_backup:
        loglevel_file_not_found=logging.WARNING
    if '/' in hostname:
        hostname, namespace = hostname.split('/')
    if not os.path.isdir(os.path.join(FLASH_BASE_DIR, hostname)):
        log.arbitrary(loglevel_file_not_found, 'Flash does not have a directory for hostname {!r}.'.format(sanitizeFilename(os.path.join(FLASH_BASE_DIR, hostname))))
        return None
    if namespace != '':
        subdir = NSLOOKUP[namespace]
        nsdir = os.path.join(FLASH_BASE_DIR, hostname, '#CoC', subdir)
        if not os.path.isdir(nsdir):
            log.arbitrary(loglevel_file_not_found, 'Flash does not have a directory for namespace {!r} ({}).'.format(namespace, sanitizeFilename(nsdir)))
            return None

    filename = getSaveFilename(orighost, slotID)

    save = loadSaveFromFile(filename, quiet, loglevel_file_not_found=loglevel_file_not_found)
    save.loading_for_backup = for_backup
    save.id = slotID
    save.host = hostname
    save.namespace = namespace
    save.global_data = loadGlobalData(save.GLOBAL_DATA_TYPE, save.GLOBAL_DATA_FILENAME, hostname, quiet, loglevel_file_not_found=loglevel_file_not_found)
    return save

def loadSaveFromFile(filename: str, quiet:bool = False, loglevel_file_not_found: int=logging.CRITICAL) -> BaseSave:
    try:
        if not quiet:
            log.info(f'Loading {sanitizeFilename(filename)}...').indent()
        if not os.path.isfile(filename):
            log.arbitrary(loglevel_file_not_found, '%r does not exist.', sanitizeFilename(filename))
            return None
        save = loadSave(sol.load(filename))
        save.filename = filename
    finally:
        if not quiet:
            log.dedent()
    return save

def loadGlobalData(pObjType, pObjFilename:str, hostname:str, quiet:bool = False, loglevel_file_not_found: int=logging.CRITICAL) -> Optional[GlobalData]:
    if pObjType is None:
        return None
    if pObjFilename is None:
        return None
    filename = os.path.join(FLASH_BASE_DIR, hostname, pObjFilename+'.sol')
    try:
        if not quiet:
            log.info('Loading %s...', sanitizeFilename(filename)).indent()
        if not os.path.isfile(filename):
            log.arbitrary(loglevel_file_not_found, '%r does not exist.', sanitizeFilename(filename))
            return None

        pobj = pObjType()
        pobj.deserialize(sol.load(filename))
        pobj.filename = filename
        pobj.host = hostname
    finally:
        if not quiet:
            log.dedent()
    return pobj

def saveToSlot(hostname: str, slotID: int, save:BaseSave, quiet:bool=False) -> None:
    filename = getSaveFilename(hostname, slotID)
    try:
        if not quiet:
            log.info(f'Saving {sanitizeFilename(filename)}...').indent()
        saveToSOL(save, filename, slotID)
    finally:
        if not quiet:
            log.dedent()

# I've talked to Mothman of HGG about a better way of detecting the mod, and apparently got some balls rolling.
# Until then:
def detectModType(data:sol.SOL) -> Optional[BaseSave]:
    log.debug(f'SOL name: {data.name!r}')
    if 'version' in data:
        log.debug(f'Game version: {data["version"]!r}')
    else:
        log.debug('Game version: SOL["version"] missing!')

    save_version = data["flags"].get("2066", 'N/A')
    log.debug(f'Flag 2066 (MOD_SAVE_VERSION): {save_version!r}')

    if DETECTION_DEBUG:
        with open('last-save-raw.yml', 'w') as f:
            yaml.dump(data, f, default_flow_style=False)
        with open('last-save-cleaned.yml', 'w') as f:
            yaml.dump(_do_cleanup(data), f, default_flow_style=False)

    flags = data.get('flags', {})
    if str(MOD_VERSION) not in flags:
        log.debug(f'Flag {MOD_VERSION} not set.')
        log.debug('DETECTED AS VANILLA')
        return VanillaSave

    # Endless Journey nicely announces itself.
    if data['version'] == 'Endless Journey':
        log.debug('save["version"] is "Endless Journey".')
        log.debug('DETECTED AS EJ')
        return EJSave

    # HGG version 1.4.11 finally announces itself.
    if data['version'] is not None and data['version'].startswith('hgg '):
        # Some version of HGG
        log.debug(f'save["version"] is present and starts with \'hgg \'.')
        log.debug(f'DETECTED AS HGG')
        return HGGSave

    if 'serializationVersionDictionary' in data:
        log.debug('Found UEE\'s weird serializationVersionDictionary shit')
        log.debug('DETECTED AS UEE')
        return UEESave

    # HGG has the selfSaving NPCs.
    # EJ has them under the npc key in a similar way.
    # Same with UEE, but UEE has a different serialization versioning thing.
    if 'selfSaving' in data:
        # Some version of HGG
        log.debug(f'save["selfSaving"] is present.')
        log.debug(f'DETECTED AS HGG')
        return HGGSave

    if isinstance(data['version'], str):
        # Avoiding regex for speed
        # EJ, UEE, and Revamp: 1.2.3_mod_4.5.6
        chunks = data['version'].split('_')
        if len(chunks) == 3:
            baseversion, mod, modversion = chunks
            if mod == 'mod': # Uncreative name? UEE!
                log.debug(f'save["version"] has "mod" as the mod ID.')
                log.debug(f'DETECTED AS UEE/REVAMP.')
                return UEESave

    return None
