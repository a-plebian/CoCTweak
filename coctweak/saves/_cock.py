'''
cocks:
- cockLength: 5.5
  cockThickness: 1
  cockType: 0
  knotMultiplier: 1
  pLongDesc: ''
  pShortDesc: ''
  pierced: 0
  serializationVersion: 1
  sock: ''
'''
from enum import IntFlag, auto
from coctweak.utils import display_length, in2feetinches
from coctweak.saves._saveobj import SaveObject
from coctweak.saves.hgg.enums.cocktypes import HGGCockTypes
from coctweak.saves.vanilla._base.cock import BaseCock
from coctweak.logging import getLogger
log = getLogger(__name__)

class ECockTypeFlags(IntFlag):
    HAS_KNOT = auto()

class Cock(BaseCock):
    COCK_TYPES = HGGCockTypes
    COCK_TYPE_FLAGS = {
        HGGCockTypes.DOG: ECockTypeFlags.HAS_KNOT
    }

    def __init__(self):
        super().__init__()

    def getCockTypeStr(self) -> str:
        return self.type.name

    def getTypeFlags(self) -> ECockTypeFlags:
        return self.COCK_TYPE_FLAGS.get(self.type, ECockTypeFlags(0))

    def hasTypeFlag(self, flag: ECockTypeFlags):
        return (self.getTypeFlags() & flag) == flag

    def canHaveKnot(self) -> bool:
        return self.hasTypeFlag(ECockTypeFlags.HAS_KNOT)

    def display(self):
        desc = [f'{display_length(self.length)}L x {display_length(self.thickness)}W ({round(self.length * self.thickness):g}in² area) {self.getCockTypeStr()} cock']
        if self.knotMultiplier > 0 and self.canHaveKnot():
            kw = (self.thickness*self.knotMultiplier)
            desc += [f'with a knot {display_length(kw)} wide (KM={self.knotMultiplier:g})']
        if self.pierced > 0:
            desc += [f'with piercing #{self.pierced}']
        log.info(', '.join(desc))
