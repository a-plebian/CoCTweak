# @GENERATED from https://raw.githubusercontent.com/OXOIndustries/Corruption-of-Champions/master/classes/classes/BreastRowClass.as
from coctweak.saves._saveobj import SaveObject

__ALL__=['BaseBreastRow']

class BaseBreastRow(SaveObject):
    def __init__(self):
        super().__init__()
        self.breasts: float = 0.0
        self.nipplesPerBreast: float = 0.0
        self.breastRating: float = 0.0
        self.lactationMultiplier: float = 0.0
        self.milkFullness: float = 0.0
        self.fullness: float = 0.0
        self.fuckable: bool = False
    def serialize(self) -> dict:
        data = super().serialize()
        data["breasts"] = self.breasts
        data["nipplesPerBreast"] = self.nipplesPerBreast
        data["breastRating"] = self.breastRating
        data["lactationMultiplier"] = self.lactationMultiplier
        data["milkFullness"] = self.milkFullness
        data["fullness"] = self.fullness
        data["fuckable"] = self.fuckable
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.breasts = self._getFloat("breasts", 0.0)
        self.nipplesPerBreast = self._getFloat("nipplesPerBreast", 0.0)
        self.breastRating = self._getFloat("breastRating", 0.0)
        self.lactationMultiplier = self._getFloat("lactationMultiplier", 0.0)
        self.milkFullness = self._getFloat("milkFullness", 0.0)
        self.fullness = self._getFloat("fullness", 0.0)
        self.fuckable = self._getBool("fuckable", False)
