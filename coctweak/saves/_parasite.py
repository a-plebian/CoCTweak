from typing import List, Dict, Any
from enum import Enum
from coctweak.saves._statuseffect import StatusEffect
from coctweak.saves._perk import Perk
from coctweak.logging import getLogger
log = getLogger(__name__)
class Parasite(object):
    NAME: str = ''
    STATUS_EFFECTS: List[str] = []
    PERKS: List[str] = []
    KFLAGS: List[int] = []

    def __init__(self):
        self.save = None
        self.indicating_sfx: List[StatusEffect] = []
        self.indicating_perks: List[Perk] = []
        self.indicating_kflags: Dict[int, Any] = {}

    def checkSFX(self, se:StatusEffect) -> bool:
        if se.id in self.STATUS_EFFECTS:
            self.indicating_sfx += [se]
            return True
        return False

    def checkPerk(self, p:Perk) -> bool:
        if p.id in self.PERKS:
            self.indicating_perks += [p]
            return True
        return False

    def checkKFlag(self, k: int, v: Any) -> bool:
        if k in self.KFLAGS:
            self.indicating_kflags[k] = v
            return True
        return False

    def check(self, save) -> bool:
        self.save = save
        o = False
        for sfxid in self.STATUS_EFFECTS:
            if save.hasStatusEffect(sfxid):
                self.indicating_sfx.append(save.statusEffects[sfxid])
                o = True
        for pid in self.PERKS:
            if save.hasPerk(pid):
                self.indicating_perks.append(save.perks[pid])
                o = True
        for k in self.KFLAGS:
            if save.flags.has(k):
                self.indicating_kflags[k] = save.flags.get(k)
                o = True
        return o

    def displayDiagnosis(self, save) -> None:
        self.save = save
        with log.warning('INFECTED with %s!', self.NAME):
            with log.info('Indicators:'):
                for se in self.indicating_sfx:
                    log.info('Status effect %r - %s', se.id, se.displayValues())
                for p in self.indicating_perks:
                    log.info('Perk %r - %s', p.id, p.displayValues())
                for k in self.indicating_kflags:
                    log.info('kFlag %s (%d) - %s', save.ENUM_FLAGS(k).name, k, save.flags.get(k))
            with log.info('Additional Information:'):
                self.displayState()

    def displayState(self) -> None:
        return

    def removeInfection(self, save, **kwargs) -> None:
        if len(self.indicating_sfx) > 0:
            for se in self.indicating_sfx:
                self.removeStatusEffect(se)

        if len(self.indicating_perks) > 0:
            for p in self.indicating_perks:
                self.removePerk(p)

        if len(self.indicating_kflags.keys()) > 0:
            for k,v in self.indicating_kflags.items():
                self.removeKFlag(k)

    def removeStatusEffect(self, se):
        seid = None
        if isinstance(se, StatusEffect):
            seid = se.id
        if isinstance(se, Enum):
            seid = se.value
        if isinstance(se, str):
            seid = se
        if self.save.hasStatusEffect(seid):
            log.info('Removed status effect %r...', seid)
            del self.save.statusEffects[seid]

    def removePerk(self, p):
        pid = None
        if isinstance(p, Perk):
            pid = p.id
        if isinstance(p, Enum):
            pid = p.value
        if isinstance(p, str):
            pid = p
        if self.save.hasPerk(pid):
            log.info('Removing perk %r...', pid)
            del self.save.perks[pid]

    def removeKFlag(self, k):
        log.info('Unsetting kFlag %s (%d)...', self.save.ENUM_FLAGS(k).name, k)
        self.save.flags.remove(k)

    def addInfection(self, save, **kwargs) -> None:
        return
