# How we store all pregnancies.
from ._saveobj import SaveObject
from .vanilla.pregnancytypes import VanillaPregnancyType
class BasePregnancy(SaveObject):
    TYPE_KEY: str = ''
    TYPE_ENUM: type = VanillaPregnancyType
    INCUBATION_KEY: str = ''

    def __init__(self):
        super().__init__()

        self.type: int = 0
        self.incubation: int = 0

    def clear(self) -> None:
        self.type = 0
        self.incubation = 0

    def serialize(self) -> dict:
        o = {}
        self.serializeTo(o)
        return o

    def serializeTo(self, data) -> None:
        data[self.TYPE_KEY] = self.type
        data[self.INCUBATION_KEY] = self.incubation

    def deserialize(self, data) -> None:
        self.type = int(data[self.TYPE_KEY])
        self.incubation = int(data[self.INCUBATION_KEY])
        #print(self.TYPE_KEY, self.type, self.INCUBATION_KEY, self.incubation)

    def isPregnant(self) -> bool:
        return self.type > 0

    def describe(self) -> str:
        if self.type == 0:
            return 'Not pregnant'
        else:
            typestr = self.TYPE_ENUM(self.type).name
            return f'Incubating {typestr} (time: {self.incubation})'

class VaginalPregnancy(BasePregnancy):
    TYPE_KEY = 'pregnancyType'
    INCUBATION_KEY = 'pregnancyIncubation'

class ButtPregnancy(BasePregnancy):
    TYPE_KEY = 'buttPregnancyType'
    INCUBATION_KEY = 'buttPregnancyIncubation'

__all__ = ['BasePregnancy', 'ButtPregnancy', 'VaginalPregnancy']
