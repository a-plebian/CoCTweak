# @GENERATED from https://raw.githubusercontent.com/Kitteh6660/Corruption-of-Champions-Mod/master/classes/classes/Ass.as
from enum import IntEnum

__ALL__ = ['UEEAssLooseness']

class UEEAssLooseness(IntEnum):
    VIRGIN     = 0
    TIGHT      = 1
    NORMAL     = 2
    LOOSE      = 3
    STRETCHED  = 4
    GAPING     = 5
