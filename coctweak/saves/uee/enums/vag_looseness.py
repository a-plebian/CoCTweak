# @GENERATED from https://raw.githubusercontent.com/Kitteh6660/Corruption-of-Champions-Mod/master/classes/classes/Vagina.as
from enum import IntEnum

__ALL__ = ['UEEVagLooseness']

class UEEVagLooseness(IntEnum):
    TIGHT            = 0
    NORMAL           = 1
    LOOSE            = 2
    GAPING           = 3
    GAPING_WIDE      = 4
    LEVEL_CLOWN_CAR  = 5
