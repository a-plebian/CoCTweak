# @GENERATED from https://raw.githubusercontent.com/Kitteh6660/Corruption-of-Champions-Mod/master/classes/classes/Ass.as
from enum import IntEnum

__ALL__ = ['UEEAssWetness']

class UEEAssWetness(IntEnum):
    DRY             = 0
    NORMAL          = 1
    MOIST           = 2
    SLIMY           = 3
    DROOLING        = 4
    SLIME_DROOLING  = 5
