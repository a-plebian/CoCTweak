# @GENERATED from https://raw.githubusercontent.com/Kitteh6660/Corruption-of-Champions-Mod/master/classes/classes/Items/JewelryLib.as
from enum import IntEnum

__ALL__ = ['UEEJewelryModifiers']

class UEEJewelryModifiers(IntEnum):
    MINIMUM_LUST  = 1
    FERTILITY     = 	2
    CRITICAL      = 	3
    REGENERATION  = 4
    HP            = 			5
    ATTACK_POWER  = 6
    SPELL_POWER   = 	7
    PURITY        = 				8
    CORRUPTION    = 			9
