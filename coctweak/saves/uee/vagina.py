from coctweak.saves.uee._base.vagina import UEEBaseVagina
from coctweak.saves.uee.enums.vag_wetness import UEEVagWetness
from coctweak.saves.uee.enums.vag_looseness import UEEVagLooseness
from coctweak.saves.uee.enums.vag_types import UEEVagTypes

__ALL__ = ['UEEVagina']

class UEEVagina(UEEBaseVagina):
    ENUM_WETNESS   = UEEVagWetness
    ENUM_LOOSENESS = UEEVagLooseness
    ENUM_TYPES     = UEEVagTypes
