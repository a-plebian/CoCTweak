# @GENERATED from https://raw.githubusercontent.com/Kitteh6660/Corruption-of-Champions-Mod/master/classes/classes/BreastRow.as
from coctweak.saves._breastrow import BreastRow
from coctweak.saves.uee.serialization import UEESerializationVersion

__ALL__=['UEEBaseBreastRow']

class UEEBaseBreastRow(BreastRow):
    SERIALIZATION_STAMP = UEESerializationVersion('c862ee0a-5667-4fd3-a178-37a5e85c86d6', 0, 1)
    def __init__(self):
        super().__init__()
        self.nippleCocks: bool = False
    def serialize(self) -> dict:
        data = super().serialize()
        data["nippleCocks"] = self.nippleCocks
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.nippleCocks = self._getBool("nippleCocks", False)
