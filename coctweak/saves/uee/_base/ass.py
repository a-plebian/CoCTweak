# @GENERATED from https://raw.githubusercontent.com/Kitteh6660/Corruption-of-Champions-Mod/master/classes/classes/Ass.as
from coctweak.saves._ass import Ass
from coctweak.saves.uee.serialization import UEESerializationVersion

__ALL__=['UEEBaseAss']

class UEEBaseAss(Ass):
    SERIALIZATION_STAMP = UEESerializationVersion('0a3bd267-6ce0-4fed-a81b-53a4ccd6c17d', 0, 1)
    def __init__(self):
        super().__init__()
        #self.virgin: bool = False
    def serialize(self) -> dict:
        data = super().serialize()
        #data["virgin"] = self.virgin
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        #self.virgin = self._getBool("virgin", False)
