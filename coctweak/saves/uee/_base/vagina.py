# @GENERATED from https://raw.githubusercontent.com/Kitteh6660/Corruption-of-Champions-Mod/master/classes/classes/Vagina.as
from coctweak.saves._vagina import Vagina
from coctweak.saves.uee.serialization import UEESerializationVersion

__ALL__=['UEEBaseVagina']

class UEEBaseVagina(Vagina):
    SERIALIZATION_STAMP = UEESerializationVersion('cfe61f89-7ab6-4e4b-83aa-33f738dd2f05', 0, 1)
    def __init__(self):
        super().__init__()
        self.clitLength: float = 0.0
        self.recoveryProgress: int = 0
    def serialize(self) -> dict:
        data = super().serialize()
        data["clitLength"] = self.clitLength
        data["recoveryProgress"] = self.recoveryProgress
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.clitLength = self._getFloat("clitLength", 0.0)
        self.recoveryProgress = self._getInt("recoveryProgress", 0)
