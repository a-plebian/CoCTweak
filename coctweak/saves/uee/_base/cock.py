# @GENERATED from https://raw.githubusercontent.com/Kitteh6660/Corruption-of-Champions-Mod/master/classes/classes/Cock.as
from coctweak.saves._cock import Cock
from coctweak.saves.uee.serialization import UEESerializationVersion

__ALL__=['UEEBaseCock']

class UEEBaseCock(Cock):
    SERIALIZATION_STAMP = UEESerializationVersion('98367a43-6bf4-4b5c-b509-abea7e416416', 0, 1)
    def __init__(self):
        super().__init__()
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
