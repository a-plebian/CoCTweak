from coctweak.saves._wearableslot import WearableSlot
from coctweak.saves._saveobj import SaveObject
class UEEItemEffect(SaveObject):
    def __init__(self):
        self.id = None
        self.magnitude = 0
        super().__init__()

    def deserialize(self, data: dict):
        self.raw = data
        self.id = self._getInt('effectId', None)
        self.magnitude = self._getFloat('effectMagnitude', None)
    def serialize(self) -> dict:
        data = {}
        data.update(self.raw)
        if self.id is not None:
            data['effectId'] = int(self.id)
        else:
            if 'effectId' in data:
                del data['effectId']
        if self.magnitude is not None:
            data['effectMagnitude'] = float(self.magnitude)
        else:
            if 'effectMagnitude' in data:
                del data['effectMagnitude']
        return data

class UEEWearableSlot(WearableSlot):
    def __init__(self, save, _id: str, label: str, defaultID: str):
        self.effect: UEEItemEffect = UEEItemEffect()

        super().__init__(save, _id, label, defaultID)

    def deserialize(self, data: dict):
        super().deserialize(data)
        self.effect = UEEItemEffect()
        self.effect.deserialize(data)

    def serialize(self):
        data = super().serialize()
        data.update(self.effect.serialize())
        return data
