import math
from coctweak.saves._combatstats import BaseCombatStats
from coctweak.saves.uee.enums.kflags import UEEKFlags
from coctweak.saves._stat import Stat, StatHealType
from coctweak.saves.uee.enums.jewelrymodifiers import UEEJewelryModifiers
from coctweak.saves.uee.enums.itemlib import UEEItemLib
from coctweak.saves.uee.enums.perklib import UEEPerkLib
from coctweak.saves.uee.enums.statuslib import UEEStatusLib
from coctweak.saves.uee.bimboprogress import UEEBimboProgress

class UEECombatStats(BaseCombatStats):
    def __init__(self, save):
        self.hunger: Stat = None
        super().__init__(save)

    def init(self):
        super().init()
        self.hunger = self._add(Stat('hunger', 'Hunger', 0.0, lambda: 0, self._get_hunger_max, healtype=StatHealType.MIN))

    def _get_hunger_max(self) -> float:
        #https://github.com/Kitteh6660/Corruption-of-Champions-Mod/blob/master/classes/classes/Character.as: public function maxHunger():Number {
        return 100

    def _get_lust_min(self) -> float:
        # https://github.com/Kitteh6660/Corruption-of-Champions-Mod/blob/master/classes/classes/Player.as
        #public override function minLust():Number {
        #var min:Number = 0;
        m = 0
        #var minCap:Number = 100;
        minCap = 100

        #//Bimbo body boosts minimum lust by 40
        #if (hasStatusEffect(StatusEffects.BimboChampagne) || findPerk(PerkLib.BimboBody) >= 0 || findPerk(PerkLib.BroBody) >= 0 || findPerk(PerkLib.FutaForm) >= 0) {
        if self.save.hasStatusEffect(UEEStatusLib.BimboChampagne) or self.save.hasPerk(UEEPerkLib.BimboBody) or self.save.hasPerk(UEEPerkLib.BroBody) or self.save.hasPerk(UEEPerkLib.FutaForm):
            #if (min > 40) min += 10;
            if m > 40:
                m += 10
            #else if (min >= 20) min += 20;
            elif m >= 20:
                m += 20
            #else min += 40;
                m += 40
            #if (armorName == "bimbo skirt") min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST] / 4;
            if self.save.armor.item.id == "BimboSk":
                m += self.save.flags[UEEKFlags.BIMBOSKIRT_MINIMUM_LUST] / 4
        #}
        #else if (game.bimboProgress.ableToProgress()) {
        if UEEBimboProgress.ableToProgress(self.save):
            #if (min > 40) min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST] / 4;
            if m > 40:
                m += self.save.flags.get(UEEKFlags.BIMBOSKIRT_MINIMUM_LUST) / 4
            #else if (min >= 20) min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST] / 2;
            elif m >= 20:
                m += self.save.flags.get(UEEKFlags.BIMBOSKIRT_MINIMUM_LUST) / 2
            #else min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST];
            else:
                m += self.save.flags.get(UEEKFlags.BIMBOSKIRT_MINIMUM_LUST)
        #}
        #//Omnibus' Gift
        #if (hasPerk(PerkLib.OmnibusGift)) {
        if self.save.hasPerk(UEEPerkLib.OmnibusGift):
            #if (min > 40) min += 10;
            if m > 40:
                m += 10
            #else if (min >= 20) min += 20;
            elif m >= 20:
                m += 20
            #else min += 35;
            else:
                m += 35
        #}
        #//Nymph perk raises to 30
        #if (hasPerk(PerkLib.Nymphomania)) {
        if self.save.hasPerk(UEEPerkLib.Nymphomania):
            #if (min >= 40) min += 10;
            if m >= 40:
                m += 10
            #else if (min >= 20) min += 15;
            elif m >= 20:
                m += 15
            #else min += 30;
            else:
                m += 30
        #}
        #//Oh noes anemone!
        #if (hasStatusEffect(StatusEffects.AnemoneArousal)) {
        if self.save.hasStatusEffect(UEEStatusLib.AnemoneArousal):
            #if (min >= 40) min += 10;
            if m >= 40:
                m += 10
            #else if (min >= 20) min += 20;
            elif m >= 20:
                m += 20
            #else min += 30;
            else:
                m += 30

        #//Hot blooded perk raises min lust!
        #if (hasPerk(PerkLib.HotBlooded)) {
        if self.save.hasPerk(UEEPerkLib.HotBlooded):
            #if (min > 0) min += perk(findPerk(PerkLib.HotBlooded)).value1 / 2;
            if m > 0:
                m += self.save.getPerk(UEEPerkLib.HotBlooded).values[0] / 2
            #else min += perk(findPerk(PerkLib.HotBlooded)).value1;
            else:
                m += self.save.getPerk(UEEPerkLib.HotBlooded).values[0]
        #}
        #if (hasPerk(PerkLib.LuststickAdapted)) {
        if self.save.hasPerk(UEEPerkLib.LuststickAdapted):
            #if (min < 50) min += 10;
            if m < 50:
                m += 10
            #else min += 5;
            else:
                m += 5
        #}
        #if (hasStatusEffect(StatusEffects.Infested)) {
        if self.save.hasStatusEffect(UEEStatusLib.Infested):
            #if (min < 50) min = 50;
            m = max(50, m)

        #//Add points for Crimstone
        #min += perkv1(PerkLib.PiercedCrimstone);
        if self.save.hasPerk(UEEPerkLib.PiercedCrimstone):
            m += self.save.getPerk(UEEPerkLib.PiercedCrimstone).values[0]
        #//Subtract points for Icestone!
        #min -= perkv1(PerkLib.PiercedIcestone);
        if self.save.hasPerk(UEEPerkLib.PiercedIcestone):
            m -= self.save.getPerk(UEEPerkLib.PiercedIcestone).values[0]
        #min += perkv1(PerkLib.PentUp);
        if self.save.hasPerk(UEEPerkLib.PentUp):
            m += self.save.getPerk(UEEPerkLib.PentUp).values[0]


        #//Cold blooded perk reduces min lust, to a minimum of 20! Takes effect after piercings. This effectively caps minimum lust at 80.
        #if (hasPerk(PerkLib.ColdBlooded)) {
        if self.save.hasPerk(UEEPerkLib.ColdBlooded):
            #if (min >= 20) {
            if m >= 20:
                #if (min <= 40) min -= (min - 20);
                if m <= 40:
                    m -= (m - 20)
                #else min -= 20;
                else:
                    m -= 20
            #}
            #minCap -= 20;
            minCap -= 20
        #}

        #//Harpy Lipstick status forces minimum lust to be at least 50.
        #if (min < 50 && hasStatusEffect(StatusEffects.Luststick)) min = 50;
        if self.save.hasStatusEffect('Luststick'):
            m = max(50, m)
        #//SHOULDRA BOOSTS
        #//+20
        #if (flags[kFLAGS.SHOULDRA_SLEEP_TIMER] <= -168 && flags[kFLAGS.URTA_QUEST_STATUS] != 0.75) {
        sst = self.save.flags.get(UEEKFlags.SHOULDRA_SLEEP_TIMER, 0)
        if sst <= -168 and self.save.flags.get(UEEKFlags.URTA_QUEST_STATUS, 0) != 0.75:
            #min += 20;
            m += 20
            #if (flags[kFLAGS.SHOULDRA_SLEEP_TIMER] <= -216) min += 30;
            if sst <= -216:
                m += 30

        #//SPOIDAH BOOSTS
        # eggs() is basically: (slightly optimized)
        eggs = -1
        if self.save.hasPerk(UEEPerkLib.SpiderOvipositor):
            eggs = self.save.getPerk(UEEPerkLib.SpiderOvipositor).values[0]
        if self.save.hasPerk(UEEPerkLib.BeeOvipositor):
            eggs = self.save.getPerk(UEEPerkLib.BeeOvipositor).values[0]

        #if (eggs() >= 20) {
        if eggs >= 20:
            #min += 10;
            m += 10
            #if (eggs() >= 40) min += 10;
            if eggs >= 40:
                m += 10
        #}
        #//Jewelry effects
        #if (jewelryEffectId == JewelryLib.MODIFIER_MINIMUM_LUST)
        #{
        if self.save.jewelry.effect.id == UEEJewelryModifiers.MINIMUM_LUST:
            #min += jewelryEffectMagnitude;
            jewelryEffectMagnitude = self.save.jewelry.effect.magnitude
            m += jewelryEffectMagnitude
            #if (min > (minCap - jewelryEffectMagnitude) && jewelryEffectMagnitude < 0)
            #{
            if m > (minCap - jewelryEffectMagnitude) and jewelryEffectMagnitude < 0:
                #minCap += jewelryEffectMagnitude;
                minCap += jewelryEffectMagnitude
            #}
        #}
        #if (min < 30 && armorName == "lusty maiden's armor") min = 30;
        if m < 30 and self.save.armor.id == UEEItemLib.LMARMOR:
            m = 30
        #if (min < 20 && armorName == "tentacled bark armor") min = 20;
        if m < 20 and self.save.armor.id == UEEItemLib.TBARMOR:
            m = 20
        #//Constrain values
        #if (min < 0) min = 0;
        #if (min > 95) min = 95;
        #if (min > minCap) min = minCap;
        #return min;
        return min(min(max(m, 0), 95), minCap)


    def _get_hp_max_unmod(self) -> float:
        # Creature.maxHP:
        #var max:Number = 0;
        m = 0
        #max += int(tou * 2 + 50);
        m += int((self.save.stats.toughness.value * 2) + 50)
        #if (findPerk(PerkLib.Tank) >= 0) max += 50;
        if self.save.hasPerk(UEEPerkLib.Tank):
            m += 50
        #if (findPerk(PerkLib.Tank2) >= 0) max += Math.round(tou);
        if self.save.hasPerk(UEEPerkLib.Tank2):
            m += round(self.save.stats.toughness.value)
        #if (findPerk(PerkLib.Tank3) >= 0) max += level * 5;
        if self.save.hasPerk(UEEPerkLib.Tank3):
            m += self.save.level * 5
        #if (findPerk(PerkLib.ChiReflowDefense) >= 0) max += UmasShop.NEEDLEWORK_DEFENSE_EXTRA_HP;
        if self.save.hasPerk(UEEPerkLib.ChiReflowDefense):
            m += 50
        #if (flags[kFLAGS.GRIMDARK_MODE] >= 1)
        if self.save.getFlag(UEEKFlags.GRIMDARK_MODE) >= 1:
            #max += level * 5;
            m += self.save.level * 5
        else:
            #max += level * 15;
            m += self.save.level * 15
        #if (jewelryEffectId == JewelryLib.MODIFIER_HP) max += jewelryEffectMagnitude;
        if self.save.raw['jewelryEffectId'] == UEEJewelryModifiers.HP:
            m += self.save.raw['jewelryEffectMagnitude']
        #max *= 1 + (countCockSocks("green") * 0.02);
        m *= 1 + (self.save.countCockSocks('green') * 0.02)
        #max = Math.round(max);
        #if (max < 50) max = 50;
        #if (max > 9999) max = 9999;
        #return max;
        return min(max(round(m), 50), 9999)


    def _get_max_fatigue(self) -> float:
        #Creature.maxFatigue()

        #var max:Number = 100;
        m = 100
        #if (findPerk(PerkLib.ImprovedEndurance) >= 0) max += 20;
        if self.save.hasPerk(UEEPerkLib.ImprovedEndurance):
            m += 20
        #if (findPerk(PerkLib.ImprovedEndurance2) >= 0) max += 10;
        if self.save.hasPerk(UEEPerkLib.ImprovedEndurance2):
            m += 10
        #if (findPerk(PerkLib.ImprovedEndurance3) >= 0) max += 10;
        if self.save.hasPerk(UEEPerkLib.ImprovedEndurance3):
            m += 10
        #if (findPerk(PerkLib.AscensionEndurance) >= 0) max += perkv1(PerkLib.AscensionEndurance) * 5;
        if self.save.hasPerk(UEEPerkLib.AscensionEndurance):
            m += self.save.getPerk(UEEPerkLib.AscensionEndurance).values[0]
        #if (max > 999) max = 999;
        #return max;
        return min(m, 999)
