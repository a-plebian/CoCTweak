from coctweak.saves._cock import ECockTypeFlags
from coctweak.saves.uee._base.cock import UEEBaseCock
from coctweak.saves.uee.enums.cocktypes import UEECockTypes

class UEECock(UEEBaseCock):
    # HAS_KNOT: See classes/classes/lists/GenitalLists.as.
    COCK_TYPE_FLAGS = {
        UEECockTypes.DOG:       ECockTypeFlags.HAS_KNOT,
        UEECockTypes.FOX:       ECockTypeFlags.HAS_KNOT,
        UEECockTypes.WOLF:      ECockTypeFlags.HAS_KNOT,
        UEECockTypes.DRAGON:    ECockTypeFlags.HAS_KNOT,
        UEECockTypes.DISPLACER: ECockTypeFlags.HAS_KNOT,
    }

    def __init__(self):
        super().__init__()

    def getCockTypeStr(self):
        return UEECockTypes(self.type).name

    def serialize(self) -> dict:
        return super().serialize()
