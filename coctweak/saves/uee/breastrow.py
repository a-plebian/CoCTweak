from coctweak.saves.uee._base.breastrow import UEEBaseBreastRow

class UEEBreastRow(UEEBaseBreastRow):
    def getDisplayEntries(self) -> dict:
        o = super().getDisplayEntries()
        if self.nippleCocks:
            o['Nipple Cocks'] = 'yes'
        return o
