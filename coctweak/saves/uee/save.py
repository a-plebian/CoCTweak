from coctweak.saves._save import BaseSave
from coctweak.saves._storage import Storage
from coctweak.saves.vanilla.corestats import VanillaCoreStats
from coctweak.saves.uee.enums.kflags import UEEKFlags

from coctweak.saves.uee.item import UEEItem
from coctweak.saves.uee.breastrow import UEEBreastRow
from coctweak.saves.uee.ass import UEEAss
from coctweak.saves.uee.cock import UEECock
from coctweak.saves.uee.vagina import UEEVagina

from coctweak.saves.uee.editordata import UEECoCTweakData
from coctweak.saves.uee.combatstats import UEECombatStats

from coctweak.saves.uee.wearableslot import UEEWearableSlot

from .serialization import UEESerializationVersion
from .enums.perklib import UEEPerkLib
from coctweak.logging import getLogger
log = getLogger(__name__)
class UEESave(BaseSave):
    CORE_STATS_TYPE = VanillaCoreStats
    NAME = 'UEE'
    VERSION = '1.0.2_mod_1.4.18b'

    ENUM_FLAGS = UEEKFlags
    COMBAT_STATS_TYPE = UEECombatStats

    ASS_TYPE = UEEAss
    BREASTROW_TYPE = UEEBreastRow
    COCK_TYPE = UEECock
    VAGINA_TYPE = UEEVagina

    ITEM_TYPE = UEEItem

    COCTWEAKDATATYPE = UEECoCTweakData

    ITEMTYPES_FILE = 'data/uee/items-merged.min.json'
    PERKTYPES_FILE = 'data/uee/perks-merged.min.json'
    STATUSEFFECTTYPES_FILE = 'data/uee/statuseffects-merged.min.json'
    MAX_INVENTORY_SLOTS = 10

    WEARABLESLOT_TYPE = UEEWearableSlot

    SERIALIZATION_STAMP = UEESerializationVersion('c9ae43aa-e353-472a-a691-010f6ab68191', 0, 1)
    INVENTORY_VERSION = UEESerializationVersion('230d7e43-bbc6-4c25-bd76-1f1175a0c58e', 0, 1)
    def __init__(self):
        super().__init__()

        self.hunger = 0

        self.jewelry: WearableSlot = self.WEARABLESLOT_TYPE(self, 'jewelry', 'Jewelry', "nothing")
        self.wearableslots += [self.jewelry]

    def getDifficulty(self):
        difficulty = self.flags.get(UEEKFlags.GAME_DIFFICULTY)
        ezmode = self.flags.get(UEEKFlags.EASY_MODE_ENABLE_FLAG) == 1
        if difficulty is None or difficulty == 0:
            return 'Easy' if ezmode else 'Normal'
        elif difficulty == 1:
            return 'Hard'
        elif difficulty == 2:
            return 'Nightmare'
        elif difficulty == 3:
            return 'EXTREME'

    def serialize(self):
        data = super().serialize()
        data['hunger'] = self.hunger
        return data

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)

        self.hunger = self._getInt('hunger')

    def unlockAllSlots(self, storage:Storage) -> None:
        for x in storage.slots:
            x.unlocked = True
    def lockAllSlots(self, storage:Storage) -> None:
        for x in storage.slots:
            x.unlocked = False


    def calcSlotLocks(self, verbose: bool=False) -> int:
        '''
        public function getMaxSlots():int {
            var slots:int = 3;
            if (player.findPerk(PerkLib.StrongBack) >= 0) slots++;
            if (player.findPerk(PerkLib.StrongBack2) >= 0) slots++;
            slots += player.keyItemv1("Backpack");
            //Constrain slots to between 3 and 10.
            if (slots < 3) slots = 3;
            if (slots > 10) slots = 10;
            return slots;
        }
        '''
        unlocked = 3
        if self.hasPerk(UEEPerkLib.StrongBack):
            unlocked += 1
        if self.hasPerk(UEEPerkLib.StrongBack2):
            unlocked += 1
        uis = self.getCoCTweakConfig().unlock_inventory_slots
        if uis is not None:
            if self.getCoCTweakConfig().backup_backpack_size is None:
                self.getCoCTweakConfig().backup_backpack_size = self.getBackpackSize()
            self.setBackpackSize(uis - unlocked)
            if verbose: log.info('Save has an override of %d slots set, made backpack of %d size.', uis, uis-unlocked)
        if self.hasKeyItem('Backpack'):
            unlocked += self.getKeyItem('Backpack').values[0]
        if verbose: log.info('Calculated unlocked slot count: %d', unlocked)
        unlocked = min(max(unlocked, 3), self.MAX_INVENTORY_SLOTS)
        if verbose: log.info('Capped unlocked slot count: %d', unlocked).dedent()
        return unlocked

    def calcFixedInventorySlots(self) -> int:
        '''
		var fixedStorage:int = 4;
		if (player.hasKeyItem("Camp - Chest")) fixedStorage += 6;
		if (player.hasKeyItem("Camp - Murky Chest")) fixedStorage += 4;
		if (player.hasKeyItem("Camp - Ornate Chest")) fixedStorage += 4;
        '''
        o = 4
        if self.hasKeyItem('Camp - Chest'):
            o += 6
        if self.hasKeyItem('Camp - Murky Chest'):
            o += 4
        if self.hasKeyItem('Camp - Ornate Chest'):
            o += 4
        return o

    def unlockNumSlots(self, storage: Storage, n: int, warn_on_changes: bool = False) -> None:
        for i in range(len(storage.slots)):
            x = storage.slots[i]
            u = x.unlocked
            x.unlocked = n > 0
            if u != x.unlocked:
                status = 'unlocked' if u else 'locked'
                if warn_on_changes:
                    log.warning('Inventory slot #%d was incorrectly %s. Fixed.', i+1, status)
            n -= 1


    def deserializeChestInventory(self, data: dict, root_data: dict = None) -> None:
        root = root_data if root_data is not None else self.raw
        if root_data is not None:
            data = root_data['inventory']['itemStorage']

        self.chestStorage.deserialize(data)
        if 'itemStorage' in root:
            with log.warning('Discovered old itemStorage chest inventory. Migrating...'):
                n = 0
                for slot in root['itemStorage']:
                    n += 1
                    if slot.get('quantity', 0) > 1:
                        self.chestStorage.add(slot['id'] if 'id' in slot else slot['shortName'], slot['quantity'])
                    else:
                        log.info(f'Slot #{n}: {slot!r}')
            del root['itemStorage']

    def deserializeInventoryObject(self, data: dict, root_data: dict = None) -> None:
        root = root_data if root_data is not None else self.raw
        if root_data is not None:
            data = root_data['inventory']

        #assert data['serializationVersionDictionary'] == self.INVENTORY_VERSION, 'Cannot deserialize inventory: Serialization version changed, so we will not intepret it correctly.'
        self.INVENTORY_VERSION.deserialize(None, data, objname=self.__class__.__name__+'.inventory')

        #print(repr(data))
        self.deserializeChestInventory(data["itemStorage"])

    def loadInventory(self):
        '''
        inventory:
          itemStorage: []
          serializationVersionDictionary:
            230d7e43-bbc6-4c25-bd76-1f1175a0c58e: 1
        '''

        MOD_SAVE_VERSION = self.flags.get(UEEKFlags.MOD_SAVE_VERSION, 0)
        #print(f'MOD_SAVE_VERSION={MOD_SAVE_VERSION}')
        if MOD_SAVE_VERSION >= 16:
            self.deserializeInventoryObject(self.raw['inventory'])

            for storage in self.gearStorage:
                storage.deserialize(self.raw['gearStorage'])

            self.playerStorage.deserialize(self.raw['itemSlots'])
        else:
            super().loadInventory()
        return

    def saveInventory(self, data: dict):
        self.unlockNumSlots(self.playerStorage, self.calcSlotLocks(), False)
        self.unlockNumSlots(self.chestStorage, self.calcFixedInventorySlots(), False)
        
        MOD_SAVE_VERSION = self.flags.get(UEEKFlags.MOD_SAVE_VERSION, 0)
        if MOD_SAVE_VERSION >= 16:
            for storage in self.gearStorage:
                storage.serialize(data['gearStorage'])
            data['inventory']={
                'itemStorage':self.chestStorage.serialize(),
            }
            data['inventory'] = self.INVENTORY_VERSION.serialize(None, data['inventory'])
            self._fixRemoveKey(data, 'itemStorage', 'Old itemStorage inventory structure')
            data['itemSlots'] = self.playerStorage.serialize()
        else:
            super().saveInventory(data)
        return
