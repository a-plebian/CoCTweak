from coctweak.saves.uee._base.ass import UEEBaseAss
from coctweak.saves.uee.enums.ass_looseness import UEEAssLooseness
from coctweak.saves.uee.enums.ass_wetness import UEEAssWetness
from coctweak.saves.uee.enums.ass_ratings import UEEAssRatings

class UEEAss(UEEBaseAss):
    ENUM_LOOSENESS = UEEAssLooseness
    ENUM_WETNESS = UEEAssWetness
    ENUM_RATINGS = UEEAssRatings

    def __init__(self):
        super().__init__()
