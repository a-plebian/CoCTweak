#from .save import UEESave
from .enums.perklib import UEEPerkLib
from .enums.itemlib import UEEItemLib
from .enums.kflags import UEEKFlags

# I may move this into a utils file.
class UEEBimboProgress(object):
    @staticmethod
    def ableToProgress(save: 'UEESave') -> bool:
        #if ((player.hasPerk(PerkLib.BimboBrains)) && (player.hasPerk(PerkLib.BimboBody))) return false;
        if save.hasPerk(UEEPerkLib.BimboBrains) and save.hasPerk(UEEPerkLib.BimboBody):
            return False
        #if (player.lowerGarment != UndergarmentLib.NOTHING) return false;
        if save.lowerGarment.id != UEEItemLib.NONE:
            return False
        #if (player.armorName == "bimbo skirt" && flags[kFLAGS.BIMBO_MINISKIRT_PROGRESS_DISABLED] == 0) return true;
        if save.lowerGarment.id == UEEItemLib.BIMBOSK and save.flags.get(UEEKFlags.BIMBO_MINISKIRT_PROGRESS_DISABLED, 0) == 0:
            return True
        return False
