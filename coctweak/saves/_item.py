from coctweak.saves.vanilla._base.item import BaseItem

from coctweak.logging import getLogger
log = getLogger(__name__)

class Item(BaseItem):
    DEFAULT_VALUE = 6
    def __init__(self):
        super().__init__()
        #self.id: str = ''
        #self.quantity: int = 0
        #self.damage: int = 0

        self.libraw: dict={}
        self.durability: int = 0
        self.value: int = 0
        self.max_stack: int = 0
        self.category: str = ''

    def serialize(self) -> dict:
        o = super().serialize()
        o['id'] = o['id'] if self.quantity > 0 else 'NOTHING!'
        return o

    def _getLibInt(self, key: str, default: int=0) -> int:
        return int(self.libraw.get(key, default))
    def _getLibStr(self, key: str, default: str='') -> str:
        return self.libraw.get(key, default)

    def from_items_json(self, data):
        self.libraw = data
        self.value      = self._getLibInt('value',        self.DEFAULT_VALUE)
        self.max_stack  = self._getLibInt('maxStackSize', 5)
        self.durability = self._getLibInt('durability',   -1)
        self.category   = self._getLibStr('category',     'UNKNOWN')

    def can_sort(self):
        return True

    def cloneMutables(self, template):
        self.raw = template.raw

        self.id = template.id
        self.quantity = template.quantity
        #self.damage = template.damage

        self.libraw = template.libraw
        self.durability = template.durability
        self.value = template.value
        self.max_stack = template.max_stack
        self.category = template.category

    def clone(self):
        i = self.__class__()
        i.cloneMutables(self)
        return i

    def is_empty(self):
        return self.quantity == 0 or self.id.lower() in ('', 'nothing!', 'none')

    def set_empty(self):
        self.raw = {}
        self.id = 'NOTHING!'
        self.quantity = 0
        #self.damage = 0

        self.libraw = {}
        self.value = 0
        self.max_stack = 0
        self.durability = 0
        self.category = ''

    def formatValuesForDisplay(self, name: str, quantity: int, extra: dict) -> str:
        o = '[Empty]'.center(15)
        if not self.is_empty():
            qstr = str(quantity).ljust(2)
            o = f'{name.rjust(10)} x {qstr}'
        if len(extra.keys()) > 0:
            el=[]
            for k,v in extra.items():
                el += [f'{k}: {v}']
            o += ' - '+(', '.join(el))
        return o

    def displayValues(self, extra=None, hide_locked=False):
        if extra is None:
            extra = {}
        
        return self.formatValuesForDisplay(self.id, self.quantity, extra)


    def __str__(self):
        return f'{self.id!r} x {self.quantity!r}: {self.damage!r}'
