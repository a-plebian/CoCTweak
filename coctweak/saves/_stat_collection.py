from coctweak.saves._saveobj import SaveObject
from coctweak.saves._stat import Stat

class StatCollection(SaveObject):
    '''
    A collection of Stats, each with minimums, maximums, and default valuesself.

    Used for Core Stats and Combat Stats.
    '''
    def __init__(self, save):
        self.save = save
        self.all = []
        self.allByID = {}

        self.init()

    def init(self):
        pass

    def _add(self, stat):
        self.all.append(stat)
        self.allByID[stat.id]=stat
        return stat

    def get(self, statID):
        k = statID.lower()
        for stat in self.all:
            if k == stat.id or k == stat.name.lower():
                return stat
        return None

    def has(self, statID):
        return self.get(statID) != None

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)

        for stat in self.all:
            stat.value = self._getFloat(stat.id)

    def serialize(self) -> dict:
        data = super().serialize()

        for stat in self.all:
            data[stat.id] = stat.value

        return data
