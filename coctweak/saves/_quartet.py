from typing import Any, List
from coctweak.saves._saveobj import SaveObject

from coctweak.logging import getLogger
log = getLogger(__name__)
class Quartet(SaveObject):
    '''
    An object that contains four Numbers.

    Used in KeyItems, StatusEffects, and Perks.
    '''

    '''
    Name of the dict item that is the unique ID of this thing.
    '''
    ID_KEY = ''

    def __init__(self):
        super().__init__()
        self.id: str = ''
        self.values: List[Any] = [0]*4

    def serialize(self) -> dict:
        o = {
            self.ID_KEY: self.id,
            #'values': self.values
        }
        for i in range(4):
            o[f'value{i+1}']=self.values[i]
        return o

    def deserialize(self, data):
        self.id = data[self.ID_KEY]
        self.values = [data['value'+str(i+1)] for i in range(4)]

    def displayValues(self):
        return repr(self.values)

    def __str__(self):
        return f'{self.id!r}: {self.values!r}'

class QuartetWithMeta(Quartet):
    '''
    A quartet with associated metadata.
    '''

    FLAG_EXPLAINS = {
        'merge-flags': None,
        'fully-mapped': 'Full support by CoCTweak',
        'values-unused': 'Values Unused',
        'coctweak-internal': 'COCTWEAK INTERNAL USE ONLY'
    }

    def __init__(self):
        super().__init__()
        self._metaLoaded:bool = False
        self.description: str = []
        self.flags: List[str] = []
        self.meanings: List[str] = [f'Value {i+1}' for i in range(4)]

    def load_from_meta(self, data: dict):
        self.flags = data.get('flags', [])
        self.description = data.get('desc', None)
        self.meanings = data.get('values', [f'Value {i+1}' for i in range(4)])
        self._metaLoaded = True

    def longPrintValues(self) -> None:
        with log.info(f'{self.id}'):
            if self.description is not None:
                log.info(self.description)
        with log.info('Flags:'):
            for f in sorted(map(lambda x: f'{self.FLAG_EXPLAINS.get(x, "UNKNOWN")} ({x})', self.flags)):
                if 'coctweak' in f:
                    log.warning(f)
                else:
                    log.info(f)
        with log.info('Values:'):
            for i in range(4):
                meaning = self.meanings[i] if i < len(self.meanings) else None
                if meaning is None and 'fully-mapped' in self.flags:
                    meaning = 'Unused'
                if meaning is not None:
                    log.info(f'value{i+1} ({meaning}): {self.values[i]!r}')
                else:
                    log.info(f'value{i+1}: {self.values[i]!r}')
