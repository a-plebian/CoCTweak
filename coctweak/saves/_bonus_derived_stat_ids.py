# @GENERATED 2019-07-11T23:44:18.341291 from HGG
from enum import Enum

class BaseBonusDerivedStatIDs(Enum):
    dodge               = "Dodge Chance"
    spellMod            = "Spell Mod"
    critC               = "Critical Chance"
    critD               = "Critical Damage"
    maxHealth           = "Max Health"
    spellCost           = "Spell Cost"
    accuracy            = "Accuracy"
    physDmg             = "Physical Damage"
    healthRegenPercent  = "Health Regen(%)"
    healthRegenFlat     = "Health Regen(Flat)"
    minLust             = "Minimum Lust"
    lustRes             = "Lust Resistance"
    seduction           = "Tease Chance"
    sexiness            = "Tease Damage"
    attackDamage        = "Attack Damage"
    globalMod           = "Global Damage"
    weaponDamage        = "Weapon Damage"
    fatigueMax          = "Max Fatigue"
    damageMulti         = "Damage Multiplier"
