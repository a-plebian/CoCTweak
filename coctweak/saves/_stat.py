from ._saveobj import SaveObject
from enum import IntEnum
from typing import Callable

class StatHealType(IntEnum):
    NONE = 0
    MIN = 1
    MAX = 2

class Stat(SaveObject):
    def __init__(self, _id: str, name: str, defaultValue: float, mincb: Callable[[], float], maxcb: Callable[[], float], healtype=StatHealType.NONE):
        super().__init__()
        self.id = _id
        self.name = name
        self._value = defaultValue

        self.mincb = mincb
        self.maxcb = maxcb

        self.healtype = healtype

    def get_value(self):
        return self._value

    def set_value(self, value):
        self._value = value

    value = property(get_value, set_value)

    @property
    def min(self) -> float:
        return self.mincb()

    @property
    def max(self) -> float:
        return self.maxcb()

    def heal(self) -> None:
        if self.healtype == StatHealType.NONE:
            return
        self._value = self.min if self.healtype == StatHealType.MIN else self.max
