from coctweak.saves._stat_collection import StatCollection
from coctweak.saves._stat import Stat

class BaseCoreStats(StatCollection):
    def __init__(self, save):
        self.corruption: Stat  = None
        self.intellect: Stat   = None
        self.libido: Stat      = None
        self.sensitivity: Stat = None
        self.speed: Stat       = None
        self.strength: Stat    = None
        self.toughness: Stat   = None
        super().__init__(save)

    def init(self):
        self.corruption  = self._add(Stat('cor',  'Corruption',  0.0, lambda: 0, lambda: 100))
        self.intellect   = self._add(Stat('inte', 'Intellect',   0.0, lambda: 0, lambda: 100))
        self.libido      = self._add(Stat('lib',  'Libido',      0.0, lambda: 0, lambda: 100))
        self.sensitivity = self._add(Stat('sens', 'Sensitivity', 0.0, lambda: 0, lambda: 100))
        self.speed       = self._add(Stat('spe',  'Speed',       0.0, lambda: 0, lambda: 100))
        self.strength    = self._add(Stat('str',  'Strength',    0.0, lambda: 0, lambda: 100))
        self.toughness   = self._add(Stat('tou',  'Toughness',   0.0, lambda: 0, lambda: 100))
