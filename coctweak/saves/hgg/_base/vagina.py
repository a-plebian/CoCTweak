# @GENERATED from https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/Vagina.as
from coctweak.saves._vagina import Vagina
from coctweak.saves.hgg.serialization import HGGSerializationVersion

__ALL__=['HGGBaseVagina']

class HGGBaseVagina(Vagina):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self):
        super().__init__()
        self.clitLength: float = 0.0
        self.recoveryProgress: int = 0
    def serialize(self) -> dict:
        data = super().serialize()
        data["clitLength"] = self.clitLength
        data["recoveryProgress"] = self.recoveryProgress
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.clitLength = self._getFloat("clitLength", 0.0)
        self.recoveryProgress = self._getInt("recoveryProgress", 0)
