# @GENERATED from https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/ItemSlot.as
from coctweak.saves._item import Item

__ALL__=['HGGBaseItem']

class HGGBaseItem(Item):
    def __init__(self):
        super().__init__()
        self.damage: int = 0
    def serialize(self) -> dict:
        data = super().serialize()
        data["damage"] = self.damage
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        self.damage = self._getInt("damage", 0)
