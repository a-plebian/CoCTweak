# @GENERATED from https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/Cock.as
from coctweak.saves._cock import Cock
from coctweak.saves.hgg.serialization import HGGSerializationVersion

__ALL__=['HGGBaseCock']

class HGGBaseCock(Cock):
    SERIALIZATION_STAMP = HGGSerializationVersion((0, 1))
    def __init__(self):
        super().__init__()
    def serialize(self) -> dict:
        data = super().serialize()
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
