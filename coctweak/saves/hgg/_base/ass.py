# @GENERATED from https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/Ass.as
from coctweak.saves._ass import Ass

__ALL__=['HGGBaseAss']

class HGGBaseAss(Ass):
    def __init__(self):
        super().__init__()
        #self.virgin: bool = False
    def serialize(self) -> dict:
        data = super().serialize()
        #data["virgin"] = self.virgin
        return data
    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        #self.virgin = self._getBool("virgin", False)
