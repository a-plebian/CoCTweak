from typing import Tuple
from coctweak.saves._serialization import SerializationVersionInfo
from coctweak.logging import getLogger
log = getLogger(__name__)

class HGGSerializationVersion(SerializationVersionInfo):
    KEY = 'serializationVersion'
    def __init__(self, versions: Tuple[int, int]):
        self.oldest_permitted_version: int = versions[0]
        self.current_version: int = versions[1]

    def serialize(self, obj, data: dict) -> dict:
        data[self.KEY] = self.current_version
        return data

    def deserialize(self, obj, data: dict) -> None:
        objname = obj.__class__.__name__
        obj.version = 0
        if self.KEY not in data:
            log.warning(f'{objname}: Save serialization version stamp not present in this object.  We could break something by messing with this!')
            return
        v = data[self.KEY]
        assert v >= self.oldest_permitted_version, f'{objname}: Cannot deserialize this object: save serialization version ({v}) < coctweak\'s oldest acceptable version ({self.oldest_permitted_version}), meaning we could corrupt things.'
        assert v <= self.current_version, f'{objname}: Cannot deserialize this object: save serialization version ({v}) > coctweak\'s newest acceptable version ({self.current_version}), meaning it\'s too new for us to read.'
        obj.version = v
