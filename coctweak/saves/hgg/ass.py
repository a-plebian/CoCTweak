from coctweak.saves.hgg._base.ass import HGGBaseAss
from coctweak.saves.hgg.enums.ass_looseness import HGGAssLooseness
from coctweak.saves.hgg.enums.ass_wetness import HGGAssWetness
from coctweak.saves.hgg.enums.ass_ratings import HGGAssRatings

class HGGAss(HGGBaseAss):
    ENUM_LOOSENESS = HGGAssLooseness
    ENUM_WETNESS = HGGAssWetness
    ENUM_RATINGS = HGGAssRatings

    def __init__(self):
        super().__init__()
