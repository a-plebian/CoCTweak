from coctweak.saves._corestats import BaseCoreStats
from coctweak.saves._stat import Stat
class HGGCoreStats(BaseCoreStats):
    def __init__(self, save):
        self.esteem: Stat     = None
        self.femininity: Stat = None
        self.fertility: Stat  = None
        self.obeyance: Stat   = None
        self.will: Stat       = None
        super().__init__(save)

    def init(self):
        super().init()
        self.esteem     = self._add(Stat('esteem',     'Esteem',     0.0, lambda: 0, lambda: 100))
        self.femininity = self._add(Stat('femininity', 'Femininity', 0.0, lambda: 0, lambda: 100))
        self.fertility  = self._add(Stat('fertility',  'Fertility',  0.0, lambda: 0, lambda: 100))
        self.obeyance   = self._add(Stat('obey',       'Obeyance',   0.0, lambda: 0, lambda: 100))
        self.will       = self._add(Stat('will',       'Will',       0.0, lambda: 0, lambda: 100))
