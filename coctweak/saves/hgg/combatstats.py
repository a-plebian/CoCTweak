import math
from coctweak.saves._combatstats import BaseCombatStats
from coctweak.saves.hgg.enums.age import HGGAges
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves._stat import Stat, StatHealType
from coctweak.saves.hgg.enums.jewelrymodifiers import HGGJewelryModifiers
from coctweak.saves.hgg.enums.bonus_derived_stat_ids import HGGBonusDerivedStatIDs
from coctweak.saves.hgg.enums.itemlib import HGGItemLib
from coctweak.saves.hgg.enums.perklib import HGGPerkLib
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.bimboprogress import HGGBimboProgress

class HGGCombatStats(BaseCombatStats):
    def __init__(self, save):
        self.hunger: Stat = None
        super().__init__(save)

    def init(self):
        super().init()
        self.hunger = self._add(Stat('hunger', 'Hunger', 0.0, lambda: 0, self._get_hunger_max, healtype=StatHealType.MAX))

    def _get_hunger_max(self) -> float:
        #https://gitgud.io/BelshazzarII/CoCAnon_mod/blob/master/classes/classes/Character.as: public function maxHunger():Number {
        return 100

    def _get_lust_min(self) -> float:
        #public override function minLust():Number {
        #var min:Number = 0;
        m = 0
        #var minCap:Number = maxLust();
        minCap = self._get_lust_max()

        #//Bimbo body boosts minimum lust by 40
        #if (hasStatusEffect(StatusEffects.BimboChampagne) || hasPerk(PerkLib.BimboBody) || hasPerk(PerkLib.BroBody) || hasPerk(PerkLib.FutaForm)) {
        if self.save.hasStatusEffect(HGGStatusLib.BimboChampagne) or self.save.hasPerk(HGGPerkLib.BimboBody) or self.save.hasPerk(HGGPerkLib.BroBody) or self.save.hasPerk(HGGPerkLib.FutaForm):
            #if (min > 40) min += 10;
            if m > 40:
                m += 10
            #else if (min >= 20) min += 20;
            elif m >= 20:
                m += 20
            #else min += 40;
                m += 40
            #if (armorName == "bimbo skirt") min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST] / 4;
            if self.save.armor.item.id == "BimboSk":
                m += self.save.flags[HGGKFlags.BIMBOSKIRT_MINIMUM_LUST] / 4
        #}
        #else if (game.bimboProgress.ableToProgress()) {
        if HGGBimboProgress.ableToProgress(self.save):
            #if (min > 40) min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST] / 4;
            if m > 40:
                m += self.save.flags.get(HGGKFlags.BIMBOSKIRT_MINIMUM_LUST) / 4
            #else if (min >= 20) min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST] / 2;
            elif m >= 20:
                m += self.save.flags.get(HGGKFlags.BIMBOSKIRT_MINIMUM_LUST) / 2
            #else min += flags[kFLAGS.BIMBOSKIRT_MINIMUM_LUST];
            else:
                m += self.save.flags.get(HGGKFlags.BIMBOSKIRT_MINIMUM_LUST)
        #}
        #//Oh noes anemone!
        #if (hasStatusEffect(StatusEffects.AnemoneArousal)) {
        if self.save.hasStatusEffect(HGGStatusLib.AnemoneArousal):
            #if (min >= 40) min += 10;
            if m >= 40:
                m += 10
            #else if (min >= 20) min += 20;
            elif m >= 20:
                m += 20
            #else min += 30;
            else:
                m += 30

        #if (hasStatusEffect(StatusEffects.Infested)) {
        if self.save.hasStatusEffect(HGGStatusLib.Infested):
            #if (min < 50) min = 50;
            m = max(50, m)

        #if (hasStatusEffect(StatusEffects.ParasiteSlug)) {
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteSlug):
            #if (min < 50) min += 10;
            if m < 50:
                m += 10
            #else min += 5;
            else:
                m += 5

        #if (hasStatusEffect(StatusEffects.ParasiteSlugReproduction)) {//Hard to ignore.
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteSlugReproduction):
            #if (min < 80) min = 80;
            m = max(80, m)

        #if (hasPerk(PerkLib.ParasiteMusk)) {//Just like worms, but less gross!
        if self.save.hasPerk(HGGPerkLib.ParasiteMusk):
            #if (min < 50) min = 50;
            m = max(50, m)

        #if (hasStatusEffect(StatusEffects.ParasiteEelNeedCum)) {
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteEelNeedCum):
            #min += 5 * statusEffectv3(StatusEffects.ParasiteEelNeedCum);
            m += 5 * self.save.getStatusEffect(HGGStatusLib.ParasiteEelNeedCum).values[2]

        #if (hasStatusEffect(StatusEffects.ParasiteNephilaNeedCum)) {
        if self.save.hasStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum):
            #min += 5 * statusEffectv3(StatusEffects.ParasiteNephilaNeedCum);
            m += 5 * self.save.getStatusEffect(HGGStatusLib.ParasiteNephilaNeedCum).values[2]

        #//Harpy Lipstick status forces minimum lust to be at least 50.
        #if (min < 50 && hasStatusEffect(StatusEffects.Luststick)) min = 50;
        if self.save.hasStatusEffect(HGGStatusLib.Luststick):
            m = max(50, m)

        #//SHOULDRA BOOSTS
        #//+20
        #if (flags[kFLAGS.SHOULDRA_SLEEP_TIMER] <= -168 && flags[kFLAGS.URTA_QUEST_STATUS] != 0.75) {
        sst = self.save.flags.get(HGGKFlags.SHOULDRA_SLEEP_TIMER, 0)
        if sst <= -168 and self.save.flags.get(HGGKFlags.URTA_QUEST_STATUS, 0) != 0.75:
            #min += 20;
            m += 20
            #if (flags[kFLAGS.SHOULDRA_SLEEP_TIMER] <= -216) min += 30;
            if sst <= -216:
                m += 30

        #//SPOIDAH BOOSTS
        # eggs() is basically: (slightly optimized)
        eggs = -1
        if self.save.hasPerk(HGGPerkLib.SpiderOvipositor):
            eggs = self.save.getPerk(HGGPerkLib.SpiderOvipositor).values[0]
        if self.save.hasPerk(HGGPerkLib.BeeOvipositor):
            eggs = self.save.getPerk(HGGPerkLib.BeeOvipositor).values[0]
        #if (eggs() >= 20) {
        if eggs >= 20:
            #min += 10;
            m += 10
            #if (eggs() >= 40) min += 10;
            if eggs >= 40:
                m += 10
        #}
        #if (min < 30 && armorName == "lusty maiden's armor") min = 30;
        if m < 30 and self.save.armor.id == HGGItemLib.LMARMOR:
            m = 30
        #if (min < 20 && armorName == "tentacled bark armor") min = 20;
        if m < 20 and self.save.armor.id == HGGItemLib.TBARMOR:
            m = 20

        m = self.save.bonuses._getMinLust(m)

        #if hasPerk(PerkLib.ColdBlooded)) {
        if self.save.hasPerk(HGGPerkLib.ColdBlooded):
            #min = Math.max(min - 20, 20);
            m = max(m - 20, 20)
            #minCap -= 20;
            minCap -= 20
        #}
        #//Constrain values
        #if (min < 0) min = 0;
        #if (min > minCap) min = minCap;
        m = min(max(m, 0), minCap)
        return m


    def _get_hp_max_unmod(self) -> float:
        # https://gitgud.io/BelshazzarII/CoCAnon_mod/blob/master/classes/classes/Creature.as: public function maxHPUnmodified():Number {

        #var max:Number = 0;
        m = 0

        #max += int(tou * 2 + 50);
        m += int((self.save.stats.toughness.value * 2) + 50)

        #if (isChild()) max -= 15;
        if self.save.age == HGGAges.CHILD:
            m -= 15

        #if (hasPerk(PerkLib.Tank)) max += 50;
        if self.save.hasPerk(HGGPerkLib.Tank):
            m += 50

        #if (hasPerk(PerkLib.Tank2)) max += Math.round(tou);
        if self.save.hasPerk(HGGPerkLib.Tank2):
            m += round(self.save.stats.toughness.value)

        #if (hasPerk(PerkLib.Tank3)) max += level * 5;
        if self.save.hasPerk(HGGPerkLib.Tank3):
            m += self.level.value * 5

        #if (flags[kFLAGS.GRIMDARK_MODE] >= 1) max += level * 5;
        #else max += level * 15;
        if self.save.flags.get(HGGKFlags.GRIMDARK_MODE, 0) >= 1:
            m += self.level.value * 5
        else:
            m += self.level.value * 15

        #max += getBonusStat(BonusDerivedStats.maxHealth);
        m += self.save.bonuses.get('Max Health')

        #if (hasPerk(PerkLib.ChiReflowDefense)) max += UmasShop.NEEDLEWORK_DEFENSE_EXTRA_HP;
        if self.save.hasPerk(HGGPerkLib.ChiReflowDefense):
            m += 50

        #if (jewelryEffectId == JewelryLib.MODIFIER_HP) max += jewelryEffectMagnitude;
        # We have to figure out what the effect ID is from our worn jewelry.
        if self.save.jewelry.effect.id == HGGJewelryModifiers.HP:
            m += self.save.jewelry.effect.magnitude

        #max *= 1 + (countCockSocks("green") * 0.02);
        m += 1 + (self.save.countCockSocks('green') * 0.02)

        #if (isChild() || isElder()) max *= 0.8;
        if self.save.age in (HGGAges.CHILD, HGGAges.ELDER):
            m *= 0.08

        #max *= getBonusStatMultiplicative(BonusDerivedStats.maxHealth);
        m *= self.save.bonuses.getMultiplicative(HGGBonusDerivedStatIDs.maxHealth)

        #if (hasStatusEffect(StatusEffects.Soulburst)) max = max / Math.pow(2, statusEffectv1(StatusEffects.Soulburst));
        if self.save.hasStatusEffect(HGGStatusLib.Soulburst):
            m /= pow(2, self.save.statusEffects[HGGStatusLib.Soulburst].values[0])

        return m

    def _get_hp_max(self) -> float:
        # https://gitgud.io/BelshazzarII/CoCAnon_mod/blob/master/classes/classes/Creature.as: public function maxHP():Number {
        #var max:Number = maxHPUnmodified();
        m = self._get_hp_max_unmod()

        #if (hasStatusEffect(StatusEffects.Overhealing)) max += statusEffectv1(StatusEffects.Overhealing);
        if self.save.hasStatusEffect(HGGStatusLib.Overhealing):
            m += self.save.statusEffects[HGGStatusLib.Overhealing].values[0]

        #max = Math.round(max);
        #if (max > 9999) max = 9999;
        m = min(9999, round(m))

        #return max;
        return m


    def _get_max_fatigue(self) -> float:
        # https://gitgud.io/BelshazzarII/CoCAnon_mod/blob/master/classes/classes/Creature.as: public function maxFatigue():Number {:
        #var max:Number = 100;
        m = 100
        #if (findPerk(PerkLib.ImprovedEndurance) >= 0) max += 20;
        if self.save.hasPerk(HGGPerkLib.ImprovedEndurance):
            m += 20
        #if (findPerk(PerkLib.ImprovedEndurance2) >= 0) max += 10;
        if self.save.hasPerk(HGGPerkLib.ImprovedEndurance2):
            m += 10
        #if (findPerk(PerkLib.AscensionEndurance) >= 0) max += perkv1(PerkLib.AscensionEndurance) * 5;
        if self.save.hasPerk(HGGPerkLib.AscensionEndurance):
            m += self.save.perks[HGGPerkLib.AscensionEndurance].values[0] * 5
        #max += getBonusStat(BonusDerivedStats.fatigueMax);
        m += self.save.bonuses.get(HGGBonusDerivedStatIDs.fatigueMax)
        #if (isChild()) max -= 10;
        if self.save.age == HGGAges.CHILD:
            m -= 10
        #if (max > 999) max = 999;
        m = min(m, 999)
        #return max;
        return m
