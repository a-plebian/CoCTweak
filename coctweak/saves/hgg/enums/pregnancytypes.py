# @GENERATED from https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/PregnancyStore.as
from enum import IntEnum

__ALL__ = ['HGGPregnancyType']

class HGGPregnancyType(IntEnum):
    IMP                    = 1
    MINOTAUR               = 2
    COCKATRICE             = 3
    MOUSE                  = 4
    OVIELIXIR_EGGS         = 5
    HELL_HOUND             = 6
    CENTAUR                = 7
    MARBLE                 = 8
    BUNNY                  = 9
    ANEMONE                = 10
    AMILY                  = 11
    IZMA                   = 12
    SPIDER                 = 13
    BASILISK               = 14
    DRIDER_EGGS            = 15
    GOO_GIRL               = 16
    EMBER                  = 17
    BENOIT                 = 18
    SATYR                  = 19
    COTTON                 = 20
    URTA                   = 21
    SAND_WITCH             = 22
    FROG_GIRL              = 23
    FAERIE                 = 24
    PLAYER                 = 25
    BEE_EGGS               = 26
    SANDTRAP_FERTILE       = 27
    SANDTRAP               = 28
    JOJO                   = 29
    KELT                   = 30
    TAOTH                  = 31
    GOO_STUFFED            = 32
    WORM_STUFFED           = 33
    MINERVA                = 34
    BEHEMOTH               = 35
    PHOENIX                = 36
    ANDY                   = 37
    CORRWITCH              = 38
    TENTACLE_BEAST_SEED    = 39
    BUTT_BEE               = 2
    BUTT_DRIDER            = 3
    BUTT_SANDTRAP_FERTILE  = 4
    BUTT_SANDTRAP          = 5
