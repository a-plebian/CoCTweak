# @GENERATED from https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/Ass.as
from enum import IntEnum

__ALL__ = ['HGGAssLooseness']

class HGGAssLooseness(IntEnum):
    VIRGIN     = 0
    TIGHT      = 1
    NORMAL     = 2
    LOOSE      = 3
    STRETCHED  = 4
    GAPING     = 5
