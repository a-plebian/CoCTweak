# @GENERATED from https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/CockTypesEnum.as
from enum import IntEnum

__ALL__ = ['HGGCockTypes']

class HGGCockTypes(IntEnum):
    HUMAN      = 0
    HORSE      = 1
    DOG        = 2
    DEMON      = 3
    TENTACLE   = 4
    CAT        = 5
    LIZARD     = 6
    ANEMONE    = 7
    KANGAROO   = 8
    DRAGON     = 9
    DISPLACER  = 10
    FOX        = 11
    BEE        = 12
    PIG        = 13
    AVIAN      = 14
    RHINO      = 15
    ECHIDNA    = 16
    WOLF       = 17
    RED_PANDA  = 18
    FERRET     = 19
    GNOLL      = 20
    UNDEFINED  = 21
