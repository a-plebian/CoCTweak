# @GENERATED from https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/Vagina.as
from enum import IntEnum

__ALL__ = ['HGGVagTypes']

class HGGVagTypes(IntEnum):
    HUMAN            = 0
    EQUINE           = 1
    BLACK_SAND_TRAP  = 5
