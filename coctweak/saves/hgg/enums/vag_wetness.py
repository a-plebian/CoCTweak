# @GENERATED from https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/Vagina.as
from enum import IntEnum

__ALL__ = ['HGGVagWetness']

class HGGVagWetness(IntEnum):
    DRY        = 0
    NORMAL     = 1
    WET        = 2
    SLICK      = 3
    DROOLING   = 4
    SLAVERING  = 5
