# @GENERATED from https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/BonusDerivedStats.as
from enum import Enum

__ALL__ = ['HGGBonusDerivedStatIDs']

class HGGBonusDerivedStatIDs(Enum):
    dodge                = "Dodge Chance"
    spellMod             = "Spell Mod"
    critC                = "Critical Chance"
    critCWeapon          = "Weapon Critical Chance"
    critD                = "Critical Damage"
    maxHealth            = "Max Health"
    spellCost            = "Spell Cost"
    accuracy             = "Accuracy"
    physDmg              = "Physical Damage"
    healthRegenPercent   = "Health Regen (%)"
    healthRegenFlat      = "Health Regen (Flat)"
    minLust              = "Minimum Lust"
    lustRes              = "Lust Resistance"
    seduction            = "Tease Chance"
    sexiness             = "Tease Damage"
    attackDamage         = "Attack Damage"
    globalMod            = "Global Damage"
    weaponDamage         = "Weapon Damage"
    fatigueMax           = "Max Fatigue"
    damageMulti          = "Damage Multiplier"
    armor                = "Armor"
    goodNegatives        = [spellCost, minLust]
    percentageAdditions  = [dodge, spellMod, critC, critCWeapon, critD, spellCost, accuracy, physDmg, healthRegenPercent, lustRes, attackDamage, globalMod, damageMulti, armor]
