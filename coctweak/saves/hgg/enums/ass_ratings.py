# @GENERATED from https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/BodyParts/Butt.as
from enum import IntEnum

__ALL__ = ['HGGAssRatings']

class HGGAssRatings(IntEnum):
    BUTTLESS           = 0
    TIGHT              = 2
    AVERAGE            = 4
    NOTICEABLE         = 6
    LARGE              = 8
    JIGGLY             = 10
    EXPANSIVE          = 13
    HUGE               = 16
    INCONCEIVABLY_BIG  = 20
