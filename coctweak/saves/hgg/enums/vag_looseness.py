# @GENERATED from https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/Vagina.as
from enum import IntEnum

__ALL__ = ['HGGVagLooseness']

class HGGVagLooseness(IntEnum):
    TIGHT            = 0
    NORMAL           = 1
    LOOSE            = 2
    GAPING           = 3
    GAPING_WIDE      = 4
    LEVEL_CLOWN_CAR  = 5
