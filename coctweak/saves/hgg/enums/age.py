# @GENERATED from https://gitgud.io/BelshazzarII/CoCAnon_mod/raw/master/classes/classes/lists/Age.as
from enum import IntEnum

__ALL__ = ['HGGAges']

class HGGAges(IntEnum):
    ADULT  = 0
    CHILD  = 1
    TEEN   = 2
    ELDER  = 3
