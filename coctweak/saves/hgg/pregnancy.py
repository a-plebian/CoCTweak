from coctweak.saves._pregnancy import VaginalPregnancy, ButtPregnancy
from coctweak.saves.hgg.enums.pregnancytypes import HGGPregnancyType
class HGGVaginalPregnancy(VaginalPregnancy):
    TYPE_ENUM = HGGPregnancyType
class HGGButtPregnancy(ButtPregnancy):
    TYPE_ENUM = HGGPregnancyType
