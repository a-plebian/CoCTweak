from coctweak.saves.hgg._base.item import HGGBaseItem
class HGGItem(HGGBaseItem):
    DEFAULT_VALUE = 6
    MAX_INVENTORY_SLOTS = 10
    def __init__(self):
        super().__init__()
        self.unlocked: bool = False
        self.bonus: str = ''

    def can_sort(self):
        return self.unlocked

    def deserialize(self, data):
        super().deserialize(data)
        self.unlocked = self._getBool('unlocked', False)
        self.bonus = self._getStr('bonus', '')

    def serialize(self):
        o = super().serialize()
        o['unlocked'] = self.unlocked
        if self.bonus != '':
            o['bonus'] = self.bonus
        return o

    def cloneMutables(self, template):
        super().cloneMutables(template)
        #self.unlocked = template.unlocked
        self.bonus = template.bonus

    def cloneImmutables(self, template):
        super().cloneMutables(template)
        self.unlocked = template.unlocked
        #self.bonus = template.bonus

    def displayValues(self, extra=None, hide_locked=False):
        if extra is None:
            extra = {}
        if hide_locked and not self.unlocked:
            return '--LOCKED--'.center(14)
        if self.damage > 0:
            extra['Damage'] = f'{self.damage:g}/{self.durability:g} {(self.damage/self.durability)*100:g}%'
        #extra['Unlocked'] = self.unlocked
        return super().displayValues(extra, hide_locked)
