from coctweak.saves._parasite import Parasite
from typing import List, Dict, Any
from coctweak.saves._statuseffect import StatusEffect
from coctweak.saves._perk import Perk

from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.pregnancytypes import HGGPregnancyType
from coctweak.logging import getLogger
log = getLogger(__name__)

class WormsParasite(Parasite):
    NAME: str = 'Worms'

    STATUS_EFFECTS: List[str] = [HGGStatusLib.WormPlugged.value, HGGStatusLib.Infested.value]
    PERKS: List[str] = []
    KFLAGS: List[int] = [HGGKFlags.PLAYER_PREGGO_WITH_WORMS]

    def removeInfection(self, save, **kwargs) -> None:
        # This is how Jojo does it. Quick and easy.
        self.removeStatusEffect(HGGStatusLib.Infested)

        if self.save.pregnancy.type == HGGPregnancyType.WORM_STUFFED.value:
            # Let's also clear the pregnancy.
            self.removeStatusEffect(HGGStatusLib.WormPlugged)
            with log.info('Clearing pregnancy...'):
                self.save.pregnancy.clear()

    def getOrifaces(self) -> List[str]:
        o = []
        if self.save.hasStatusEffect(HGGStatusLib.WormPlugged):
            o.append('VAGINAL')
        if self.save.hasStatusEffect(HGGStatusLib.Infested):
            o.append('PENILE')
        return o

    def getStats(self) -> List[str]:
        o = []
        if self.save.hasStatusEffect(HGGStatusLib.WormPlugged):
            o += [f'{self.save.pregnancy.incubation}h uterine incubation left']
        if self.save.hasStatusEffect(HGGStatusLib.Infested):
            se = self.save.getStatusEffect(HGGStatusLib.Infested)
            o += [f'stage {se.values[0]+1} penile infection']
        #o += [f'Values: {se.values!r}']
        #o += [f'dataStore: {se.dataStore!r}']
        return o

    def displayState(self) -> None:
        for stat in self.getStats():
            log.info(stat)

    def __str__(self) -> str:
        orifaces = ', '.join(self.getOrifaces())
        stats = ', '.join(self.getStats())
        return f'{self.NAME} ({orifaces}) - {stats}'
