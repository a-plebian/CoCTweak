from coctweak.saves._parasite import Parasite
from typing import List, Dict, Any
from coctweak.saves._statuseffect import StatusEffect
from coctweak.saves._perk import Perk

from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logging import getLogger
log = getLogger(__name__)
class SlugParasite(Parasite):
    NAME: str = 'Slug'

    STATUS_EFFECTS: List[str] = [
        HGGStatusLib.ParasiteSlug.value,
        HGGStatusLib.ParasiteSlugMatureDay.value,
        HGGStatusLib.ParasiteSlugMusk.value,
        HGGStatusLib.ParasiteSlugReproduction.value]
    PERKS: List[str] = []
    KFLAGS: List[int] = []


    def removeInfection(self, save, **kwargs) -> None:
        # This is how Jojo does it. Quick and easy.
        self.removeStatusEffect(save, HGGStatusLib.ParasiteSlug)
