from .worms import WormsParasite
from .eel import EelParasite
from .slug import SlugParasite
__all__ = ['WormsParasite', 'EelParasite', 'SlugParasite']
