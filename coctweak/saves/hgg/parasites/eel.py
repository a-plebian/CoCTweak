from coctweak.saves._parasite import Parasite
from typing import List, Dict, Any
from coctweak.saves._statuseffect import StatusEffect
from coctweak.saves._perk import Perk

from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.enums.perklib import HGGPerkLib
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.logging import getLogger
log = getLogger(__name__)
class EelParasite(Parasite):
    NAME: str = 'Eel'

    STATUS_EFFECTS: List[str] = [
        HGGStatusLib.ParasiteEel.value,
        HGGStatusLib.ParasiteEelNeedCum.value,
        HGGStatusLib.ParasiteEelReproduction.value]
    PERKS: List[str] = [HGGPerkLib.ParasiteQueen.value]
    KFLAGS: List[int] = []

    def removeInfection(self, save, **kwargs) -> None:
        self.removeStatusEffect(save, HGGStatusLib.ParasiteEel)
        if save.hasStatusEffect(HGGStatusLib.ParasiteEelNeedCum):
            self.removeStatusEffect(save, HGGStatusLib.ParasiteEelNeedCum)
        if save.hasStatusEffect(HGGStatusLib.ParasiteEelReproduction):
            self.removeStatusEffect(save, HGGStatusLib.ParasiteEelReproduction)
        if save.hasPerk(HGGPerkLib.ParasiteQueen):
            self.removePerk(save, HGGPerkLib.ParasiteQueen)
