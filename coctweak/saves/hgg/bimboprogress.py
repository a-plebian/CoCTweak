#from .save import HGGSave
from .enums.perklib import HGGPerkLib
from .enums.itemlib import HGGItemLib
from .enums.kflags import HGGKFlags

# I may move this into a utils file.
class HGGBimboProgress(object):
    @staticmethod
    def ableToProgress(save: 'HGGSave') -> bool:
        #if ((player.hasPerk(PerkLib.BimboBrains)) && (player.hasPerk(PerkLib.BimboBody))) return false;
        if save.hasPerk(HGGPerkLib.BimboBrains) and save.hasPerk(HGGPerkLib.BimboBody):
            return False
        #if (player.lowerGarment != UndergarmentLib.NOTHING) return false;
        if save.lowerGarment.id != HGGItemLib.NONE:
            return False
        #if (player.armorName == "bimbo skirt" && flags[kFLAGS.BIMBO_MINISKIRT_PROGRESS_DISABLED] == 0) return true;
        if save.lowerGarment.id == HGGItemLib.BIMBOSK and save.flags.get(HGGKFlags.BIMBO_MINISKIRT_PROGRESS_DISABLED, 0) == 0:
            return True
        return False
