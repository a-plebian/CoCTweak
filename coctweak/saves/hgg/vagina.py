from coctweak.saves.hgg._base.vagina import HGGBaseVagina
from coctweak.saves.hgg.enums.vag_wetness import HGGVagWetness
from coctweak.saves.hgg.enums.vag_looseness import HGGVagLooseness
from coctweak.saves.hgg.enums.vag_types import HGGVagTypes

__ALL__ = ['HGGVagina']

class HGGVagina(HGGBaseVagina):
    ENUM_WETNESS = HGGVagWetness
    ENUM_LOOSENESS = HGGVagLooseness
    ENUM_TYPES = HGGVagTypes
