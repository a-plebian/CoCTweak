from coctweak.saves.hgg._base.cock import HGGBaseCock
from coctweak.saves._cock import ECockTypeFlags
from coctweak.saves.hgg.enums.cocktypes import HGGCockTypes

class HGGCock(HGGBaseCock):
    # HAS_KNOT: See classes/classes/lists/GenitalLists.as.
    COCK_TYPE_FLAGS = {
        HGGCockTypes.DOG:       ECockTypeFlags.HAS_KNOT,
        HGGCockTypes.FOX:       ECockTypeFlags.HAS_KNOT,
        HGGCockTypes.WOLF:      ECockTypeFlags.HAS_KNOT,
        HGGCockTypes.DRAGON:    ECockTypeFlags.HAS_KNOT,
        HGGCockTypes.DISPLACER: ECockTypeFlags.HAS_KNOT,
    }
    def getCockTypeStr(self):
        return HGGCockTypes(self.type).name
