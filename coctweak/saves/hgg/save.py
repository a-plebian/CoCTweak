from enum import IntEnum
from typing import List
import json # Sigh.

from coctweak.saves._save import SaveCaps, BaseSave
from coctweak.saves._storage import Storage

from coctweak.saves.hgg.ass import HGGAss
from coctweak.saves.hgg.cock import HGGCock
from coctweak.saves.hgg.combatstats import HGGCombatStats
from coctweak.saves.hgg.corestats import HGGCoreStats
from coctweak.saves.hgg.editordata import HGGCoCTweakData
from coctweak.saves.hgg.enums.age import HGGAges
from coctweak.saves.hgg.enums.itemlib import HGGItemLib
from coctweak.saves.hgg.enums.kflags import HGGKFlags
from coctweak.saves.hgg.enums.perklib import HGGPerkLib
from coctweak.saves.hgg.enums.statuslib import HGGStatusLib
from coctweak.saves.hgg.globaldata import HGGGlobalData
from coctweak.saves.hgg.item import HGGItem
from coctweak.saves.hgg.breastrow import HGGBreastRow
from coctweak.saves.hgg.parasites import *
from coctweak.saves.hgg.pregnancy import HGGButtPregnancy, HGGVaginalPregnancy
from coctweak.saves.hgg.vagina import HGGVagina
from coctweak.saves.hgg.wearableslot import HGGWearableSlot
from coctweak.saves.uee.save import UEESave

from coctweak.logging import getLogger
log = getLogger(__name__)


class HGGSave(BaseSave):
    NAME = 'HGG'

    GLOBAL_DATA_TYPE = HGGGlobalData
    GLOBAL_DATA_FILENAME = 'CoC_Main'

    CORE_STATS_TYPE = HGGCoreStats
    COMBAT_STATS_TYPE = HGGCombatStats

    ASS_TYPE = HGGAss
    COCK_TYPE = HGGCock
    VAGINA_TYPE = HGGVagina
    ITEM_TYPE = HGGItem
    PREGNANCY_TYPE = HGGVaginalPregnancy
    BUTTPREGNANCY_TYPE = HGGButtPregnancy
    BREASTROW_TYPE = HGGBreastRow

    ENUM_FLAGS = HGGKFlags

    ITEMTYPES_FILE = 'data/hgg/items-merged.min.json'
    PERKTYPES_FILE = 'data/hgg/perks-merged.min.json'
    STATUSEFFECTTYPES_FILE = 'data/hgg/statuseffects-merged.min.json'
    MAX_INVENTORY_SLOTS = 10

    WEARABLESLOT_TYPE = HGGWearableSlot

    CAPABILITIES = SaveCaps.HUNGER | SaveCaps.PARASITES
    PARASITES = [WormsParasite, EelParasite, SlugParasite]

    BAD_STATUS_FLAG = 'monster-dispellable'

    COCTWEAKDATATYPE = HGGCoCTweakData

    def __init__(self):
        super().__init__()

        self.age: HGGAges = HGGAges.ADULT

        self.jewelry: WearableSlot = self.WEARABLESLOT_TYPE(self, 'jewelry', 'Jewelry', "nothing")
        self.wearableslots += [self.jewelry]

    def postStatusEffectLoad(self) -> None:
        self.BAD_STATUSES = [x['id'] for k, x in self.ALL_STATUSEFFECTS.items() if self.BAD_STATUS_FLAG in x['flags']]
        log.debug(f'Loaded {len(self.BAD_STATUSES)} BAD_STATUSES ({self.BAD_STATUS_FLAG})')

    def unlockAllSlots(self, storage:Storage) -> None:
        for x in storage.slots:
            x.unlocked = True

    def lockAllSlots(self, storage:Storage) -> None:
        for x in storage.slots:
            x.unlocked = False

    def calcSlotLocks(self, verbose: bool=False) -> int:
        '''
        public function getMaxSlots():int {
        	var slots:int = 3;
        	if (player.hasPerk(PerkLib.StrongBack)) slots++;
        	if (player.hasPerk(PerkLib.StrongBack2)) slots++;
        	slots += player.keyItemv1("Backpack");
        	if (player.shield.id == shields.CLKSHLD.id) slots += 2;
        	//Constrain slots to between 3 and 10.
        	if (slots < 3) slots = 3;
        	if (slots > 10) slots = 10;
        	return slots;
        }
        '''
        if verbose: log.info('Recalculating inventory slot locks...').indent()
        unlocked = 3 #default
        if self.hasPerk(HGGPerkLib.StrongBack):
            unlocked += 1
        if self.hasPerk(HGGPerkLib.StrongBack2):
            unlocked += 1
        if self.shield.id == HGGItemLib.CLKSHLD.value:
            unlocked += 2
        uis = self.getCoCTweakConfig().unlock_inventory_slots
        if uis is not None:
            if self.getCoCTweakConfig().backup_backpack_size is None:
                self.getCoCTweakConfig().backup_backpack_size = self.getBackpackSize()
            self.setBackpackSize(uis - unlocked)
            if verbose: log.info('Save has an override of %d slots set, made backpack of %d size.', uis, uis-unlocked)
        if self.hasKeyItem('Backpack'):
            unlocked += self.getKeyItem('Backpack').values[0]
        if verbose: log.info('Calculated unlocked slot count: %d', unlocked)
        unlocked = min(max(unlocked, 3), self.MAX_INVENTORY_SLOTS)
        if verbose: log.info('Capped unlocked slot count: %d', unlocked).dedent()
        return unlocked

    def calcFixedInventorySlots(self) -> int:
        '''
		var fixedStorage:int = 4;
		if (player.hasKeyItem("Camp - Chest")) fixedStorage += 6;
		if (player.hasKeyItem("Camp - Murky Chest")) fixedStorage += 4;
		if (player.hasKeyItem("Camp - Ornate Chest")) fixedStorage += 4;
        '''
        o = 4
        if self.hasKeyItem('Camp - Chest'):
            o += 6
        if self.hasKeyItem('Camp - Murky Chest'):
            o += 4
        if self.hasKeyItem('Camp - Ornate Chest'):
            o += 4
        return o

    def unlockNumSlots(self, storage: Storage, n: int, warn_on_changes: bool = False) -> None:
        for i in range(len(storage.slots)):
            x = storage.slots[i]
            u = x.unlocked
            x.unlocked = n > 0
            if u != x.unlocked:
                status = 'unlocked' if u else 'locked'
                if warn_on_changes:
                    log.warning('Inventory slot #%d was incorrectly %s. Fixed.', i+1, status)
            n -= 1

    def getModName(self):
        if self.flags.get(HGGKFlags.MOD_SAVE_VERSION, 0) < 15:
            return 'Revamp'
        return 'HGG'

    def getModVersion(self):
        if self.version == None:
            return 'null (BUG)'
        elif self.version.startswith('hgg '):
            return self.version[4:]
        else:
            return self.version

    def getAgeStr(self):
        return HGGAges(self.age).name

    def deserialize(self, data: dict) -> None:
        super().deserialize(data)
        for gs in self.gearStorage:
            self.unlockAllSlots(gs)
        self.age = self._getInt('age')
        self.unlockNumSlots(self.playerStorage, self.calcSlotLocks(), False)
        self.unlockNumSlots(self.chestStorage, self.calcFixedInventorySlots(), False)

    def serialize(self):
        self.lockAllSlots(self.chestStorage)
        for gs in self.gearStorage:
            self.lockAllSlots(gs)
        #self.unlockNumSlots(self.playerStorage, self.calcSlotLocks(), True)
        self.lockAllSlots(self.playerStorage)

        data = super().serialize()
        data['age'] = self.age
        return data


    def getDifficulty(self):
        difficulty = self.flags.get(HGGKFlags.GAME_DIFFICULTY)
        ezmode = self.flags.get(HGGKFlags.EASY_MODE_ENABLE_FLAG) == 1
        if difficulty is None or difficulty == 0:
            return 'Easy' if ezmode else 'Normal'
        elif difficulty == 1:
            return 'Hard'
        elif difficulty == 2:
            return 'Nightmare'
        elif difficulty == 3:
            return 'EXTREME'

    def countCockSocks(self, sockID):
        o = 0
        for cock in self.raw['cocks']:
            if cock.get('sock') == sockID:
                o += 1
        return o

    def loadInventory(self):
        for storage in self.gearStorage:
            storage.deserialize(self.raw['gearStorage'])
        self.chestStorage.deserialize(self.raw['itemStorage'])
        self.playerStorage.deserialize(self.raw['items'])
        return

    def saveInventory(self, data: dict):
        # BUG: HGG currently locks all slots in the player's inventory.
        self.lockAllSlots(self.playerStorage)

        for storage in self.gearStorage:
            storage.serialize(data['gearStorage'])
        data['itemStorage'] = self.chestStorage.serialize()
        data['items'] = self.playerStorage.serialize()
        return

    def postLoadInventory(self):
        # Make sure we've unlocked what needs to be unlocked.
        # The game currently sets all slots to locked on save, which makes me ask why they store the variable at all.
        self.unlockNumSlots(self.playerStorage, self.calcSlotLocks(), False)
