import unittest
from coctweak.saves.uee.breastrow import UEEBreastRow
class TestUEEBreastRow(unittest.TestCase):
    '''
    breastRating: 0.0
    breasts: 2.0
    fuckable: false
    fullness: 0.0
    lactationMultiplier: 0.0
    milkFullness: 0.0
    nippleCocks: false
    nipplesPerBreast: 1.0
    serializationVersionDictionary:
      c862ee0a-5667-4fd3-a178-37a5e85c86d6: 1
    '''
    ALL_KEYS = [
        'breastRating',
        'breasts',
        'fuckable',
        'fullness',
        'lactationMultiplier',
        'milkFullness',
        'nippleCocks',
        'nipplesPerBreast',
        'serializationVersionDictionary',
    ]
    def test_contains_expected_keys(self):
        actual_keys = UEEBreastRow().serialize().keys()
        for k in self.ALL_KEYS:
            self.assertIn(k, actual_keys)

    def test_doesnt_contain_anything_else(self):
        actual_keys = UEEBreastRow().serialize().keys()

        for k in actual_keys:
            self.assertIn(k, self.ALL_KEYS)

if __name__ == '__main__':
    unittest.main()
