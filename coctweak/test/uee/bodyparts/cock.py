import unittest
from coctweak.saves.uee.cock import UEECock
class TestUEECock(unittest.TestCase):
    '''
    cocks:
    - cockLength: 5.5
      cockThickness: 1
      cockType: 0
      knotMultiplier: 1
      pLongDesc: ''
      pShortDesc: ''
      pierced: 0
      serializationVersionDictionary:
        98367a43-6bf4-4b5c-b509-abea7e416416: 1
      sock: ''
    '''
    ALL_KEYS = [
        'cockLength',
        'cockThickness',
        'cockType',
        'knotMultiplier',
        'pLongDesc',
        'pShortDesc',
        'pierced',
        'serializationVersionDictionary',
        'sock',
    ]
    def test_contains_expected_keys(self):
        actual_keys = UEECock().serialize().keys()
        for k in self.ALL_KEYS:
            self.assertIn(k, actual_keys)

    def test_doesnt_contain_anything_else(self):
        actual_keys = UEECock().serialize().keys()

        for k in actual_keys:
            self.assertIn(k, self.ALL_KEYS)

if __name__ == '__main__':
    unittest.main()
