import unittest
from coctweak.saves.uee.ass import UEEAss
class TestUEEAss(unittest.TestCase):
    '''
    ass:
      analLooseness: 0
      analWetness: 0
      fullness: 0
      serializationVersionDictionary:
        0a3bd267-6ce0-4fed-a81b-53a4ccd6c17d: 1
    '''
    ALL_KEYS = [
        'analLooseness',
        'analWetness',
        'fullness',
        'serializationVersionDictionary'
    ]
    def test_contains_expected_keys(self):
        actual_keys = UEEAss().serialize().keys()
        for k in self.ALL_KEYS:
            self.assertIn(k, actual_keys)

    def test_doesnt_contain_anything_else(self):
        actual_keys = UEEAss().serialize().keys()

        for k in actual_keys:
            self.assertIn(k, self.ALL_KEYS)

if __name__ == '__main__':
    unittest.main()
