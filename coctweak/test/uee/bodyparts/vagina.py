import unittest
from coctweak.saves.uee.vagina import UEEVagina
class TestUEEVagina(unittest.TestCase):
    '''
    vaginas:
    - clitLength: 0.5
      clitPLong: ''
      clitPShort: ''
      clitPierced: 0
      fullness: 0
      labiaPLong: ''
      labiaPShort: ''
      labiaPierced: 0
      recoveryProgress: 0
      serializationVersionDictionary:
        cfe61f89-7ab6-4e4b-83aa-33f738dd2f05: 1
      type: 0
      vaginalLooseness: 0
      vaginalWetness: 1
      virgin: true
    '''
    ALL_KEYS = [
        'clitLength',
        'clitPierced',
        'clitPLong',
        'clitPShort',
        'fullness',
        'labiaPierced',
        'labiaPLong',
        'labiaPShort',
        'recoveryProgress',
        'serializationVersionDictionary',
        'type',
        'vaginalLooseness',
        'vaginalWetness',
        'virgin',
    ]
    def test_contains_expected_keys(self):
        actual_keys = UEEVagina().serialize().keys()
        for k in self.ALL_KEYS:
            self.assertIn(k, actual_keys)

    def test_doesnt_contain_anything_else(self):
        actual_keys = UEEVagina().serialize().keys()

        for k in actual_keys:
            self.assertIn(k, self.ALL_KEYS)

if __name__ == '__main__':
    unittest.main()
