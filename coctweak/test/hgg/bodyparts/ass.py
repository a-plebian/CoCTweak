import unittest
from coctweak.saves.hgg.ass import HGGAss
from coctweak.test._testSerializing import TestSerializationOf
class TestHGGAss(TestSerializationOf):
    '''
    ass:
      '0': []
      analLooseness: 0
      analWetness: 0
      fullness: 0
    '''
    EXPECTED_KEYS = [
        'analLooseness',
        'analWetness',
        'fullness',
    ]
    TEST_SUBJECT = HGGAss

if __name__ == '__main__':
    unittest.main()
