import unittest
from coctweak.saves.hgg.breastrow import HGGBreastRow
from coctweak.test._testSerializing import TestSerializationOf
class TestHGGBreastRow(TestSerializationOf):
    '''
    breastRows:
    - breastRating: 0
      breasts: 2
      fuckable: false
      fullness: 0
      lactationMultiplier: 0
      milkFullness: 0
      nipplesPerBreast: 1
    '''
    EXPECTED_KEYS = [
        'breastRating',
        'breasts',
        'fuckable',
        'fullness',
        'lactationMultiplier',
        'milkFullness',
        'nipplesPerBreast',
    ]
    TEST_SUBJECT = HGGBreastRow

if __name__ == '__main__':
    unittest.main()
