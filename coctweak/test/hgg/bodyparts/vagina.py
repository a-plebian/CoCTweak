import unittest
from coctweak.saves.hgg.vagina import HGGVagina
from coctweak.test._testSerializing import TestSerializationOf
class TestHGGVagina(TestSerializationOf):
    '''
    vaginas:
    - clitLength: 0.5
      clitPLong: ''
      clitPShort: ''
      clitPierced: 0
      fullness: 0
      labiaPLong: ''
      labiaPShort: ''
      labiaPierced: 0
      recoveryProgress: 0
      serializationVersion: 1
      type: 0
      vaginalLooseness: 0
      vaginalWetness: 1
      virgin: true
    '''
    EXPECTED_KEYS = [
        'clitLength',
        'clitPierced',
        'clitPLong',
        'clitPShort',
        'fullness',
        'labiaPierced',
        'labiaPLong',
        'labiaPShort',
        'recoveryProgress',
        'serializationVersion',
        'type',
        'vaginalLooseness',
        'vaginalWetness',
        'virgin'
    ]
    TEST_SUBJECT = HGGVagina

if __name__ == '__main__':
    unittest.main()
