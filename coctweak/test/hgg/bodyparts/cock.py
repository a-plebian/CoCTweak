import unittest
from coctweak.saves.hgg.cock import HGGCock
from coctweak.test._testSerializing import TestSerializationOf
class TestHGGCock(TestSerializationOf):
    '''
    cocks:
    - cockLength: 5.5
      cockThickness: 1
      cockType: 0
      knotMultiplier: 1
      pLongDesc: ''
      pShortDesc: ''
      pierced: 0
      serializationVersion: 1
      sock: ''
    '''
    EXPECTED_KEYS = [
        'cockLength',
        'cockThickness',
        'cockType',
        'knotMultiplier',
        'pierced',
        'pLongDesc',
        'pShortDesc',
        'serializationVersion',
        'sock',
    ]
    TEST_SUBJECT = HGGCock

if __name__ == '__main__':
    unittest.main()
