from typing import List, Type
import unittest
class TestSerializationOf(unittest.TestCase):
    EXPECTED_KEYS: List[str] = []
    TEST_SUBJECT:Type = None

    def test_contains_expected_keys(self):
        actual_keys = self.TEST_SUBJECT().serialize().keys()
        for k in self.EXPECTED_KEYS:
            self.assertIn(k, actual_keys)

    def test_doesnt_contain_anything_else(self):
        actual_keys = self.TEST_SUBJECT().serialize().keys()

        for k in actual_keys:
            self.assertIn(k, self.EXPECTED_KEYS)
