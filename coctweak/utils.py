import yaml, os, miniamf, collections, binascii, subprocess, sys, platform, json, shutil, argparse

from miniamf.amf3 import ByteArray

_mapping_tag = yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG

HOST_TRANSLATIONS = {
    'localwithnet':  '#localWithNet',
    '@localwithnet': '#localWithNet',
    '@lwn':          '#localWithNet',
}
def flash_host(hostname):
    if hostname is None:
        return None
    lhn = hostname.lower()
    if lhn in HOST_TRANSLATIONS:
        newhostname = HOST_TRANSLATIONS[lhn]
        print(f"NOTE: Hostname {hostname} has been interpreted as {newhostname}.")
        return newhostname
    return hostname

def add_flash_host_arg(subp, varname='host', **kwargs):
    kwargs.setdefault('help', "Flash hostname the save will be stored in. Use @localWithNet instead of #localWithNet, if you're on Linux.")
    kwargs['type'] = flash_host
    subp.add_argument(varname, **kwargs)

def dict_representer(dumper, data):
    return dumper.represent_dict(data.items())

def dict_constructor(loader, node):
    return collections.OrderedDict(loader.construct_pairs(node))

yaml.add_representer(collections.OrderedDict, dict_representer)
yaml.add_constructor(_mapping_tag, dict_constructor)

def remove_if_exists(path):
    if os.path.isdir(path):
        shutil.rmtree(path)
    elif os.path.isfile(path):
        os.remove(path)

def check_dir(path):
    if not os.path.isdir(path):
        os.makedirs(path)

def bool_from_boolargs(args, oldval, arg_true: str, arg_false: str) -> bool:
    if getattr(args, arg_true):
        return True
    if getattr(args, arg_false):
        return False
    return oldval


def in2feetinches(inches, footsuffix='\'', inchsuffix='"', divider='', maxprec=2):
    feet = round(inches // 12)
    inches = inches - (12 * feet)
    o = []
    if feet > 0:
        o += [f'{round(feet,maxprec):g}{footsuffix}']
    if inches > 0:
        o += [f'{round(inches,maxprec):g}{inchsuffix}']
    return divider.join(o)

def in2cm(inches):
    return inches * 2.54

def cm2metercm(cm, metersuffix='m', cmsuffix='cm', divider=', ', maxprec=2):
    meters = round(cm // 100)
    cm = cm - (100 * meters)
    o = []
    if meters > 0:
        o += [f'{round(meters,maxprec):g}{metersuffix}']
    if cm > 0:
        o += [f'{round(cm, maxprec):g}{cmsuffix}']
    return divider.join(o)

def display_length(inches, footsuffix='\'', inchsuffix='"', impdivider='', metersuffix='m', cmsuffix='cm', sidivider=', ', maxprec=1, systemdivider=' ', siwrap=True):
    impunits = in2feetinches(inches, footsuffix, inchsuffix, impdivider, maxprec)
    siunits = cm2metercm(in2cm(inches), metersuffix, cmsuffix, sidivider, maxprec)
    if siwrap:
        siunits = f'({siunits})'
    return f'{impunits}{systemdivider}{siunits}'

def dump_to_yaml(input_filename, output_filename):
    if not os.path.isfile(input_filename):
        return
    sol = miniamf.sol.load(input_filename)
    data = _do_cleanup_object(sol)
    with open(output_filename, 'w') as f:
        yaml.dump(data, f, default_flow_style=False)
        #json.dump(data, f, indent=4)

def dump_to_json(input_filename, output_filename):
    if not os.path.isfile(input_filename):
        return
    sol = miniamf.sol.load(input_filename)
    data = _do_cleanup_object(sol)
    with open(output_filename, 'w') as f:
        json.dump(data, f, indent=2)

def is_int(inp:str) -> bool:
    try:
        return float(inp).is_integer()
    except:
        return False

def is_float(inp:str) -> bool:
    try:
        float(inp)
        return True
    except:
        return False

def _do_cleanup_object(obj):
    newobj = collections.OrderedDict()
    for k in sorted(obj.keys(), key=lambda x: str(x)):
        #for k in obj.keys():
        newobj[str(k)] = _do_cleanup(obj[k])
    return newobj


def _do_cleanup_list(lst):
    newlst = []
    for v in lst:
        newlst.append(_do_cleanup(v))
    return newlst


def _do_cleanup(thing):
    if isinstance(thing, dict):
        return _do_cleanup_object(thing)
    elif isinstance(thing, list):
        return _do_cleanup_list(thing)
    elif isinstance(thing, ByteArray):
        return _do_cleanup_bytearray(thing)
    else:
        return thing

def _do_cleanup_bytearray(ba):
    data = collections.OrderedDict()
    data['_type'] = 'amf3.ByteArray'
    data['compressed'] = ba.compressed
    data['data'] = binascii.hexlify(ba.getvalue())
    return data

if platform.system() == 'Windows':
    def sanitizeFilename(filename):
        return os.path.join('%APPDATA%', os.path.relpath(filename, os.environ['APPDATA']))
else:
    def sanitizeFilename(filename):
        home = os.path.expanduser('~')
        return os.path.join('~', os.path.relpath(filename, start=home))

def execute(cmd:list, display:bool=True, capture:bool=False) -> str:
    print('$ '+' '.join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False)
    o = b''
    while True:
        out = p.stdout.read(1)
        if out == b'' and p.poll() != None:
            break
        if out != b'':
            if capture:
                o += out
            if display:
                sys.stdout.write(out.decode('utf-8'))
                sys.stdout.flush()
    return o.decode('utf-8')

def add_hostid_args(subp: argparse.ArgumentParser, batch: bool) -> None:
    if not batch:
        #subp.add_argument('host', type=str, help="Hostname the save will be stored in")
        add_flash_host_arg(subp)
        subp.add_argument('id', type=str, help="ID of the save.")
