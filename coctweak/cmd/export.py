import tempfile, os, yaml, json
from ._header import header
from coctweak.saves import getSaveFilename, loadFromSlot
from coctweak.utils import dump_to_yaml, dump_to_json, is_int, _do_cleanup_object, sanitizeFilename, add_flash_host_arg
from coctweak.logging import LogWrapper
log = LogWrapper()
def register_parsers__export(subp):
    p_export = subp.add_parser('export', help='Export save to YAML or JSON.')
    add_flash_host_arg(p_export)
    p_export.add_argument('id', type=str, help="ID of the save, or Main for the Permanent Object file.")
    p_export.add_argument('filename', type=str, help="File to export to.")
    p_export.add_argument('--format', choices=['yaml', 'json'], default=None, help="File to export to.")
    p_export.add_argument('--raw', action='store_true', default=False, help="Process the file as a raw SOL, with no COCTweak processing or decoding.")
    p_export.set_defaults(cmd=cmd_export)

def cmd_export(args):
    '''
    export localhost 1 backups/localhost_1.yml --format=yaml
    '''
    header(args)
    if args.format is None:
        _, ext = os.path.splitext(args.filename)
        if ext in ['.json']:
            args.format = 'json'
        elif ext in ['.yaml', '.yml']:
            args.format = 'yaml'
        else:
            log.critical('Unable to figure out what kind of format you want to output in.  Please end your filename with .(json|yml|yaml) or specify --format=(json|yml|yaml)')
            return False
    if is_int(args.id) and not args.raw:
        savefile = getSaveFilename(args.host, args.id)
        with log.info('Exporting save from %s...', sanitizeFilename(savefile)):
            save = loadFromSlot(args.host, args.id, args.quiet)
            log.info('Loaded as a %s save.', save.NAME)
            with open(args.filename, 'w') as f:
                data = _do_cleanup_object(save.serialize())
                if args.format == 'yaml':
                    yaml.dump(data, f, default_flow_style=False)
                else:
                    json.dump(data, f, indent=2)
    else:
        src = getSaveFilename(args.host, args.id)
        with log.info('Exporting raw SOL data from %s to %s...', sanitizeFilename(src), sanitizeFilename(args.filename)):
            if args.format == 'yaml':
                dump_to_yaml(src, args.filename)
            else:
                dump_to_json(src, args.filename)
