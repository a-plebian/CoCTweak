import argparse
from coctweak.cmd._header import header
from coctweak.saves import saveToSlot, loadFromSlot
from coctweak.saves._save import SaveCaps
from coctweak.utils import add_hostid_args
from coctweak.logging import getLogger
log = getLogger(__name__)
def register_parsers__heal(subp: argparse.ArgumentParser, batch: bool=False):
    p_heal = subp.add_parser('heal', help='Instantly heal the character to our best calculations of maxima/minima of combat stats.')
    add_hostid_args(p_heal, batch)
    p_heal.add_argument('--no-hp', dest='hp', action='store_false', default=True, help="Do NOT modify HP.")
    p_heal.add_argument('--lust', dest='lust', action='store_true', default=False, help="Set lust to 0.")
    p_heal.add_argument('--fatigue', dest='fatigue', action='store_true', default=False, help="Set fatigue to 0.")
    p_heal.add_argument('--hunger', dest='hunger', action='store_true', default=False, help="Set hunger to 0.")
    p_heal.add_argument('--parasites', dest='parasites', action='store_true', default=False, help="Clear parasites (in mods that support parasites).")
    p_heal.add_argument('--status-effects', dest='status_effects', action='store_true', default=False, help="Clear bad status effects (may be outdated).")
    p_heal.add_argument('--all', dest='all', action='store_true', default=False, help="FIX EVERYTHING (except --parasites and --status-effects).")
    p_heal.set_defaults(cmd=cmd_heal)
def cmd_heal(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    hp = args.hp or args.all
    lust = args.lust or args.all
    fatigue = args.fatigue or args.all
    hunger = args.hunger or args.all
    with log.info('Stats:'):
        for stat, change in save.heal(hp=hp, lust=lust, hunger=hunger, fatigue=fatigue).items():
            before, after = change
            log.info(f'{stat}: {before} -> {after}')

    if (save.CAPABILITIES & SaveCaps.PARASITES) == SaveCaps.PARASITES:
        with log.info('Parasites%s:', ' (Diagnosis only; fix with --parasites)' if not args.parasites else ''):
            if not args.parasites:
                results = save.get_parasites()
                if len(results) == 0:
                    log.info('Not infected!')
                else:
                    for entry in results:
                        log.info(f'* {entry}')
            else:
                save.heal_parasites()
    else:
        log.info('Parasites: (Not supported in this mod)')
    with log.info('Status Effects%s:', ' (Diagnosis only; fix with --status-effects)' if not args.status_effects else ''):
        if args.status_effects:
            save.heal_statuses()
        else:
            results = save.get_bad_statuses()
            if len(results) == 0:
                log.info('Not infected!')
            else:
                for entry in results:
                    log.info(f'* {entry.id}')
    if batch_save is None:
        saveToSlot(args.host, args.id, save)
