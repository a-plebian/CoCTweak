import os, argparse
from coctweak.cmd._header import header
from coctweak.saves import loadFromSlot, getSaveFilename, FLASH_BASE_DIR, saveToSOL, BaseSave
from coctweak.utils import add_hostid_args

def register_parsers__show(subp: argparse.ArgumentParser, batch: bool=False):
    p_show = subp.add_parser('show', aliases=['info'], help='Show save info.')
    add_hostid_args(p_show, batch)
    p_show.set_defaults(cmd=cmd_show)

def cmd_show(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    save.display()
