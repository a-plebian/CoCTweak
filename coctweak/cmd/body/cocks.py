import sys
from coctweak.cmd._header import header
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.logging import LogWrapper
log = LogWrapper()
def register_parsers__body_cocks(p_body_subp):
    p_body_cocks = p_body_subp.add_parser('cocks', aliases=['cock', 'penis', 'penises'], help='Operations to perform on your cocks.')
    p_body_cocks_subp = p_body_cocks.add_subparsers()

    p_body_cocks_add = p_body_cocks_subp.add_parser('add', help='Add a cock')
    p_body_cocks_add.add_argument('count', type=int, default=1, help="How many to add.")
    p_body_cocks_add.add_argument('--type', '-t', type=str, default='', help="Cock type to use. See your mod's CockType enumeration for possible values. Will otherwise use the last cock in the collection as a template.")
    p_body_cocks_add.add_argument('--length', '-l', type=float, default=0, help="The length of the cock. Will otherwise use the last cock in the collection as a template.")
    p_body_cocks_add.add_argument('--width', '-w', type=float, default=0, help="The width of the cock. Will otherwise use the last cock in the collection as a template.")
    p_body_cocks_add.add_argument('--knot-multiplier', '-k', type=float, default=-1, help="The knot multiplier (W*Km in most mods).")
    p_body_cocks_add.add_argument('--sock', '-s', type=str, default='null', help="Sock ID (none or null for no sock)")
    p_body_cocks_add.add_argument('--pierced', action='store_true', default=False, help="Sets pierced to TRUE")
    p_body_cocks_add.add_argument('--not-pierced', action='store_true', default=False, help="Sets pierced to FALSE")
    p_body_cocks_add.set_defaults(cmd=cmd_body_cocks_add)

    p_body_cocks_remove = p_body_cocks_subp.add_parser('remove', aliases=['rm'], help='Remove a cock')
    p_body_cocks_remove.add_argument('cock_id', type=int, help="Which cock to remove (0-n).")
    p_body_cocks_remove.set_defaults(cmd=cmd_body_cocks_remove)

    p_body_cocks_set = p_body_cocks_subp.add_parser('set', help='Change a cock')
    p_body_cocks_set.add_argument('cock_id', type=int, help="Which cock to modify.")
    p_body_cocks_set.add_argument('--type', '-t', type=str, default='', help="Cock type to use. See your mod's CockType enumeration for possible values")
    p_body_cocks_set.add_argument('--length', '-l', type=float, default=0, help="The length of the cock in inches.")
    p_body_cocks_set.add_argument('--width', '-w', type=float, default=0, help="The width of the cock.")
    p_body_cocks_set.add_argument('--knot-multiplier', '-k', type=float, default=-1, help="The knot multiplier (W*Km in most mods).")
    p_body_cocks_set.add_argument('--sock', '-s', type=str, default='null', help="Sock ID (none or null for no sock)")
    p_body_cocks_set.add_argument('--pierced', action='store_true', default=False, help="Sets pierced to TRUE")
    p_body_cocks_set.add_argument('--not-pierced', action='store_true', default=False, help="Sets pierced to FALSE")
    p_body_cocks_set.set_defaults(cmd=cmd_body_cocks_set)

    p_body_cocks_average = p_body_cocks_subp.add_parser('average', aliases=['avg'], help='Average out the size of all your cocks.')
    p_body_cocks_average.add_argument('--not-length', action='store_true', default=False, help='Do not average length.')
    p_body_cocks_average.add_argument('--not-width', action='store_true', default=False, help='Do not average width.')
    p_body_cocks_average.set_defaults(cmd=cmd_body_cocks_average)

def cmd_body_cocks_add(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    lastcock = save.COCK_TYPE()
    if len(save.cocks) > 0:
        lastcock.deserialize(save.cocks[-1].serialize())
    else:
        lastcock.type = lastcock.COCK_TYPES.HUMAN
        lastcock.length=5.5
        lastcock.thickness=1

    newcock = save.COCK_TYPE()
    newcock.type = save.COCK_TYPE.COCK_TYPES[args.type] if args.type != '' else lastcock.type
    newcock.length = args.length if args.length > 0 else lastcock.length
    newcock.thickness = args.width if args.width > 0 else lastcock.thickness
    newcock.knotMultiplier = args.knot_multiplier if args.knot_multiplier > -1 else lastcock.knotMultiplier
    newcock.sock = args.sock if args.sock not in ('', 'none', 'null') else lastcock.sock
    newcock.pierced = lastcock.pierced
    if args.pierced:
        newcock.pierced = True
    if args.not_pierced:
        newcock.pierced = False
    with log.info('Added:'):
        for _ in range(args.count):
            save.cocks.append(newcock)
            newcock.display()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_cocks_remove(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    cock = save.cocks[args.cock_id]
    with log.info('Removing:'):
        save.cocks.remove(cock)
        cock.display()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_cocks_set(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    cock = save.cocks[args.cock_id]
    with log.info('Before:'):
        cock.display()

    if args.type != '':
        cock.type = save.COCK_TYPE.COCK_TYPES[args.type]
    if args.length > 0:
        cock.length = args.length
    if args.width > 0:
        cock.thickness = args.width
    if args.knot_multiplier > -1:
        cock.knotMultiplier = args.knot_multiplier
    if args.sock not in ('', 'none', 'null'):
        cock.sock = args.sock
    if args.pierced:
        cock.piercing = True
    if args.not_pierced:
        cock.piercing = False

    with log.info('After:'):
        cock.display()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_cocks_average(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    lt=0
    tt=0

    with log.info('Before:'):
        save.displayCocks()

    for cock in save.cocks:
        lt += cock.length
        tt += cock.thickness

    avgl = lt/len(save.cocks)
    avgt = tt/len(save.cocks)
    for cock in save.cocks:
        if not args.not_length:
            cock.length = avgl
        if not args.not_width:
            cock.thickness = avgt

    with log.info('After:'):
        save.displayCocks()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)
