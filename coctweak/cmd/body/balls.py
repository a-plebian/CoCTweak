import sys
from coctweak.cmd._header import header
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.utils import display_length
from coctweak.logging import LogWrapper
log = LogWrapper()
def register_parsers__body_balls(p_body_subp):
    p_body_balls = p_body_subp.add_parser('balls', help='Operations to perform on your balls.')
    p_body_balls_subp = p_body_balls.add_subparsers()

    p_body_balls_add = p_body_balls_subp.add_parser('add', help='Add a testicle or two')
    p_body_balls_add.add_argument('count', type=int, default=1, help="How many to add.")
    p_body_balls_add.set_defaults(cmd=cmd_body_balls_add)

    p_body_balls_remove = p_body_balls_subp.add_parser('remove', aliases=['rm'], help='Remove testicles')
    p_body_balls_remove.add_argument('count', type=int, default=1, help="How many balls to remove (1-n).")
    p_body_balls_remove.set_defaults(cmd=cmd_body_balls_remove)

    p_body_balls_set = p_body_balls_subp.add_parser('set-count', help='Set how many balls you have.')
    p_body_balls_set.add_argument('count', type=int, help="How many testicles you want")
    p_body_balls_set.set_defaults(cmd=cmd_body_balls_setcount)

    p_body_balls_setsz = p_body_balls_subp.add_parser('set-size', help='Set how big your balls are.')
    p_body_balls_setsz.add_argument('inches', type=int, help="How many inches in diameter")
    p_body_balls_setsz.set_defaults(cmd=cmd_body_balls_setsize)

    p_body_balls_setm = p_body_balls_subp.add_parser('set-mult', help='Set how virile you are.')
    p_body_balls_setm.add_argument('multiplier', type=float, help="Cum multiplier")
    p_body_balls_setm.set_defaults(cmd=cmd_body_balls_setmult)

    p_body_balls_inflate = p_body_balls_subp.add_parser('inflate', aliases=['grow'], help='Increase the size of your balls')
    p_body_balls_inflate.add_argument('inches', type=int, help="How many inches in diameter")
    p_body_balls_inflate.set_defaults(cmd=cmd_body_balls_inflate)

    p_body_balls_deflate = p_body_balls_subp.add_parser('deflate', aliases=['shrink'], help='Decrease the size of your balls')
    p_body_balls_deflate.add_argument('inches', type=int, help="How many inches in diameter")
    p_body_balls_deflate.set_defaults(cmd=cmd_body_balls_deflate)

def _displayNutState(save):
    if save.balls == 0:
        log.info('You now have no balls.')
    else:
        log.info(f'You now have {save.balls}, each {display_length(save.ballSize)} in diameter, with a multiplier of {save.cumMultiplier}.')
    if save.balls > 4:
        log.warning('Having more than 4 balls in this game may cause bugs, since it\'s impossible to get this many "naturally".')

def cmd_body_balls_add(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    save.balls += max(args.count, 1)
    _displayNutState(save)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_balls_remove(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    save.balls = max(save.balls-args.count, 0)
    _displayNutState(save)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)


def cmd_body_balls_setcount(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    save.balls = max(args.count, 0)
    _displayNutState(save)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_balls_setmult(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    save.cumMultiplier = max(args.multiplier, 1)
    _displayNutState(save)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_balls_setsize(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    save.ballSize = max(0, args.inches)
    _displayNutState(save)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_balls_inflate(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    save.ballSize += max(0, args.inches)
    _displayNutState(save)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_balls_deflate(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    save.ballSize = max(1, save.ballSize - max(0, args.inches))
    _displayNutState(save)

    if batch_save is None:
        saveToSlot(args.host, args.id, save)
