import sys, argparse
from coctweak.cmd._header import header
from coctweak.saves import loadFromSlot, saveToSlot

from .cocks import register_parsers__body_cocks
from .balls import register_parsers__body_balls
from .vaginas import register_parsers__body_vaginas
from coctweak.utils import add_hostid_args

def register_parsers__body(subp: argparse.ArgumentParser, batch: bool=False):
    p_body = subp.add_parser('body', help='Change the PC\'s body')
    if not batch:
        add_hostid_args(p_body, batch)

    p_body_subp = p_body.add_subparsers()

    register_parsers__body_cocks(p_body_subp)
    register_parsers__body_balls(p_body_subp)
    register_parsers__body_vaginas(p_body_subp)
