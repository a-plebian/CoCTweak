import sys
from coctweak.utils import bool_from_boolargs
from coctweak.cmd._header import header
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.logging import LogWrapper
log = LogWrapper()

def register_parsers__body_vaginas(p_body_subp):
    p_body_vaginas = p_body_subp.add_parser('vaginas', aliases=['vagina', 'vag', 'vags'], help='Operations to perform on your vaginas.')
    p_body_vaginas_subp = p_body_vaginas.add_subparsers()

    p_body_vaginas_add = p_body_vaginas_subp.add_parser('add', help='Add a vagina')
    p_body_vaginas_add.add_argument('count', type=int, default=1, help="How many to add.")
    p_body_vaginas_add.add_argument('--type', '-t', type=str, default='', help="vagina type to use. See your mod's vaginaType enumeration for possible values. Will otherwise use the last vagina in the collection as a template.")
    p_body_vaginas_add.add_argument('--clit-length', '-l', type=float, default=-1, help="The length of the clit. Will otherwise use the last vagina in the collection as a template.")
    p_body_vaginas_add.add_argument('--fullness', '-f', type=float, default=-1, help="The fullness of the vagina. Will otherwise use the last vagina in the collection as a template.")
    p_body_vaginas_add.add_argument('--looseness', '-L', type=str, default='', help="The looseness of the vagina. See the appropriate VagLooseness enum for your mod.")
    p_body_vaginas_add.add_argument('--wetness', '-W', type=str, default='', help="The wetness of the vagina. See the appropriate VagWetness enum for your mod.")
    p_body_vaginas_add.add_argument('--recovery-progress', '-r', type=int, default=-1, help="The wetness of the vagina. See the appropriate VagWetness enum for your mod.")
    p_body_vaginas_add.add_argument('--clit-pierced', action='store_true', default=False, help="Sets clit pierced to TRUE")
    p_body_vaginas_add.add_argument('--clit-not-pierced', action='store_true', default=False, help="Sets clit pierced to FALSE")
    p_body_vaginas_add.add_argument('--labia-pierced', action='store_true', default=False, help="Sets labia pierced to TRUE")
    p_body_vaginas_add.add_argument('--labia-not-pierced', action='store_true', default=False, help="Sets labia pierced to FALSE")
    p_body_vaginas_add.add_argument('--virgin', action='store_true', default=False, help="Sets virgin to TRUE")
    p_body_vaginas_add.add_argument('--not-virgin', action='store_true', default=False, help="Sets virgin to FALSE")
    p_body_vaginas_add.set_defaults(cmd=cmd_body_vaginas_add)

    p_body_vaginas_remove = p_body_vaginas_subp.add_parser('remove', aliases=['rm'], help='Remove a vagina')
    p_body_vaginas_remove.add_argument('vagina_id', type=int, help="Which vagina to remove (0-n).")
    p_body_vaginas_remove.set_defaults(cmd=cmd_body_vaginas_remove)

    p_body_vaginas_set = p_body_vaginas_subp.add_parser('set', help='Change a vagina')
    p_body_vaginas_set.add_argument('vagina_id', type=int, help="Which vagina to modify.")
    p_body_vaginas_set.add_argument('--type', '-t', type=str, default='', help="vagina type to use. See your mod's vaginaType enumeration for possible values")
    p_body_vaginas_set.add_argument('--clit-length', '-l', type=float, default=-1, help="The length of the clit. Will otherwise use the last vagina in the collection as a template.")
    p_body_vaginas_set.add_argument('--fullness', '-f', type=float, default=-1, help="The fullness of the vagina. Will otherwise use the last vagina in the collection as a template.")
    p_body_vaginas_set.add_argument('--looseness', '-L', type=str, default='', help="The looseness of the vagina. See the appropriate VagLooseness enum for your mod.")
    p_body_vaginas_set.add_argument('--wetness', '-W', type=str, default='', help="The wetness of the vagina. See the appropriate VagWetness enum for your mod.")
    p_body_vaginas_set.add_argument('--recovery-progress', '-r', type=int, default=-1, help="The wetness of the vagina. See the appropriate VagWetness enum for your mod.")
    p_body_vaginas_set.add_argument('--clit-pierced', action='store_true', default=False, help="Sets clit pierced to TRUE")
    p_body_vaginas_set.add_argument('--clit-not-pierced', action='store_true', default=False, help="Sets clit pierced to FALSE")
    p_body_vaginas_set.add_argument('--labia-pierced', action='store_true', default=False, help="Sets labia pierced to TRUE")
    p_body_vaginas_set.add_argument('--labia-not-pierced', action='store_true', default=False, help="Sets labia pierced to FALSE")
    p_body_vaginas_set.add_argument('--virgin', action='store_true', default=False, help="Sets virgin to TRUE")
    p_body_vaginas_set.add_argument('--not-virgin', action='store_true', default=False, help="Sets virgin to FALSE")
    p_body_vaginas_set.set_defaults(cmd=cmd_body_vaginas_set)

    p_body_vaginas_average = p_body_vaginas_subp.add_parser('average', aliases=['avg'], help='Average out the size of all your clits.')
    p_body_vaginas_average.set_defaults(cmd=cmd_body_vaginas_average)

def cmd_body_vaginas_add(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    lastvagina = save.VAGINA_TYPE()
    if len(save.vaginas) > 0:
        lastvagina.deserialize(save.vaginas[-1].serialize())
    else:
        lastvagina.type = lastvagina.ENUM_TYPES.HUMAN
        lastvagina.length=5.5
        lastvagina.thickness=1

    newvagina = save.VAGINA_TYPE()
    newvagina.type = save.VAGINA_TYPE.ENUM_TYPES[args.type] if args.type != '' else lastvagina.type
    newvagina.clitLength = args.clit_length if args.clit_length > -1 else lastvagina.clitLength
    newvagina.clitPierced = bool_from_boolargs(args, lastvagina.clitPierced, 'clit_pierced', 'clit_not_pierced')
    #newvagina.clitPLong = args.clitPLong if args.clitPLong > 0 else lastvagina.clitPLong
    #newvagina.clitPShort = args.clitPShort if args.clitPShort > 0 else lastvagina.clitPShort
    newvagina.fullness = args.fullness if args.fullness > -1 else lastvagina.fullness
    newvagina.labiaPierced = bool_from_boolargs(args, lastvagina.labiaPierced, 'labia_pierced', 'labia_not_pierced')
    #newvagina.labiaPLong = args.labiaPLong if args.labiaPLong > 0 else lastvagina.labiaPLong
    #newvagina.labiaPShort = args.labiaPShort if args.labiaPShort > 0 else lastvagina.labiaPShort
    newvagina.looseness = save.VAGINA_TYPE.ENUM_LOOSENESS[args.looseness] if args.looseness != '' else lastvagina.looseness
    newvagina.recoveryProgress = args.recovery_progress if args.recovery_progress > -1 else lastvagina.recoveryProgress
    newvagina.virgin = bool_from_boolargs(args, lastvagina.virgin, 'virgin', 'not_virgin')
    newvagina.wetness = save.VAGINA_TYPE.ENUM_WETNESS[args.wetness] if args.wetness != '' else lastvagina.wetness
    with log.info('Added:'):
        for _ in range(args.count):
            save.vaginas.append(newvagina)
            newvagina.display()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_vaginas_remove(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    vagina = save.vaginas[args.vagina_id]
    with log.info('Removing:'):
        save.vaginas.remove(vagina)
        vagina.display()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_vaginas_set(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    vagina = save.vaginas[args.vagina_id]
    with log.info('Before:'):
        vagina.display()

    if args.type != '':
        vagina.type = save.VAGINA_TYPE.ENUM_TYPES[args.type]
    if args.clit_length > -1:
        vagina.clitLength = args.clit_length
    if args.fullness > -1:
        vagina.fullness = args.fullness
    if args.recovery_progress > -1:
        vagina.recoveryProgress = args.recovery_progress
    vagina.clitPierced = bool_from_boolargs(args, vagina.clitPierced, 'clit_pierced', 'clit_not_pierced')
    vagina.labiaPierced = bool_from_boolargs(args, vagina.labiaPierced, 'labia_pierced', 'labia_not_pierced')
    vagina.virgin = bool_from_boolargs(args, vagina.virgin, 'virgin', 'virgin')
    vagina.looseness = save.VAGINA_TYPE.ENUM_LOOSENESS[args.looseness] if args.looseness != '' else vagina.looseness
    vagina.wetness = save.VAGINA_TYPE.ENUM_WETNESS[args.wetness] if args.wetness != '' else vagina.wetness

    with log.info('After:'):
        vagina.display()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_body_vaginas_average(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    lt=0

    with log.info('Before:'):
        save.displayVaginas()

    for vagina in save.vaginas:
        lt += vagina.clitLength

    avgl = lt/len(save.vaginas)
    for vagina in save.vaginas:
        vagina.clitLength = avgl

    with log.info('After:'):
        save.displayVaginas()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)
