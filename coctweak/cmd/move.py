import os, sys, shutil
from coctweak.cmd._header import header
from coctweak.saves import getSaveFilename, loadFromSlot, saveToSlot
from coctweak.utils import sanitizeFilename, add_flash_host_arg
from coctweak.logging import LogWrapper
log = LogWrapper()
def register_parsers__move(subp):
    p_move = subp.add_parser('move', aliases=['mv'], help='Move a save slot around, including between hosts.')
    add_flash_host_arg(p_move, varname='src_host', help="Hostname the save is stored in")
    p_move.add_argument('src_id', type=int, help="ID of the original save.")
    add_flash_host_arg(p_move, varname='dest_host', help="Hostname the new save will be stored in")
    p_move.add_argument('dest_id', type=int, help="ID slot to rename it to. Must not exist.")
    p_move.add_argument('--force', action='store_true', default=False, help='If dest_id exists, overwrite it.')
    p_move.set_defaults(cmd=cmd_move)

def cmd_move(args):
    header(args)
    src_fn = getSaveFilename(args.src_host, args.src_id)
    if not os.path.isfile(src_fn):
        log.critical(f'{sanitizeFilename(src_fn)!r} does not exist.')
        sys.exit(1)
    dest_fn = getSaveFilename(args.dest_host, args.dest_id)
    if not os.path.isdir(os.path.dirname(dest_fn)):
        os.makedirs(os.path.dirname(dest_fn))
    if not args.force and os.path.isfile(dest_fn):
        log.critical(f'{sanitizeFilename(dest_fn)!r} exists. Please remove it, or use --force to overwrite it anyway.')
        sys.exit(1)
    with log.info(f'Moving {sanitizeFilename(src_fn)} to {sanitizeFilename(dest_fn)} at logical level...'):
        #shutil.move(src_fn, dest_fn) - Doesn't work.
        save = loadFromSlot(args.src_host, args.src_id, args.quiet)
        save.filename = dest_fn
        save.host = args.dest_host
        save.id = args.dest_id
        saveToSlot(args.dest_host, args.dest_id, save)
        os.remove(src_fn)
        log.info('  OK!')
