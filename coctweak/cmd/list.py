import os, yaml, json, sys
from coctweak.cmd._header import header
from coctweak.saves import loadSave, getSaveFilename, FLASH_BASE_DIR, saveToSOL, BaseSave
from coctweak.utils import _do_cleanup, add_flash_host_arg
from miniamf import sol
from coctweak.logging import LogWrapper
log = LogWrapper()

def register_parsers__list(subp):
    p_list = subp.add_parser('list', help='List saves', aliases=['ls'])
    add_flash_host_arg(p_list, nargs='?')
    p_list.add_argument('--format', choices=['coc', 'long', 'json', 'yaml'], default='long', help="Format of output.")
    p_list.set_defaults(cmd=cmd_list)

def cmd_list(args):
    if args.format in ('yaml', 'json'):
        args.quiet = True
    header(args)
    if args.host is None:
        found = False
        o=[]
        for hostname in os.listdir(FLASH_BASE_DIR):
            found = False
            for root, _, filenames in os.walk(os.path.join(FLASH_BASE_DIR, hostname)):
                for filename in filenames:
                    abspath = os.path.abspath(os.path.join(root, filename))
                    basefn = os.path.basename(abspath)
                    if basefn.startswith('CoC_') and basefn.endswith('.sol'):
                        found = True
                        break
                if found:
                    break
            if found:
                o += [hostname]
        if args.format == 'yaml':
            print(yaml.safe_dump(o, default_flow_style=True)) # Compress
        elif args.format == 'json':
            print(json.dumps(o, separators=(',',':'))) # Compress
        else:
            for hostname in o:
                log.info('* '+hostname)
    else:
        basedir = os.path.join(FLASH_BASE_DIR, args.host)
        if os.path.isdir(args.host):
            log.critical(f'Host {args.host!r} does not exist.')
            sys.exit(1)
        savesFound=[]
        for saveID in sorted(os.listdir(basedir)):
            if not saveID.startswith('CoC_'):
                continue
            savePath = os.path.join(basedir, saveID)
            saveID = saveID[4:-4]
            if saveID.startswith('Main'):
                continue
            if saveID.endswith('_backup'):
                continue
            savesFound += [('', int(saveID))]
        ejdir = os.path.join(basedir, '#CoC', 'EndlessJourney')
        if os.path.isdir(ejdir):
            for saveID in sorted(os.listdir(ejdir)):
                if not saveID.startswith('CoC_'):
                    continue
                savePath = os.path.join(ejdir, saveID)
                saveID = saveID[4:-4]
                if saveID == 'Main':
                    continue
                if saveID.endswith('_backup'):
                    continue
                savesFound += [('ej', int(saveID))]

        out = []
        maxpad = 0
        for orig_prefix, saveID in sorted(savesFound):
            maxpad = max(maxpad, len(args.host+'/'+orig_prefix + ' ' if orig_prefix != '' else args.host + ' '))
        for orig_prefix, saveID in sorted(savesFound):
            prefix = args.host+'/'+orig_prefix + ' ' if orig_prefix != '' else args.host + ' '
            prefix = prefix.ljust(maxpad+1)
            nssuffix = '/'+orig_prefix if orig_prefix != '' else ''
            slotID = prefix + str(saveID)
            savePath = getSaveFilename(args.host+nssuffix, saveID)
            save = loadSave(sol.load(savePath))
            out.append({
                'namespace': orig_prefix,
                'slot': saveID,
                'short_name': save.shortName,
                'notes': save.notes,
                'level': save.combat_stats.level.value,
                'gems': save.gems,
                'days': save.days,
                'difficulty': save.getDifficulty(),
                'mod': {
                    'name': save.NAME,
                    'version': save.version,
                }
            })
            if args.format == 'coc':
                '''
                1: Name - NOTES
                Days - 0 | Gender - MF | Difficulty - ???
                '''
                #if args.format == 'coc'
                with log.info(f'{slotID}: {save.shortName} - {save.notes}'):
                    log.info(f'Days - {save.days} | Gender - {save.getGender()} | Difficulty - {save.getDifficulty()} | Mod - {save.NAME} v{save.version}')
            elif args.format == 'long':
                '''
                1: Name (NOTES) - Lvl 0 Male, 1G, 0d on Normal (Vanilla v1.0.2)
                '''
                log.info(f'{slotID}: {save.shortName} ({save.notes}) - Lvl {save.combat_stats.level.value} {save.getLongGender()}, {save.gems}G, {save.days}d on {save.getDifficulty()} ({save.NAME} v{save.version})')
            elif args.format in ('yaml', 'json'):
                continue
        if args.format == 'yaml':
            print(yaml.safe_dump(out, default_flow_style=True)) # Compress
        elif args.format == 'json':
            print(json.dumps(out, separators=(',',':'))) # Compress
