import sys, argparse
from ._header import header
from coctweak.saves import loadFromSlot
from coctweak.utils import add_hostid_args

from coctweak.logging import getLogger
log = getLogger(__name__)
def register_parsers__get(subp: argparse.ArgumentParser, batch: bool=False):
    p_get = subp.add_parser('get', help='Get a particular element of a save.  Useful for scripting.')
    add_hostid_args(p_get, batch)

    p_get_subp = p_get.add_subparsers()

    p_get_notes = p_get_subp.add_parser('notes', help='Get the notes for a save.')
    p_get_notes.set_defaults(cmd=cmd_get_notes)

    p_get_stat = p_get_subp.add_parser('stat', help='Get the value for a core stat.')
    p_get_stat.add_argument('statID', type=str, help="Stat ID")
    p_get_stat.set_defaults(cmd=cmd_get_stat)

    p_get_gems = p_get_subp.add_parser('gems', help='Get the gems your character haves.')
    p_get_gems.set_defaults(cmd=cmd_get_gems)

def cmd_get_notes(args, batch_save=None):
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    print(save.notes)

def cmd_get_gems(args, batch_save=None):
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    print(save.gems)

def cmd_get_stat(args, batch_save=None):
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    if not save.stats.has(args.statID):
        header(args)
        log.critical('Stat %r does not exist in %s saves.', args.statID, save.NAME)
        sys.exit(1)

    print(save.stats.get(args.statID).value)
