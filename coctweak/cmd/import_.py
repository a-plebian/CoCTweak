import tempfile, os, json, yaml, miniamf.sol, miniamf
from coctweak.cmd._header import header
from coctweak.saves import getSaveFilename, loadSave, saveToSlot
from coctweak.utils import execute, _do_cleanup, is_int, sanitizeFilename, add_flash_host_arg
from coctweak.logging import LogWrapper
log = LogWrapper()
def register_parsers__import(subp):
    p_import = subp.add_parser('import', help='Import save from YAML or JSON.')
    p_import.add_argument('filename', type=str, help="File to import from.")
    add_flash_host_arg(p_import)
    p_import.add_argument('id', type=str, help="ID of the save, or Main for the Permanent Object file.")
    p_import.add_argument('--format', choices=['yaml', 'json'], default='yaml', help="Format to expect. YAML can usually parse JSON just fine.")
    p_import.set_defaults(cmd=cmd_import)

def cmd_import(args):
    '''
    import backups/localhost_1.yml localhost 1 --format=yaml
    '''
    header(args)
    src = args.filename

    data = miniamf.sol.SOL('CoC_'+str(args.id))
    with log.info('Loading %s...', sanitizeFilename(src)):
        with open(src, 'r') as f:
            if args.format == 'yaml':
                data.update(_do_cleanup(yaml.safe_load(f)))
            else:
                data.update(_do_cleanup(json.load(f)))
    if is_int(args.id):
        dest = getSaveFilename(args.host, args.id)
        save = loadSave(data)
        save.filename = dest
        save.host = args.host
        save.id = args.id
        saveToSlot(args.host, args.id, save, args.quiet)
    else:
        miniamf.sol.save(data, dest, encoding=miniamf.AMF3)
