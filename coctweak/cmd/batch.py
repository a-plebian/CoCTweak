import argparse, sys, shlex
from .backup import register_parsers__backup
#from .batch import register_parsers__batch
#from .changes import register_parsers__changes
#from .compare import register_parsers__compare
#from .copy import register_parsers__copy
#from .export import register_parsers__export
#from .import_ import register_parsers__import
#from .list import register_parsers__list
#from .move import register_parsers__move
#from .rename import register_parsers__rename
from .body import register_parsers__body
from .get import register_parsers__get
from .heal import register_parsers__heal
from .inventory import register_parsers__inv
from .keyitem import register_parsers__keyitem
from .perk import register_parsers__perk
from .restore import register_parsers__restore
from .set import register_parsers__set
from .show import register_parsers__show
from .statuseffect import register_parsers__statuseffect

from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.utils import add_flash_host_arg

def register_parsers(subp):
    register_parsers__backup(subp, True)
    #register_parsers__batch(subp)
    #register_parsers__changes(subp)
    #register_parsers__compare(subp)
    #register_parsers__copy(subp)
    #register_parsers__export(subp)
    #register_parsers__import(subp)
    #register_parsers__list(subp)
    #register_parsers__move(subp)
    #register_parsers__rename(subp)
    #register_parsers__restore(subp)
    register_parsers__body(subp, True)
    register_parsers__get(subp, True)
    register_parsers__heal(subp, True)
    register_parsers__inv(subp, True)
    register_parsers__keyitem(subp, True)
    register_parsers__perk(subp, True)
    register_parsers__set(subp, True)
    register_parsers__show(subp, True)
    register_parsers__statuseffect(subp, True)

def register_parsers__batch(subp):
    p_batch = subp.add_parser('batch', help='Run a batched command.')
    add_flash_host_arg(p_batch)
    p_batch.add_argument('id', type=int, help="ID of the save.")
    p_batch.add_argument('filename', type=str, help='File containing commands on each line.')
    p_batch.set_defaults(cmd=cmd_batch)

def cmd_batch(args):
    from ._header import header
    header(args)

    save = loadFromSlot(args.host, args.id, args.quiet)

    argp = argparse.ArgumentParser('coctweak', description='CoC Save Editor')

    argp.add_argument('--quiet', '-q', action='store_true', default=False, help='Silence information-level messages.')
    argp.add_argument('--verbose', '-v', action='store_true', default=False, help='More verbose logging.')
    argp.add_argument('--debug', action='store_true', default=False, help='Display debug-level messages.')

    subp = argp.add_subparsers()

    register_parsers(subp)

    with open(args.filename, 'r') as f:
        for line in f:
            line = line.strip()
            if line == '':
                continue
            if line.startswith('#'):
                continue

            ns = argparse.Namespace()
            argv = shlex.split(line)
            if line == 'backup':
                argv = 'backup'
            else:
                ns = argparse.Namespace(host=args.host, id=args.id)
            args = argp.parse_args(argv, ns)
            if getattr(args, 'cmd', None) is None:
                argp.print_help()
                sys.exit(1)

            #if not args.verbose:
            #    defaultSetup(logging.DEBUG if args.debug else logging.INFO)
            #else:
            #    verboseSetup(logging.DEBUG if args.debug else logging.INFO)
            args.cmd(args, save)

    saveToSlot(args.host, args.id, save)
