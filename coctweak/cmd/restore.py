import os,yaml,shutil
from miniamf import sol, AMF3
from coctweak.saves import saveToSOL, FLASH_BASE_DIR, loadSaveFromFile
from coctweak.utils import execute, _do_cleanup, check_dir, sanitizeFilename
from coctweak.logging import LogWrapper
log = LogWrapper()

def register_parsers__restore(subp):
    p_restore = subp.add_parser('restore', help='Restores all saves from backup/ directory.')
    p_restore.add_argument('-Y', '--yaml', action='store_true', default=False, help='Restore from YAML rather than the binary .sol files.')
    p_restore.add_argument('--clobber', action='store_true', default=False, help='Overwrite existing files.')
    p_restore.add_argument('--clean-slate', action='store_true', default=False, help='Destroy existing Macromedia Shared Objects Library, first. Irreversible.')
    p_restore.set_defaults(cmd=cmd_restore)

def remove_empty_dirs(dirname: str) -> int:
    if '.git' in dirname:
        return 0
    removed = 1
    dirs = 0
    files = 0
    while(removed > 0):
        dirs = 0
        files = 0
        removed = 0
        entries = os.listdir(dirname)
        for entryname in entries:
            entry = os.path.join(dirname, entryname)
            if os.path.isdir(entry):
                removed += remove_empty_dirs(entry)
                dirs += 1
            elif os.path.isfile(entry):
                files += 1
    if dirs + files == 0:
        print(f'rmdir {dirname} (empty)')
        os.rmdir(dirname)
        return 1
    return removed

def cmd_restore(args):
    if args.clean_slate:
        log.warning('--clean-slate specified!')
        log.warning(f'Calling shutil.rmtree({sanitizeFilename(FLASH_BASE_DIR)!r})')
        shutil.rmtree(FLASH_BASE_DIR)
    target_ext = '.yml' if args.yaml else '.sol'
    sources = []

    for root, _, files in os.walk('backup'):
        for filename in files:
            abspath = os.path.join(root, filename)
            relpath = os.path.relpath(abspath, 'backup')

            pathchunks = relpath.split(os.path.sep)
            if pathchunks[0].startswith('.'):
                #print(f'Skipping {relpath}')
                continue
            if not pathchunks[-1].endswith(target_ext):
                log.info(f'Skipping {relpath}')
                continue
            #print(repr(pathchunks))
            host = pathchunks[0]
            ns = [] if len(pathchunks) <= 3 else pathchunks[1:-2]
            objid, _ = os.path.splitext(pathchunks[-2])
            spec = (host, ns, objid)
            #print(repr(spec))
            sources += [spec]

    for host, ns, objid in sources:
        nspath = os.path.join(*ns) if len(ns) > 0 else ''
        srcfile = os.path.join('backup', host, nspath, objid+'.sol', objid+target_ext)
        destfile = os.path.join(FLASH_BASE_DIR, host, nspath, objid+'.sol')
        check_dir(os.path.dirname(destfile))

        if os.path.isfile(destfile):
            if not args.clobber:
                log.warning(f'File {sanitizeFilename(destfile)} exists already; Skipping. Use --clobber to overwrite anyway.')
                continue
            else:
                log.warning(f'Overwriting {sanitizeFilename(destfile)}.')

        with log.info(f'Saving {sanitizeFilename(destfile)}...'):
            if args.yaml:
                data = sol.SOL(objid)
                with open(srcfile, 'r') as f:
                    data.update(_do_cleanup(yaml.safe_load(f)))
                sol.save(data, destfile, AMF3)
            else:
                shutil.copy2(srcfile, destfile)
