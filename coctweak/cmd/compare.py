import tempfile, os, yaml
from coctweak.saves import getSaveFilename, loadSaveFromFile
from coctweak.utils import dump_to_yaml, execute, add_flash_host_arg

def register_parsers__compare(subp):
    p_compare = subp.add_parser('compare', help='Compares two saves,')
    add_flash_host_arg(p_compare, varname='left_hostname', help='Hostname of left save.')
    p_compare.add_argument('left_slotid', type=str, help='Slot ID of left save.')
    add_flash_host_arg(p_compare, varname='right_hostname', help='Hostname of right save.')
    p_compare.add_argument('right_slotid', type=str, help='Slot ID of right save.')
    p_compare.add_argument('--raw', action='store_true', default=False, help='Compare raw SOL files without trying to read them as saves. Useful for development.')
    p_compare.set_defaults(cmd=cmd_compare)

def cmd_compare(args):
    '''
    compare localhost 1 localhost 2
    '''
    with tempfile.TemporaryDirectory() as tmpdir:
        leftsrc = getSaveFilename(args.left_hostname, args.left_slotid)
        rightsrc = getSaveFilename(args.right_hostname, args.right_slotid)

        leftdest = os.path.join(tmpdir, os.path.basename(leftsrc[:-4]+'.yml'))
        rightdest = os.path.join(tmpdir, os.path.basename(rightsrc[:-4]+'.yml'))

        if args.raw:
            dump_to_yaml(leftsrc, leftdest)
            dump_to_yaml(rightsrc, rightdest)
        else:
            with open(leftdest, 'w') as f:
                yaml.dump(loadSaveFromFile(leftsrc, args.quiet).serialize(), f, default_flow_style=False)
            with open(rightdest, 'w') as f:
                yaml.dump(loadSaveFromFile(rightsrc, args.quiet).serialize(), f, default_flow_style=False)


        execute(['diff', '-u', '--color=always', leftdest, rightdest])
