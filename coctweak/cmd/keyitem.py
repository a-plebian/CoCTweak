import os, argparse, sys, yaml, json
from coctweak.cmd._header import header
from coctweak.saves import loadFromSlot, getSaveFilename, FLASH_BASE_DIR, saveToSlot
from coctweak.saves._keyitem import KeyItem
from coctweak.utils import add_hostid_args
from coctweak.logging import getLogger
log = getLogger(__name__)

def register_parsers__keyitem(subp: argparse.ArgumentParser, batch: bool=False):
    p_keyitem = subp.add_parser('keyitem', aliases=['keyitems'], help='View and modify the player\'s keyitems.')
    add_hostid_args(p_keyitem, batch)

    p_keyitem_subp = p_keyitem.add_subparsers()

    register_parsers___keyitem_add(p_keyitem_subp)
    register_parsers___keyitem_list(p_keyitem_subp)
    register_parsers___keyitem_remove(p_keyitem_subp)
    register_parsers___keyitem_show(p_keyitem_subp)

def register_parsers___keyitem_add(subp):
    p_add = subp.add_parser('add', help='Create a key item on the player.')
    p_add.add_argument('keyitem_id', type=str, help='Key Item ID.  See the listing of available Key Items for your mod.')
    p_add.add_argument('value1', type=float, default=0., help='Value 1, used as data storage in some key items.')
    p_add.add_argument('value2', type=float, default=0., help='Value 2, used as data storage in some key items.')
    p_add.add_argument('value3', type=float, default=0., help='Value 3, used as data storage in some key items.')
    p_add.add_argument('value4', type=float, default=0., help='Value 4, used as data storage in some key items.')
    p_add.set_defaults(cmd=cmd_keyitem_add)
def cmd_keyitem_add(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    keyitem = KeyItem()
    keyitem.id = args.keyitem_id
    keyitem.values = [args.value1, args.value2, args.value3, args.value4]
    save.keyItems[keyitem.id] = keyitem
    keyitem.displayValues()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def register_parsers___keyitem_remove(subp):
    p_remove = subp.add_parser('remove')
    p_remove.add_argument('keyitem_id', type=str, help='Key Item ID.  See the listing of available Key Items for your mod.')
    p_remove.set_defaults(cmd=cmd_keyitem_remove)
def cmd_keyitem_remove(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    if args.keyitem_id not in save.keyItems.keys():
        log.critical(f'Key Item ID {args.keyitem_id!r} is not set on the player. Aborting.')
        sys.exit(1)
    log.info('Removing %s...', save.keyItems[args.keyitem_id].id)
    del save.keyItems[args.keyitem_id]
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def register_parsers___keyitem_list(subp):
    p_ls = subp.add_parser('list', aliases=['ls'], help='List the IDs of all active Key Items.')
    p_ls.add_argument('--format', '-f', type=str, choices=['ascii', 'markdown', 'json', 'yaml'], default='ascii', help='Format of display.')
    p_ls.set_defaults(cmd=cmd_keyitem_list)
def cmd_keyitem_list(args, batch_save=None):
    # DELIBERATELY uses print().
    #header(args)
    save = batch_save or loadFromSlot(args.host, args.id, True)
    if args.format in ('ascii', 'markdown') and len(save.keyItems.keys()) == 0:
        print('<No key items set>')

    o={}
    for keyitemid in sorted(save.keyItems.keys()):
        keyitem = save.keyItems[keyitemid]
        if args.format == 'ascii':
            print(f'{keyitem.id}')
        elif args.format == 'markdown':
            print(f' * {keyitem.id}')
        else:
            o[keyitem.id] = {'values': keyitem.values}
    if args.format == 'json':
        print(json.dumps(o, indent=2))
    if args.format == 'yaml':
        print(yaml.dump(o, default_flow_style=False))

def register_parsers___keyitem_show(subp):
    p_ls = subp.add_parser('show', help='Display all values associated with the Key Item and any known interpretations of their meaning.')
    p_ls.add_argument('keyitem_id', type=str, help='ID of the key item.  Use quotes if it has a space!')
    p_ls.set_defaults(cmd=cmd_keyitem_show)
def cmd_keyitem_show(args, batch_save=None):
    #header(args)
    save = batch_save or loadFromSlot(args.host, args.id, True)
    if args.keyitem_id not in save.keyItems:
        log.critical(f'Key Item ID {args.keyitem_id!r} is not set on the player. Aborting.')
        sys.exit(1)
    keyitem = save.keyItems[args.keyitem_id]
    keyitem.longPrintValues()
