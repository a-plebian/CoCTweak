import miniamf.sol, miniamf
from typing import List
from ._header import header
from coctweak.saves import getSaveFilename
from coctweak.utils import add_flash_host_arg

from coctweak.logging import LogWrapper
log = LogWrapper()

def register_parsers__clean(subp):
    p_clean = subp.add_parser('clean', help='Clean cruft from the save, if applicable.')
    add_flash_host_arg(p_clean)
    p_clean.add_argument('id', type=str, help="ID of the save, or Main for the Permanent Object file.")
    p_clean.set_defaults(cmd=cmd_clean)

def delKeyIfPresent(save, key: List[str], reason: str):
    curbase = save
    for ckey in key[:-1]:
        if ckey not in curbase:
            return
        curbase = curbase[ckey]
    ckey = key[-1]
    if ckey in curbase:
        del curbase[ckey]
        log.info('Found and removed save entry %s (%s)', '/'.join(key), reason)

def cmd_clean(args):
    header(args)
    #save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    filename = getSaveFilename(args.host, args.id)
    save = miniamf.sol.load(filename)
    delKeyIfPresent(save, ['flags-decoded-NOT-LOADED'], 'Just used for comparing saves in CoCTweak.')
    miniamf.sol.save(save, filename, miniamf.AMF3)
