import os, argparse
from coctweak.cmd._header import header
from coctweak.saves import loadFromSlot, getSaveFilename, FLASH_BASE_DIR, saveToSlot, BaseSave
from coctweak.saves._item import Item
from coctweak.utils import add_hostid_args
from typing import List

from coctweak.logging import getLogger
log = getLogger(__name__)

# coctweak inv <inventory_key> add <item_id> [count]
# coctweak inv <inventory_key> rem <item_id> [count]
# coctweak inv <inventory_key> clear
# coctweak inv <inventory_key> set <slot> (<item_id>|Nothing|empty) [count]
# coctweak inv <inventory_key> unlock (n|auto|all)
# coctweak inv <inventory_key> restash
# coctweak inv list
def register_parsers__inv(subp: argparse.ArgumentParser, batch: bool=False):
    p_inv = subp.add_parser('inventory', help='Do stuff to the inventory.', aliases=['inv'])
    add_hostid_args(p_inv, batch)

    p_inv_subp = p_inv.add_subparsers()

    p_inv_list = p_inv_subp.add_parser('list', help='List all available inventory keys for this save.')
    p_inv_list.set_defaults(cmd=cmd_inventory_list)

    p_inv_add = p_inv_subp.add_parser('add', help='Add items to a given inventory.')
    p_inv_add.add_argument('inventory_key', type=str, help='Inventory key to modify.  See inv list.')
    p_inv_add.add_argument('item_id', type=str, help='Item ID.  Invalid IDs will break your save!')
    p_inv_add.add_argument('count', type=int, default=1, help='How many items to add.')
    p_inv_add.set_defaults(cmd=cmd_inventory_add)

    p_inv_rm = p_inv_subp.add_parser('remove', aliases=['rm'], help='Remove items from a given inventory.')
    p_inv_rm.add_argument('inventory_key', type=str, help='Inventory key to modify.  See inv list.')
    p_inv_rm.add_argument('item_id', type=str, help='Item ID.  Invalid IDs will break your save!')
    p_inv_rm.add_argument('count', type=int, default=-1, help='How many items to remove.')
    p_inv_rm.set_defaults(cmd=cmd_inventory_remove)

    p_inv_clear = p_inv_subp.add_parser('clear', aliases=['nuke'], help='Clear ALL items from a given inventory.')
    p_inv_clear.add_argument('inventory_key', type=str, help='Inventory key to modify.  See inv list.')
    p_inv_clear.set_defaults(cmd=cmd_inventory_clear)

    p_inv_set = p_inv_subp.add_parser('set', help='Set a slot in a given inventory.')
    p_inv_set.add_argument('inventory_key', type=str, help='Inventory key to modify.  See inv list.')
    p_inv_set.add_argument('slot', type=int, help='Slot ID, from 1 - n.')
    p_inv_set.add_argument('--item', '-i', type=str, default=None, help='Item ID.  Invalid IDs will break your save!')
    p_inv_set.add_argument('--count', '-c', type=int, default=None, help='How many items in that slot.')
    p_inv_set.add_argument('--damage', '-D', type=float, default=None, help='Damage/wear of the item. Not implemented in Vanilla and some mods.')
    #p_inv_set.add_argument('--bonus', '-', type=float, default=None, help='Damage/wear of the item. Not implemented in Vanilla and some mods.')
    p_inv_set.set_defaults(cmd=cmd_inventory_set)

    p_inv_sort = p_inv_subp.add_parser('sort', help='Sort a given inventory.')
    p_inv_sort.add_argument('inventory_key', type=str, help='Inventory key to modify.  See inv list.')
    p_inv_sort.add_argument('--by', choices=['id', 'value'], default='id', help='How to sort the list.')
    p_inv_sort.set_defaults(cmd=cmd_inventory_sort)

    p_inv_restash = p_inv_subp.add_parser('restash', help='Collects all items from your camp storage, and then re-stores them according to known rules. Helpful if you recently found a chest or rack.')
    p_inv_restash.set_defaults(cmd=cmd_inventory_restash)

    p_inv_stash = p_inv_subp.add_parser('stash', help='Collects all items from your player inventory, and then re-stores them according to known rules. Helpful if you recently found a chest or rack.')
    p_inv_stash.set_defaults(cmd=cmd_inventory_stash)

    def int_or_choice(choices: List[str], min_int=0, max_int=255):
        def check(x: str):
            if x.isnumeric():
                x = int(x)
                if x < min_int: raise argparse.ArgumentTypeError(f'Value less than {min_int}')
                if x > max_int: raise argparse.ArgumentTypeError(f'Value greater than {max_int}')
                return x
            else:
                if x not in choices:
                    raise argparse.ArgumentTypeError(f'value is not one of {choices!r}')
                return x
        return check

    p_inv_unlock = p_inv_subp.add_parser('unlock', help='Unlock X slots of the given inventory key.  Currently, only player is accepted. WILL SCREW WITH YOUR BACKPACK, MAKE BACKUPS.')
    p_inv_unlock.add_argument('inventory_key', type=str, help='Inventory key to modify.  See inv list.')
    p_inv_unlock.add_argument('n', type=int_or_choice(['auto', 'all'], 1, 10), default='auto', metavar='{#,auto,all}', help='How many slots to unlock. "all" will unlock all slots, "auto" will reset back to the game\'s default logic.')
    p_inv_unlock.set_defaults(cmd=cmd_inventory_unlock)

def cmd_inventory_add(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = loadFromSlot(args.host, args.id, args.quiet)
    save.getStorageByKey(args.inventory_key).add(args.item_id, args.count)
    if batch_save is None:
        saveToSlot(args.host, args.id, save)
def cmd_inventory_remove(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = loadFromSlot(args.host, args.id, args.quiet)
    save.getStorageByKey(args.inventory_key).remove(args.item_id, args.count)
    if batch_save is None:
        saveToSlot(args.host, args.id, save)
def cmd_inventory_clear(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = loadFromSlot(args.host, args.id, args.quiet)
    save.getStorageByKey(args.inventory_key).clear()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)
def cmd_inventory_set(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    slot = save.getStorageByKey(args.inventory_key).slots[args.slot-1]

    oldid = slot.id
    olddmg = slot.damage
    oldqty = slot.quantity

    if args.item is not None:
        if args.item.lower() in ('nothing!', 'empty', 'null', 'nil', 'none'):
            slot.set_empty()
        else:
            slot.id = args.item

    if args.count is not None:
        slot.quantity = args.count

    if args.damage is not None:
        slot.damage = args.damage
    with log.info(f'Inventory slot #{args.slot}:'):
        if oldid != slot.id:
            log.info(f'ID: {oldid} -> {slot.id}')
        if olddmg != slot.damage:
            log.info(f'Damage: {olddmg} -> {slot.damage}')
        if oldqty != slot.quantity:
            log.info(f'Count: {oldqty} -> {slot.quantity}')

    if batch_save is None:
        saveToSlot(args.host, args.id, save)
def cmd_inventory_list(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    with log.info('Available inventory keys: (use any listed key)'):
        for line in save.availableInventories():
            log.info(', '.join(line))
def cmd_inventory_sort(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    slots = []
    inv = save.getStorageByKey(args.inventory_key)
    with log.info('Loading inventory items from %s...', inv.name):
        for i in range(len(inv.slots)):
            oslot = inv.slots[i]
            if not oslot.can_sort():
                continue
            newslot = save.cloneSlot(oslot)
            slots += [newslot]
    log.info('Sorting...')
    sortby = lambda x: (x.is_empty(), x.id)
    if args.by == 'value':
        sortby = lambda x: (x.is_empty(), x.value)
    slots.sort(key=sortby)
    with log.info('Storing...'):
        for i in range(len(slots)):
            before = inv.slots[i].displayValues()
            if inv.slots[i].can_sort():
                inv.slots[i].cloneMutables(slots[i])
                after = inv.slots[i].displayValues(hide_locked=True)
                istr = f'[{i}]'.ljust(4)
                log.info(f'{istr} {before} -> {after}')
            else:
                log.info(f'{istr} {before} (unchanged)')

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_inventory_unlock(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)
    if not hasattr(save, 'unlockNumSlots'):
        log.critical('This save is for %r, which does not support this kind of hack.', save.NAME)
        return
    if args.inventory_key in save.chestStorage.keys:
        if args.n == 'auto':
            with log.info('Resetting slot locks...'):
                save.unlockNumSlots(save.playerStorage, save.calcFixedInventorySlots())

    if args.inventory_key in save.playerStorage.keys:
        cfg = save.getCoCTweakConfig()
        if args.n == 'auto':
            with log.info('Removing backpack hack and restoring normal slot operation.'):
                if cfg.backup_backpack_size is not None:
                    save.setBackpackSize(cfg.backup_backpack_size)
                    cfg.unlock_inventory_slots = None
                    cfg.backup_backpack_size = None
                else:
                    log.warning('Backup backpack size is None, so we may have already removed the hack. Or the save is fucked.')
                save.unlockNumSlots(save.playerStorage, save.calcSlotLocks(verbose=True))
        elif args.n == 'all':
            with log.info('Using backpack hack to unlock %d slots...', save.MAX_INVENTORY_SLOTS):
                if cfg.backup_backpack_size is None:
                    cfg.backup_backpack_size = save.getBackpackSize()
                log.info('Old backpack size: %d', cfg.backup_backpack_size)
                cfg.unlock_inventory_slots = save.MAX_INVENTORY_SLOTS
                #save.unlockNumSlots(save.inventory, save.MAX_INVENTORY_SLOTS)
                save.unlockNumSlots(save.playerStorage, save.calcSlotLocks(verbose=True))
        else:
            with log.info('Using backpack hack to unlock %d slots...', int(args.n)):
                if cfg.backup_backpack_size is None:
                    cfg.backup_backpack_size = save.getBackpackSize()
                log.info('Old backpack size: %d', cfg.backup_backpack_size)
                cfg.unlock_inventory_slots = int(args.n)
                #save.unlockNumSlots(save.inventory, cfg.unlock_inventory_slots)
                save.unlockNumSlots(save.playerStorage, save.calcSlotLocks(verbose=True))
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_inventory_restash(args, batch_save=None):
    if batch_save is None:
        header(args)
    save: BaseSave = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    stashed: List[Item]=[]
    with log.info('Gathering from camp storage...'):
        for storage in save.campStorage:
            stashed += [x.clone() for x in storage.slots if not x.is_empty()]
            with log.info('Clearing %s...', storage.name):
                storage.clear()
    with log.info('Stashing items...'):
        for item in stashed:
            storage = save.stash(item)
            if storage is None:
                log.critical('Could not stash %d x %s.  Aborting.', item.quantity, item.id)
                return
            log.info('Stored %d x %s in %s.', item.quantity, item.id, storage.name)
    for storage in save.campStorage:
        storage.display()
    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_inventory_stash(args, batch_save=None):
    if batch_save is None:
        header(args)
    save: BaseSave = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    stashed: List[Item]=[]
    leftovers: List[Item]=[]
    with log.info('Gathering from player...'):
        stashed += [x.clone() for x in save.playerStorage.slots if not x.is_empty()]
    with log.info('Clearing playerStorage...'):
        save.playerStorage.clear()
    with log.info('Stashing items...'):
        for item in stashed:
            storage = save.stash(item)
            if storage is None:
                log.warning('Could not stash %d x %s in camp inventory.  Will try to put back in player inventory.', item.quantity, item.id)
                leftovers += [item]
                continue
            log.info('Stored %d x %s in %s.', item.quantity, item.id, storage.name)
    for storage in save.campStorage:
        storage.display()
    for item in leftovers:
        stored = False
        for slot in save.playerStorage.slots:
            if slot.is_empty():
                slot.cloneMutables(item)
                stored=True
                break
        if not stored:
            log.critical('Could not stash %d x %s in player inventory.  Aborting.', item.quantity, item.id)
            return
    if batch_save is None:
        saveToSlot(args.host, args.id, save)
