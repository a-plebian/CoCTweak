import os, argparse, sys, yaml, json
from coctweak.cmd._header import header
from coctweak.saves import loadFromSlot, getSaveFilename, FLASH_BASE_DIR, saveToSlot
from coctweak.saves._statuseffect import StatusEffect
from coctweak.utils import add_hostid_args

from coctweak.logging import getLogger
log = getLogger(__name__)

def register_parsers__statuseffect(subp: argparse.ArgumentParser, batch: bool=False):
    p_sfx = subp.add_parser('statuseffect', aliases=['sfx'], help='View and modify the player\'s status effects.')
    add_hostid_args(p_sfx, batch)

    p_sfx_subp = p_sfx.add_subparsers()

    register_parsers___statuseffect_add(p_sfx_subp)
    register_parsers___statuseffect_list(p_sfx_subp)
    register_parsers___statuseffect_remove(p_sfx_subp)
    register_parsers___statuseffect_show(p_sfx_subp)

def register_parsers___statuseffect_add(subp):
    p_add = subp.add_parser('add', help='Create a status effect on the player.')
    p_add.add_argument('sfx_id', type=str, help='Status Effect ID.  See the listing of available status effects for your mod.')
    p_add.add_argument('value1', type=float, default=0., help='Value 1, used as data storage in some status effects.')
    p_add.add_argument('value2', type=float, default=0., help='Value 2, used as data storage in some status effects.')
    p_add.add_argument('value3', type=float, default=0., help='Value 3, used as data storage in some status effects.')
    p_add.add_argument('value4', type=float, default=0., help='Value 4, used as data storage in some status effects.')
    p_add.set_defaults(cmd=cmd_statuseffect_add)
def cmd_statuseffect_add(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    with log.info('Adding new Status Effect quartet...'):
        sfx = StatusEffect()
        sfx.id = args.sfx_id
        sfx.values = [args.value1, args.value2, args.value3, args.value4]
        save.statusEffects[sfx.id] = sfx
        sfx.displayValues()

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def register_parsers___statuseffect_remove(subp):
    p_remove = subp.add_parser('remove', aliases=['rm'], help='Remove a status effect')
    p_remove.add_argument('sfx_id', type=str, help='Status Effect ID.  See the listing of available status effects for your mod.')
    p_remove.set_defaults(cmd=cmd_statuseffect_remove)
def cmd_statuseffect_remove(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    if args.sfx_id not in save.statusEffects.keys():
        log.critical(f'Status Effect ID {args.sfx_id!r} is not set on the player. Aborting.')
        sys.exit(1)
    log.info('Removing %r...', save.statusEffects[args.sfx_id].id)
    del save.statusEffects[args.sfx_id]
    saveToSlot(args.host, args.id, save)

def register_parsers___statuseffect_list(subp):
    p_ls = subp.add_parser('list', aliases=['ls'], help='List the IDs of all active status effects.')
    p_ls.add_argument('--format', '-f', type=str, choices=['ascii', 'markdown', 'json', 'yaml'], default='ascii', help='Format of display.')
    p_ls.set_defaults(cmd=cmd_statuseffect_list)
def cmd_statuseffect_list(args, batch_save=None):
    # This DELIBERATELY uses print(), so we avoid breaking stuff trying to read stdout.
    #header(args)
    save = batch_save or loadFromSlot(args.host, args.id, True)
    if args.format in ('ascii', 'markdown') and len(save.statusEffects.keys()) == 0:
        print('<No status effects set>')

    o={}
    for sfxid in sorted(save.statusEffects.keys()):
        sfx = save.statusEffects[sfxid]
        if args.format == 'ascii':
            print(f'{sfx.id}')
        elif args.format == 'markdown':
            print(f' * {sfx.id}')
        else:
            o[sfx.id] = {'values': sfx.values}
            if sfx.dataStore is not None:
                o[sfx.id]['dataStore'] = sfx.dataStore
    if args.format == 'json':
        print(json.dumps(o, indent=2))
    if args.format == 'yaml':
        print(yaml.dump(o, default_flow_style=False))

def register_parsers___statuseffect_show(subp):
    p_ls = subp.add_parser('show', help='Display all values associated with the status effect and any known interpretations of their meaning.')
    p_ls.add_argument('sfx_id', type=str, help='ID of the status effect.  Use quotes if it has a space!')
    p_ls.set_defaults(cmd=cmd_statuseffect_show)
def cmd_statuseffect_show(args, batch_save=None):
    #if batch_save is None:
    #    header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    if args.sfx_id not in save.statusEffects:
        log.critical(f'Status Effect ID {args.sfx_id!r} is not set on the player. Aborting.')
        sys.exit(1)
    sfx = save.statusEffects[args.sfx_id]
    sfx.longPrintValues()
