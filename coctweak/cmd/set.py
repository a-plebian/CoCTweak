import sys, argparse
from ._header import header
from coctweak.saves import loadFromSlot, saveToSlot
from coctweak.utils import add_hostid_args

from coctweak.logging import getLogger
log = getLogger(__name__)

def register_parsers__set(subp: argparse.ArgumentParser, batch: bool=False):

    p_set = subp.add_parser('set', help='Set a particular element of a save')
    add_hostid_args(p_set, batch)
    p_set_subp = p_set.add_subparsers()

    p_set_notes = p_set_subp.add_parser('notes', help='Set the notes for a save.')
    p_set_notes.add_argument('newnotes', type=str, help="New notes for the save.")
    p_set_notes.set_defaults(cmd=cmd_set_notes)

    p_set_stat = p_set_subp.add_parser('stat', help='Set the value for a core stat.')
    p_set_stat.add_argument('statID', type=str, help="Stat ID")
    p_set_stat.add_argument('new_value', type=float, help="New value.")
    p_set_stat.set_defaults(cmd=cmd_set_stat)

    register_parsers__set_gems(p_set_subp)

def cmd_set_notes(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    if not args.quiet:
        log.info(f'Notes: {save.notes!r} -> {args.newnotes!r}')
    save.notes = args.newnotes

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def cmd_set_stat(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)

    if not save.stats.has(args.statID):
        log.critical('Stat %r does not exist in %s saves.', args.statID, save.NAME)
        sys.exit(1)

    if not args.quiet:
        stat = save.stats.get(args.statID)
        log.info(f'Stat {stat.name} ({stat.id}): {stat.value!r} -> {args.new_value!r}')

    save.stats.get(args.statID).value = args.new_value

    if batch_save is None:
        saveToSlot(args.host, args.id, save)

def register_parsers__set_gems(p_set_subp):
    p_set_gems = p_set_subp.add_parser('gems', help='Set how many gems (currency) you have in your bag.')
    p_set_gems.add_argument('new_value', type=int, help="New value.")
    p_set_gems.set_defaults(cmd=cmd_set_gems)
def cmd_set_gems(args, batch_save=None):
    if batch_save is None:
        header(args)
    save = batch_save or loadFromSlot(args.host, args.id, args.quiet)


    if not args.quiet:
        gems = save.gems
        log.info(f'Gems : {gems!r} -> {args.new_value!r}')

    save.gems = args.new_value

    if batch_save is None:
        saveToSlot(args.host, args.id, save)
