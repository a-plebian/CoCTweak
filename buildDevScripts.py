import markdown, os, sys, re

import jinja2

from buildtools.maestro import BuildMaestro
from buildtools.maestro.fileio import ReplaceTextTarget
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.coffeescript import CoffeeBuildTarget

jenv = jinja2.Environment(loader=jinja2.FileSystemLoader('docs-src'))

class BuildMarkdown(SingleBuildTarget):
    BT_TYPE = 'MARKDOWN'
    def __init__(self, target, filename, title='Untitled', dependencies=[]):
        self.infile=filename
        self.outfile=target
        self.title=title

        self.currentSection=0
        self.currentInstruction=0

        super().__init__(target=target, files=[filename], dependencies=dependencies)

    def get_config(self) -> dict:
        return {
            'title': self.title
        }

    def handleCheckbox(self, m) -> str:
        chkid=f'sec_{self.currentSection}_{self.currentInstruction}'
        self.currentInstruction += 1
        return f'<li><input type="checkbox" id="{chkid}"><label for="{chkid}">{m.group(1)}</label>'+m.group(2)

    def build(self) -> None:
        #pandoc -s -f commonmark -t html -o docs/development/samples.html docs/development/samples.md
        #sed -i "s@\[ \]@\<input type=\"checkbox\" \/\>@g" docs/development/samples.html

        #node_modules/.bin/marked --gfm -i docs/development/samples.md -o docs/development/samples.html
        mdcode = ''
        print(self.infile)
        with open(self.infile, 'r') as f:
            mdcode = f.read()

        mdcode = markdown.markdown(mdcode, extensions=[], output_format='html5', tab_length=2)

        newmdcode = ''
        self.currentSection = 0
        for line in mdcode.splitlines():
            m = re.match(r'^<h\d>.*', line)
            if m is not None:
                self.currentSection += 1
                self.currentInstruction=0
            line = re.sub(r'<li> *\[ \](.*)(<(/li|ul|ol)>)$', self.handleCheckbox, line)
            newmdcode += line+'\n'
        mdcode=newmdcode
        with open(self.outfile, 'w') as f:
            f.write(jenv.get_template('rendered.jinja2.html').render(title=self.title, body=mdcode))

def mkMarkdown(basename, title):
    global bm
    return bm.add(BuildMarkdown(os.path.join('docs', basename+'.html'), os.path.join('docs-src', basename+'.md'), title))

bm = BuildMaestro()
mkMarkdown(os.path.join('development', 'samples'), 'Sample Generation Scripts')
bm.as_app()
