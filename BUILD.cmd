@echo off
:: Windows has shitty naming schemes for Python, so you have to suffer.
SET PYTHON=%PYTHON%
if [%PYTHON%]==[] (
  for %%x in (python3.8, python38, python3.7, python37, python3.6, python36, python3) do (
    WHERE %%x
    if %ERRORLEVEL%==0 (
      set PYTHON=%%x
    )
  )
)

if [%PYTHON%]==[] (
  echo ERROR: The command %PYTHON% is missing from PATH. Please install Python from http://python.org and add it to PATH.
  exit 1
)
echo PYTHON=%PYTHON%

call %PYTHON% devtools/sortdata.py
call %PYTHON% devtools/buildData.py
call %PYTHON% devtools/importFlags.py
call %PYTHON% devtools/importClass.py
call %PYTHON% devtools/mkLibEnums.py
call %PYTHON% devtools/buildDocs.py
call %PYTHON% -m unittest coctweak.test

rm -rfv dist build
pyinstaller --noupx coctweak.spec

copy /y skeleton\README.md dist\README.md
copy /y skeleton\CHANGELOG.md dist\CHANGELOG.md
copy /y LICENSE dist\LICENSE
mkdir dist\batches
xcopy /y batches\* dist\batches\
mkdir dist\data\hgg
xcopy /y data\hgg\*.min.json dist\data\hgg\
mkdir dist\data\uee
xcopy /y data\uee\*.min.json dist\data\uee\
