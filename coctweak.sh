#!/bin/bash
PYTHON="$PYTHON"

[ -z "$PYTHON" ] && command -v python3.8 > /dev/null && { PYTHON=`command -v python3.8`; }
[ -z "$PYTHON" ] && command -v python3.7 > /dev/null && { PYTHON=`command -v python3.7`; }
[ -z "$PYTHON" ] && command -v python3.6 > /dev/null && { PYTHON=`command -v python3.6`; }
[ -z "$PYTHON" ] && command -v python3 > /dev/null && { PYTHON=`command -v python3`; echo "WARNING: Using fallback interpreter!"; }

if [ ! -z "$PYTHON" ]; then
  #echo "Selected $PYTHON as interpreter."
  $PYTHON -m coctweak "$@"
else
  echo "Unable to find an available python3 interpreter. Please install python >=3.6."
  exit 1
fi
