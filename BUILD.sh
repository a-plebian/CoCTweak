#!/bin/bash
set -ex
PYTHON="$PYTHON"
[ -z "$PYTHON" ] && command -v python3.8 && { PYTHON=`command -v python3.8`; }
[ -z "$PYTHON" ] && command -v python3.7 && { PYTHON=`command -v python3.7`; }
[ -z "$PYTHON" ] && command -v python3.6 && { PYTHON=`command -v python3.6`; }
[ -z "$PYTHON" ] && command -v python3 && { PYTHON=`command -v python3`; echo "WARNING: Using fallback interpreter!"; }
if [ -z "$PYTHON" ]; then
  echo "Unable to find an available python3 interpreter. Please install python >=3.6."
  exit 1
fi
echo \$PYTHON=$PYTHON

function execPy() {
  if [ -e $1 ]; then
    $PYTHON $1
  else
    echo Python devtool $1 is missing, skipped.
  fi
}

execPy devtools/sortdata.py
execPy devtools/buildData.py
execPy devtools/importFlags.py
execPy devtools/importClass.py
execPy devtools/mkLibEnums.py
execPy devtools/buildDocs.py
$PYTHON -m unittest coctweak.test

rm -rfv dist build
mkdir -pv archives
tar cjvf archives/coctweak-source-nightly.tar.gz coctweak/ data/ docs/ batches/ skeleton/ BUILD.sh class-config.yml coctweak.cmd coctweak.sh coctweak.spec doclist.yml flag-config.yml LICENSE README.md requirements.txt TEST.sh
pyinstaller coctweak.spec

cp -v skeleton/README.md dist/README.md
cp -v LICENSE dist/LICENSE
cp -v CHANGELOG.md dist/CHANGELOG.md
mkdir -pv dist/batches
cp -v batches/* dist/batches/
mkdir -pv dist/data/hgg
cp -v data/hgg/*.min.json dist/data/hgg/
mkdir -pv dist/data/uee
cp -v data/uee/*.min.json dist/data/uee/

cd dist
tar cjvf ../archives/coctweak-linux-amd64-nightly.tar.gz batches/ data/ coctweak LICENSE README.md CHANGELOG.md
cd ..
