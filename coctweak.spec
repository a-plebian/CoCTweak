# -*- mode: python -*-

block_cipher = None


a = Analysis(['coctweak/__main__.py'],
             pathex=['.'],
             binaries=[],
             datas=[],
             hiddenimports=[
               'miniamf.adapters._sets'
             ],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure,
          a.zipped_data,
          cipher=block_cipher,
          noupx=True)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='coctweak',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=False,
          runtime_tmpdir=None,
          console=True )
