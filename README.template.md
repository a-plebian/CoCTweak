{%- set repo_uri = "https://gitlab.com/Anonymous-BCFED/CoCTweak" -%}
{%- set support = [
  ['Vanilla',             'Fenoxo',           'Y', 'Y', '?',      'https://github.com/OXOIndustries/Corruption-of-Champions'],
  ['HGG',                 'OCA/OtherCoCAnon', 'Y', 'Y', 'FULL',   'https://gitgud.io/BelshazzarII/CoCAnon_mod'],
  ['Endless Journey',     'Oxdeception',      'N', 'N', 'BROKEN', 'https://github.com/Oxdeception/Corruption-of-Champions'],
  ['UEE',                 'Kitteh6660' ,      'Y', 'Y', 'WIP',    'https://github.com/Kitteh6660/Corruption-of-Champions-Mod'],
] -%}
# CoCTweak

A thorough, mod-friendly save editor with a multitude of console commands. Built with Python 3 and mini-amf, so it works on Linux and Windows, as well.

## A Note

As a save editor, CoCTweak is not supported by the developers of CoC.  By using CoCTweak, you can potentially break saves and trigger bugs.  **You use CoCTweak at your own risk.**

So, **don't bug CoC devs about bugs triggered by CoCTweak** in CoC.  They aren't responsible, and can't fix our problems anyway.

## Mod Support
Most development is done against the HGG mod of CoC for purely preferential reasons.

Saves are (mostly) parsed by automatically-generated code, so there (hopefully) won't be too many issues when upgrading.

**You are welcome to file issue reports and merge requests if you'd like to help out**

<table><thead><th>Mod</th><th>Author</th><th>Read</th><th>Write</th><th>Modding</th></thead>
<tbody>
{%- for tr in support -%}
<tr><th>
{%- if tr[5] == null -%}
  {{- tr[0] -}}
{%- else -%}
  [{{- tr[0] -}}]({{- tr[5] -}})
{%- endif -%}
</th><td>{{ tr[1] }}</td><td>{{ tr[2] }}</td><td>{{ tr[3] }}</td><td>{{ tr[4] }}</td></tr>
{%- endfor -%}
</tbody></table>

## Installing

### Binaries

Pre-compiled, binary executables are available at the GitLab [Releases]({{- repo_uri -}}/-/releases) page to make use less complicated for Windows.

Binaries are also available for Ubuntu Linux. The Ubuntu Linux binaries will *probably* work on Debian, but other distros have stuff in other locations and are not supported at this time. coctweak may or may not work on these platforms.

To install, just extract to a folder and run in your command line console.

### From Source
* Make sure [Python](https://python.org) &gt;=3.6 is installed.
* You'll also need [pip](https://pip.pypa.io/en/stable/installing/), if it's not already installed with Python.
* You will need `git` installed, too. [Official site](https://git-scm.com/), you may use other packaged editions, though, like TortoiseGit, SourceTree, etc.

```shell
# Check out the code to disk.
git clone https://gitlab.com/Anonymous-BCFED/CoCTweak.git coctweak
cd coctweak
# Install things we need to run
pip install -r requirements.txt
```

## Updating

### Binary Releases

1. Download a new Release.
1. Extract and overwrite the previous files.

## Help output

{{ cmd([COCTWEAK, '--help'], echo=True) }}

## Examples

### List Saves

{{ cmd([COCTWEAK, 'ls'], echo=True, comment='List hostnames in the Shared Objects Library.') }}
{{ cmd([COCTWEAK, 'ls', 'localhost'], echo=True, comment='List all available saves in the `localhost` hostname.') }}
{{ cmd([COCTWEAK, 'ls', 'localhost', '--format=coc'], echo=True, comment='Same as above, but with the same layout as CoC\'s loading menu.') }}


### Display Save

{{ cmd([COCTWEAK, 'show', 'localhost', '5'], echo=True, comment='Display some more detailed info about localhost 5 (CoC_5.sol).') }}

### Heal Thyself

{{ cmd([COCTWEAK, 'heal', 'localhost', '1', '--all'], echo=True, comment='Heal thyself.  --all implies --fatigue, --hunger, and --lust') }}

{%- do reset_slot(1) %}

## Commands
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- backup --></inline-elem>[coctweak backup](docs/commands/backup.md)
* <inline-elem><!-- body --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; ...](docs/commands/body/index.md)
* <inline-elem><!-- changes --></inline-elem>[coctweak changes &lt;host&gt; &lt;id&gt;](docs/commands/changes.md)
* <inline-elem><!-- compare --></inline-elem>[coctweak compare &lt;host_a&gt; &lt;id_a&gt; &lt;host_b&gt; &lt;id_b&gt;](docs/commands/compare.md)
* <inline-elem><!-- copy --></inline-elem>[coctweak (copy|cp|duplicate|dupe|clone) &lt;orig_host&gt; &lt;orig_id&gt; &lt;new_host&gt; &lt;new_id&gt;](docs/commands/copy.md)
* <inline-elem><!-- export --></inline-elem>[coctweak export &lt;host&gt; &lt;id&gt; &lt;filename.yml&gt;](docs/commands/export.md)
* <inline-elem><!-- heal --></inline-elem>[coctweak heal &lt;host&gt; &lt;id&gt;](docs/commands/heal.md)
* <inline-elem><!-- import --></inline-elem>[coctweak import &lt;filename.yml&gt; &lt;host&gt; &lt;id&gt;](docs/commands/import.md)
* <inline-elem><!-- inventory --></inline-elem>[coctweak (inventory|inv) &lt;host&gt; &lt;id&gt; ...](docs/commands/inventory/index.md)
* <inline-elem><!-- keyitem --></inline-elem>[coctweak keyitem &lt;host&gt; &lt;id&gt; ...](docs/commands/keyitem/index.md)
* <inline-elem><!-- list --></inline-elem>[coctweak (list|ls) &lsqb;&lt;host&gt; &lsqb;&lt;id&gt;&rsqb;&rsqb;](docs/commands/list.md)
* <inline-elem><!-- move --></inline-elem>[coctweak (move|mv) &lt;old_host&gt; &lt;old_id&gt; &lt;new_host&gt; &lt;new_id&gt;](docs/commands/move.md)
* <inline-elem><!-- perk --></inline-elem>[coctweak perk &lt;host&gt; &lt;id&gt;](docs/commands/perk/index.md)
* <inline-elem><!-- rename --></inline-elem>[coctweak rename &lt;host&gt; &lt;old_id&gt; &lt;new_id&gt;](docs/commands/rename.md)
* <inline-elem><!-- restore --></inline-elem>[coctweak restore](docs/commands/restore.md)
* <inline-elem><!-- set --></inline-elem>[coctweak set &lt;host&gt; &lt;id&gt; ...](docs/commands/set/index.md)
* <inline-elem><!-- show --></inline-elem>[coctweak (show|info) &lt;host&gt; &lt;id&gt;](docs/commands/show.md)
* <inline-elem><!-- statuseffect --></inline-elem>[coctweak (statuseffect|sfx) &lt;host&gt; &lt;id&gt;](docs/commands/statuseffect/index.md)
* coctweak get &lt;host&gt; &lt;id&gt; ...
