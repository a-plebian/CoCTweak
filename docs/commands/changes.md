# Changes Command

Track changes to a save between HEAD and a given commit via `git diff`.

## Command Syntax

```
usage: coctweak changes [-h] [--since-commit SINCE_COMMIT]
                        [--commits-back COMMITS_BACK]
                        host id

positional arguments:
  host                  Hostname the save is stored in
  id                    ID of the save, or Main for the Permanent Object file.

optional arguments:
  -h, --help            show this help message and exit
  --since-commit SINCE_COMMIT
                        Commit to use as the starting point
  --commits-back COMMITS_BACK
                        How many commits back
```

## Example

```shell
$ ./coctweak.sh backup
```

```diff
$ git diff --color=always HEAD~ localhost/CoC_1.sol/CoC_1.yml
diff --git a/localhost/CoC_1.sol/CoC_1.yml b/localhost/CoC_1.sol/CoC_1.yml
index cf01798..1850d5d 100644
--- a/localhost/CoC_1.sol/CoC_1.yml
+++ b/localhost/CoC_1.sol/CoC_1.yml
@@ -1,4 +1,4 @@
-HP: 840
+HP: 985
 XP: 224
 a: 'a '
 age: 0
@@ -1272,7 +1272,7 @@ lipPierced: 0
 lowerBody: 16
 lowerGarment:
   id: nounder
-lust: 3.896785714285714
+lust: 0.0
 masteries:
 - id: Fist
   level: 2
```
