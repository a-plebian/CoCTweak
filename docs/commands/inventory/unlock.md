# Inventory > Unlock Command

This powerful command permits you to change how many slots you've unlocked.

**WARNING:** This command currently only works for the pc/player inventory key. Chests will come later, if possible.

## How it works

`unlock` currently modifies your backpack's size so that, when added to the rest of the available slots, the desired number of slots are calculated.

For instance, if you have Strong Back and Strong Back 2, each of those perks counts for 1 slot. So, given that 3+1+1, you would have five slots available.
If you asked CoCTweak to ensure 10 slots were available, your backpack would be set to 10-(3+1+1), or 5. The end result is 3+1+1+5 = 10 character slots.

## Command Syntax
```
usage: coctweak inventory host id unlock [-h] inventory_key {#,auto,all}

positional arguments:
  inventory_key  Inventory key to modify. See inv list.
  {#,auto,all}   How many slots to unlock. "all" will unlock all slots, "auto"
                 will reset back to the game's default logic.

optional arguments:
  -h, --help     show this help message and exit

```
## Example

```shell
$ coctweak inventory localhost 5 unlock player all
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Using backpack hack to unlock 10 slots...
  Old backpack size: 0
  Recalculating inventory slot locks...
    Save has an override of 10 slots set, made backpack of 7 size.
    Calculated unlocked slot count: 10
    Capped unlocked slot count: 10
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
```


```shell
$ coctweak inventory localhost 5 unlock player 7
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Using backpack hack to unlock 7 slots...
  Old backpack size: 7
  Recalculating inventory slot locks...
    Save has an override of 7 slots set, made backpack of 4 size.
    Calculated unlocked slot count: 7
    Capped unlocked slot count: 7
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
```


```shell
$ coctweak inventory localhost 5 unlock player auto
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Removing backpack hack and restoring normal slot operation.
  WARNING: Backup backpack size is None, so we may have already removed the hack. Or the save is fucked.
  Recalculating inventory slot locks...
    Calculated unlocked slot count: 7
    Capped unlocked slot count: 7
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
```
