# Inventory > Sort Command

This tool allows you to sort an inventory collection by ID or value.

Value for each item type is taken from `data/{MOD ID}/items-merged.min.json`, if it exists.

Human-readable versions are available for:
* [HGG](docs/hgg/items.md)

## Command Syntax
```
usage: coctweak inventory host id sort [-h] [--by {id,value}] inventory_key

positional arguments:
  inventory_key    Inventory key to modify. See inv list.

optional arguments:
  -h, --help       show this help message and exit
  --by {id,value}  How to sort the list.

```

## Example

```shell
$ coctweak inventory localhost 6 sort player
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_6.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Loading inventory items from Player...
Sorting...
Storing...
  [0]     Icicle  x 1  ->    B. Book x 1 
  [1]     B. Book x 1  ->    C.Cloth x 1 
  [2]     C.Cloth x 1  ->    DryTent x 2 
  [3]     DryTent x 2  ->    Icicle  x 1 
  [4]     TScroll x 1  ->    KangaFt x 1 
  [5]     MinoCum x 2  ->    MinoCum x 2 
  [6]     KangaFt x 1  ->    TScroll x 1 
  [7]      [Empty]     ->     [Empty]    
  [8]      [Empty]     ->     [Empty]    
  [9]      [Empty]     ->     [Empty]    
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_6.sol...
```


```shell
$ coctweak inventory localhost 6 sort chest
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_6.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Loading inventory items from Chests...
Sorting...
Storing...
  [0]     BulbyPp x 2  ->    BulbyPp x 2 
  [1]     D.Scale x 1  ->    D.Scale x 1 
  [2]     DblPepp x 10 ->    DblPepp x 10
  [3]     Equinum x 10 ->    Equinum x 10
  [4]     GroPlus x 8  ->    GroPlus x 8 
  [5]     Hummus  x 7  ->    Hummus  x 7 
  [6]     KnottyP x 10 ->    KnottyP x 10
  [7]     L.BluEg x 1  ->    L.BluEg x 1 
  [8]     LargePp x 9  ->    LargePp x 9 
  [9]     LifeRng x 1  ->    LifeRng x 1 
  [10]    MinoBlo x 10 ->    MinoBlo x 10
  [11]    NPnkEgg x 6  ->    NPnkEgg x 6 
  [12]    P.Draft x 2  ->    P.Draft x 2 
  [13]    PowrRng x 1  ->    PowrRng x 1 
  [14]    Reducto x 3  ->    Reducto x 3 
  [15]    SpHoney x 7  ->    SpHoney x 7 
  [16]    TrapOil x 8  ->    TrapOil x 8 
  [17]    Wolf Pp x 10 ->    Wolf Pp x 10
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_6.sol...
```
