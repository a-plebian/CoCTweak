# Inventory > Add Command

This command will add a given item to the indicated inventory target. It will attempt to combine
items into stacks like the game does, but may go over stack limits for some objects due to the
dumb nature of the save editor and its ignorance of which items have what stack size.

## Command Syntax
```
usage: coctweak inventory host id add [-h] inventory_key item_id count

positional arguments:
  inventory_key  Inventory key to modify. See inv list.
  item_id        Item ID. Invalid IDs will break your save!
  count          How many items to add.

optional arguments:
  -h, --help     show this help message and exit

```

## Example

```shell
$ coctweak inventory localhost 1 add player Equinum 1
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Slot 2: (empty) -> 1
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
```
