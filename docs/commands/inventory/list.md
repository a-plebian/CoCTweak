# Inventory > List Command

The `list` subcommand will list all available inventory keys for the given save.


## Command Syntax

```
usage: coctweak inventory host id list [-h]

optional arguments:
  -h, --help  show this help message and exit

```

## Example

```shell
$ coctweak inventory localhost 1 list
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Available inventory keys: (use any listed key)
  player, pc
  weaponrack, weapon rack, weapons
  armorrack, armor rack, armor, armors
  jewelrybox, jewelrybox, jewelry
  dresser, clothes, clothing
  shieldrack, shield rack, shield, shields
  itemstorage, chest, chests
```
