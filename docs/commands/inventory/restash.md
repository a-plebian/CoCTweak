# Inventory > Clear Command

This command will remove all items from your camp storage (chests, racks) and then attempt to re-stash them.  The intent of this command
is to help with re-organizing your inventory after you find a rack or other filtered inventory, and allows you to quickly move stuff
from your chests to the appropriate racks.

## Command Syntax

```
usage: coctweak inventory host id restash [-h]

optional arguments:
  -h, --help  show this help message and exit

```

## Example

```shell
$ coctweak inventory localhost 1 restash
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Gathering from camp storage...
  Clearing Weapon Rack...
    Slot 1: W.Staff x1 -> (empty)
    Slot 2: B.Sword x1 -> (empty)
    Slot 3: Blunder x1 -> (empty)
    Slot 4: Spear   x1 -> (empty)
    Slot 5: L. Axe  x1 -> (empty)
    Slot 6: H.Gaunt x1 -> (empty)
    Slot 7: Flintlk x1 -> (empty)
  Clearing Armor Rack...
    Slot 1: FullChn x1 -> (empty)
    Slot 2: W.Robes x1 -> (empty)
    Slot 3: SamuArm x1 -> (empty)
    Slot 4: NNunHab x1 -> (empty)
    Slot 5: GelArmr x1 -> (empty)
    Slot 6: C.Cloth x1 -> (empty)
    Slot 7: S.Swmwr x1 -> (empty)
  Clearing Jewelry Box...
    Slot 1: LifeRng x1 -> (empty)
    Slot 2: PowrRng x1 -> (empty)
  Clearing Dresser...
    Slot 1: C. Loin x1 -> (empty)
    Slot 2: LtxShrt x1 -> (empty)
  Clearing Shield Rack...
    Slot 1: WoodShl x1 -> (empty)
    Slot 2: ClShield x1 -> (empty)
    Slot 3: GreatSh x1 -> (empty)
  Clearing Chests...
    Slot 1: BulbyPp x2 -> (empty)
    Slot 2: D.Scale x1 -> (empty)
    Slot 3: DblPepp x10 -> (empty)
    Slot 4: Equinum x10 -> (empty)
    Slot 5: GroPlus x8 -> (empty)
    Slot 6: Hummus  x10 -> (empty)
    Slot 7: KnottyP x10 -> (empty)
    Slot 8: L.BluEg x1 -> (empty)
    Slot 9: LargePp x10 -> (empty)
    Slot 10: MinoBlo x10 -> (empty)
    Slot 11: NPnkEgg x6 -> (empty)
    Slot 12: P.Draft x4 -> (empty)
    Slot 13: P.S.Mlk x4 -> (empty)
    Slot 14: PSDelit x2 -> (empty)
    Slot 15: Reducto x3 -> (empty)
    Slot 16: SpHoney x7 -> (empty)
    Slot 17: TrapOil x9 -> (empty)
    Slot 18: Wolf Pp x10 -> (empty)
Stashing items...
  Stored 1 x W.Staff in Weapon Rack.
  Stored 1 x B.Sword in Weapon Rack.
  Stored 1 x Blunder in Weapon Rack.
  Stored 1 x Spear   in Weapon Rack.
  Stored 1 x L. Axe  in Weapon Rack.
  Stored 1 x H.Gaunt in Weapon Rack.
  Stored 1 x Flintlk in Weapon Rack.
  Stored 1 x FullChn in Armor Rack.
  Stored 1 x W.Robes in Armor Rack.
  Stored 1 x SamuArm in Armor Rack.
  Stored 1 x NNunHab in Armor Rack.
  Stored 1 x GelArmr in Armor Rack.
  Stored 1 x C.Cloth in Armor Rack.
  Stored 1 x S.Swmwr in Armor Rack.
  Stored 1 x LifeRng in Jewelry Box.
  Stored 1 x PowrRng in Jewelry Box.
  Stored 1 x C. Loin in Dresser.
  Stored 1 x LtxShrt in Dresser.
  Stored 1 x WoodShl in Shield Rack.
  Stored 1 x ClShield in Shield Rack.
  Stored 1 x GreatSh in Shield Rack.
  Stored 2 x BulbyPp in Chests.
  Stored 1 x D.Scale in Chests.
  Stored 10 x DblPepp in Chests.
  Stored 10 x Equinum in Chests.
  Stored 8 x GroPlus in Chests.
  Stored 10 x Hummus  in Chests.
  Stored 10 x KnottyP in Chests.
  Stored 1 x L.BluEg in Chests.
  Stored 10 x LargePp in Chests.
  Stored 10 x MinoBlo in Chests.
  Stored 6 x NPnkEgg in Chests.
  Stored 4 x P.Draft in Chests.
  Stored 4 x P.S.Mlk in Chests.
  Stored 2 x PSDelit in Chests.
  Stored 3 x Reducto in Chests.
  Stored 7 x SpHoney in Chests.
  Stored 9 x TrapOil in Chests.
  Stored 10 x Wolf Pp in Chests.
Weapon Rack:
  *  1:    W.Staff x 1 
  *  2:    B.Sword x 1 
  *  3:    Blunder x 1 
  *  4:    Spear   x 1 
  *  5:    L. Axe  x 1 
  *  6:    H.Gaunt x 1 
  *  7:    Flintlk x 1 
  *  8:     [Empty]    
  *  9:     [Empty]    
Armor Rack:
  *  1:    FullChn x 1 
  *  2:    W.Robes x 1 
  *  3:    SamuArm x 1 
  *  4:    NNunHab x 1 
  *  5:    GelArmr x 1 
  *  6:    C.Cloth x 1 
  *  7:    S.Swmwr x 1 
  *  8:     [Empty]    
  *  9:     [Empty]    
Jewelry Box:
  *  1:    LifeRng x 1 
  *  2:    PowrRng x 1 
  *  3:     [Empty]    
  *  4:     [Empty]    
  *  5:     [Empty]    
  *  6:     [Empty]    
  *  7:     [Empty]    
  *  8:     [Empty]    
  *  9:     [Empty]    
Dresser:
  *  1:    C. Loin x 1 
  *  2:    LtxShrt x 1 
  *  3:     [Empty]    
  *  4:     [Empty]    
  *  5:     [Empty]    
  *  6:     [Empty]    
  *  7:     [Empty]    
  *  8:     [Empty]    
  *  9:     [Empty]    
Shield Rack:
  *  1:    WoodShl x 1 
  *  2:   ClShield x 1 
  *  3:    GreatSh x 1 
  *  4:     [Empty]    
  *  5:     [Empty]    
  *  6:     [Empty]    
  *  7:     [Empty]    
  *  8:     [Empty]    
  *  9:     [Empty]    
Chests:
  *  1:    BulbyPp x 2 
  *  2:    D.Scale x 1 
  *  3:    DblPepp x 10
  *  4:    Equinum x 10
  *  5:    GroPlus x 8 
  *  6:    Hummus  x 10
  *  7:    KnottyP x 10
  *  8:    L.BluEg x 1 
  *  9:    LargePp x 10
  * 10:    MinoBlo x 10
  * 11:    NPnkEgg x 6 
  * 12:    P.Draft x 4 
  * 13:    P.S.Mlk x 4 
  * 14:    PSDelit x 2 
  * 15:    Reducto x 3 
  * 16:    SpHoney x 7 
  * 17:    TrapOil x 9 
  * 18:    Wolf Pp x 10
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
```
