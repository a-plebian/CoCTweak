# Inventory > Set Command

This powerful command permits advanced editing of your inventory on a slot-by-slot basis.

## Command Syntax

```
usage: coctweak inventory host id set [-h] [--item ITEM] [--count COUNT]
                                      [--damage DAMAGE]
                                      inventory_key slot

positional arguments:
  inventory_key         Inventory key to modify. See inv list.
  slot                  Slot ID, from 1 - n.

optional arguments:
  -h, --help            show this help message and exit
  --item ITEM, -i ITEM  Item ID. Invalid IDs will break your save!
  --count COUNT, -c COUNT
                        How many items in that slot.
  --damage DAMAGE, -D DAMAGE
                        Damage/wear of the item. Not implemented in Vanilla
                        and some mods.

```

## Example

```shell
# Set slot 1 in the player inventory to be 10 black peppers:
$ coctweak inventory localhost 1 set player 1 --item=BlackPp --count=10
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Inventory slot #1:
  ID: AkbalSl -> BlackPp
  Count: 2 -> 10
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
```
