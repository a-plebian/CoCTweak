# Inventory > Clear Command

This command will **irreversibly** remove all items in the indicated inventory.

Invoke `./coctweak.sh backup` before continuing, just in case!

## Command Syntax

```
usage: coctweak inventory host id clear [-h] inventory_key

positional arguments:
  inventory_key  Inventory key to modify. See inv list.

optional arguments:
  -h, --help     show this help message and exit

```

## Example

```shell
$ coctweak inventory localhost 1 clear player
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Slot 1: AkbalSl x2 -> (empty)
Slot 8: B.Gossr x1 -> (empty)
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
```
