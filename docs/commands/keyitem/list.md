# Key Items > List Command

Display a list of all active Key Items, plus value information.

## Command Syntax
```shell
$ coctweak keyitem localhost 1 ls --help
```
```
usage: coctweak keyitem host id list [-h]
                                     [--format {ascii,markdown,json,yaml}]

optional arguments:
  -h, --help            show this help message and exit
  --format {ascii,markdown,json,yaml}, -f {ascii,markdown,json,yaml}
                        Format of display.
```



## Example
```shell
# Show all keyitems currently held
$ coctweak keyitem localhost 1 ls
```
```
Arian's Talisman
Backpack
Bow
Camp - Chest
Camp - Murky Chest
Camp - Ornate Chest
Carpenter's Nail Box
Carpenter's Toolbox
Dangerous Plants
Deluxe Dildo
Equipment Rack - Armor
Equipment Rack - Shields
Equipment Rack - Weapons
Equipment Storage - Jewelry Box
Gym Membership
Iron Key
Onyx Token - Ceraph's
Supervisor's Key
Traveler's Guide
Zetaz's Map
```

```shell
# Same, but in Markdown
$ coctweak keyitem localhost 1 ls --format=markdown
```
```markdown
 * Arian's Talisman
 * Backpack
 * Bow
 * Camp - Chest
 * Camp - Murky Chest
 * Camp - Ornate Chest
 * Carpenter's Nail Box
 * Carpenter's Toolbox
 * Dangerous Plants
 * Deluxe Dildo
 * Equipment Rack - Armor
 * Equipment Rack - Shields
 * Equipment Rack - Weapons
 * Equipment Storage - Jewelry Box
 * Gym Membership
 * Iron Key
 * Onyx Token - Ceraph's
 * Supervisor's Key
 * Traveler's Guide
 * Zetaz's Map
```

```shell
# And in JSON
$ coctweak keyitem localhost 1 ls --format=json
```
```json
{
  "Arian's Talisman": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Backpack": {
    "values": [
      5,
      0,
      0,
      0
    ]
  },
  "Bow": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Camp - Chest": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Camp - Murky Chest": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Camp - Ornate Chest": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Carpenter's Nail Box": {
    "values": [
      400,
      0,
      0,
      0
    ]
  },
  "Carpenter's Toolbox": {
    "values": [
      600,
      0,
      0,
      0
    ]
  },
  "Dangerous Plants": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Deluxe Dildo": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Equipment Rack - Armor": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Equipment Rack - Shields": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Equipment Rack - Weapons": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Equipment Storage - Jewelry Box": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Gym Membership": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Iron Key": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Onyx Token - Ceraph's": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Supervisor's Key": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Traveler's Guide": {
    "values": [
      0,
      0,
      0,
      0
    ]
  },
  "Zetaz's Map": {
    "values": [
      0,
      0,
      0,
      0
    ]
  }
}
```

```shell
# And finally in YAML
$ coctweak keyitem localhost 1 ls --format=yaml
```
```yaml
Arian's Talisman:
  values:
  - 0
  - 0
  - 0
  - 0
Backpack:
  values:
  - 5
  - 0
  - 0
  - 0
Bow:
  values:
  - 0
  - 0
  - 0
  - 0
Camp - Chest:
  values:
  - 0
  - 0
  - 0
  - 0
Camp - Murky Chest:
  values:
  - 0
  - 0
  - 0
  - 0
Camp - Ornate Chest:
  values:
  - 0
  - 0
  - 0
  - 0
Carpenter's Nail Box:
  values:
  - 400
  - 0
  - 0
  - 0
Carpenter's Toolbox:
  values:
  - 600
  - 0
  - 0
  - 0
Dangerous Plants:
  values:
  - 0
  - 0
  - 0
  - 0
Deluxe Dildo:
  values:
  - 0
  - 0
  - 0
  - 0
Equipment Rack - Armor:
  values:
  - 0
  - 0
  - 0
  - 0
Equipment Rack - Shields:
  values:
  - 0
  - 0
  - 0
  - 0
Equipment Rack - Weapons:
  values:
  - 0
  - 0
  - 0
  - 0
Equipment Storage - Jewelry Box:
  values:
  - 0
  - 0
  - 0
  - 0
Gym Membership:
  values:
  - 0
  - 0
  - 0
  - 0
Iron Key:
  values:
  - 0
  - 0
  - 0
  - 0
Onyx Token - Ceraph's:
  values:
  - 0
  - 0
  - 0
  - 0
Supervisor's Key:
  values:
  - 0
  - 0
  - 0
  - 0
Traveler's Guide:
  values:
  - 0
  - 0
  - 0
  - 0
Zetaz's Map:
  values:
  - 0
  - 0
  - 0
  - 0

```
