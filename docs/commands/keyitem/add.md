# Key Items > Add Command

Add a keyitem with this command.

**Note:** This command will not set flags or adjust stats like the game would.

## Command Syntax
```shell
$ coctweak keyitem localhost 5 add --help
```
```
usage: coctweak keyitem host id add [-h]
                                    keyitem_id value1 value2 value3 value4

positional arguments:
  keyitem_id  Key Item ID. See the listing of available Key Items for your
              mod.
  value1      Value 1, used as data storage in some key items.
  value2      Value 2, used as data storage in some key items.
  value3      Value 3, used as data storage in some key items.
  value4      Value 4, used as data storage in some key items.

optional arguments:
  -h, --help  show this help message and exit
```


## Example
```shell
# List current keyitems
$ coctweak keyitem localhost 5 ls
```
```
<No key items set>
```

```shell
# Add camp chest
$ coctweak keyitem localhost 5 add "Camp - Chest" 0 0 0 0
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
```

```shell
# Check if we affected anything.
$ coctweak keyitem localhost 5 ls
```
```
Camp - Chest
```


