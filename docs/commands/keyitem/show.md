# Key Items > Show Command

Shows a Key Item, plus any known interpretations of its values.

## Command Syntax
```shell
$ coctweak keyitem localhost 1 show --help
```
```
usage: coctweak keyitem host id show [-h] keyitem_id

positional arguments:
  keyitem_id  ID of the key item. Use quotes if it has a space!

optional arguments:
  -h, --help  show this help message and exit
```


## Example
```shell
# Show information about Carpenter's Nail Box
$ coctweak keyitem localhost 1 show "Carpenter's Nail Box"
```
```
Carpenter's Nail Box
  []
Flags:
Values:
  value1 (Value 1): 400
  value2 (Value 2): 0
  value3 (Value 3): 0
  value4 (Value 4): 0
```
