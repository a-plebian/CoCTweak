# Heal Command
a
The heal command repairs your HP, and optionally, your lust, fatigue, and hunger.

**NOTE:** The maximums you are healed to are _very_ rough guesses.

## Command Syntax

```
usage: coctweak heal [-h] [--no-hp] [--lust] [--fatigue] [--hunger]
                     [--parasites] [--status-effects] [--all]
                     host id

positional arguments:
  host              Flash hostname the save will be stored in. Use
                    @localWithNet instead of #localWithNet, if you're on
                    Linux.
  id                ID of the save.

optional arguments:
  -h, --help        show this help message and exit
  --no-hp           Do NOT modify HP.
  --lust            Set lust to 0.
  --fatigue         Set fatigue to 0.
  --hunger          Set hunger to 0.
  --parasites       Clear parasites (in mods that support parasites).
  --status-effects  Clear bad status effects (may be outdated).
  --all             FIX EVERYTHING (except --parasites and --status-effects).

```

## Example

```shell
$ coctweak heal localhost 1 --all
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Stats:
  Health: 861.0 -> 863
  Lust: 13.919999999999995 -> 0
  Fatigue: 0.0 -> 0
  Hunger: 80.0 -> 100
Parasites (Diagnosis only; fix with --parasites):
  Not infected!
Status Effects (Diagnosis only; fix with --status-effects):
  Not infected!
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
```


