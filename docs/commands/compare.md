# Compare Command

Compare converts two saves to YAML, and then executes diff in unified mode so you can peruse the differences.

## Command Syntax

```
usage: coctweak compare [-h]
                        left_hostname left_slotid right_hostname right_slotid

positional arguments:
  left_hostname   Hostname of left save.
  left_slotid     Slot ID of left save.
  right_hostname  Hostname of right save.
  right_slotid    Slot ID of right save.

optional arguments:
  -h, --help      show this help message and exit
```

## Example

```shell
$ ./coctweak.sh backup
```

```diff
$ diff -u --color=always /tmp/tmpl3xod50u/CoC_2.yml /tmp/tmpl3xod50u/CoC_3.yml
--- /tmp/tmpl3xod50u/CoC_2.yml  2019-06-30 19:11:54.791370782 -0700
+++ /tmp/tmpl3xod50u/CoC_3.yml  2019-06-30 19:11:54.851370277 -0700
@@ -1,5 +1,5 @@
-HP: 69.00000000000001
-XP: 951                                                                                                                                                                                                                                                                       
+HP: 170
+XP: 1061                                                                                                                                                                                                                                                                      
 a: 'a '
 age: 0
 antennae: 0
```
