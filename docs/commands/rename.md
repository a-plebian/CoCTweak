# Rename Command

The rename command allows you to rename a save within the same Flash SharedObject host. For example, you could rename CoC_1.sol to CoC_5.sol.

## Command Syntax

```
usage: coctweak rename [-h] [--force] host src_id dest_id

positional arguments:
  host        Hostname the save is stored in
  src_id      ID of the original save.
  dest_id     ID slot to rename it to. Must not exist.

optional arguments:
  -h, --help  show this help message and exit
  --force     If dest_id exists, overwrite it.
```

## Example

```shell
$ ./coctweak.sh rename localhost 10 11
```

```
CoCTweak v0.0.1
___________________________________________________________________

(c)2019 Anonymous-BCFED. Available under the MIT Open Source License.

Moving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_10.sol to ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_11.sol...
  OK!
```
