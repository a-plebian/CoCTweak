# Backup Command

This command will restore the *entire* backed-up Flash Shared Objects Library that's stored in backups/ to the correct directory.  Useful for when Flash mysteriously nukes it.

Note that this command will ONLY read the .sol binaries, unless told to use other sources directly.

## Command Syntax

```
usage: coctweak restore [-h] [-Y] [--clobber] [--clean-slate]

optional arguments:
 -h, --help     show this help message and exit
 -Y, --yaml     Restore from YAML rather than the binary .sol files.
 --clobber      Overwrite existing files.
 --clean-slate  Destroy existing Macromedia Shared Objects Library, first.
                Irreversible.
```

## Example

```shell
$ ./coctweak.sh restore
```

```
Skipping 8ch.net/CoC_1.sol/CoC_1.yml
Skipping localhost/CoC_5.sol/CoC_5.yml
Skipping localhost/CoC_4.sol/CoC_4.yml
Skipping localhost/#CoC/EndlessJourney/CoC_10.sol/CoC_10.yml
Skipping localhost/CoC_10.sol/CoC_10.yml
Skipping localhost/CoC_9.sol/CoC_9.yml
Skipping localhost/CoC_6.sol/CoC_6.yml
Skipping localhost/CoC_Main.sol/CoC_Main.yml
Skipping localhost/CoC_1.sol/CoC_1.yml
Skipping localhost/CoC_11.sol/CoC_11.yml
Skipping localhost/CoC_3.sol/CoC_3.yml
Skipping localhost/CoC_2.sol/CoC_2.yml
Skipping #localWithNet/CoC_5.sol/CoC_5.yml
Skipping #localWithNet/CoC_4.sol/CoC_4.yml
Skipping #localWithNet/CoC_13.sol/CoC_13.yml
Skipping #localWithNet/CoC_Main.sol/CoC_Main.yml
Skipping #localWithNet/CoC_1.sol/CoC_1.yml
Skipping #localWithNet/CoC_3.sol/CoC_3.yml
Skipping #localWithNet/CoC_2.sol/CoC_2.yml
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/8ch.net/CoC_1.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/8ch.net/CoC_1.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_5.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_5.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_4.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_4.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/#CoC/EndlessJourney/CoC_10.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/#CoC/EndlessJourney/CoC_10.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_10.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_10.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_9.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_9.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_6.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_6.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_1.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_1.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_11.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_11.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_3.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_3.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_2.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_2.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/#localWithNet/CoC_5.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/#localWithNet/CoC_5.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/#localWithNet/CoC_4.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/#localWithNet/CoC_4.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/#localWithNet/CoC_13.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/#localWithNet/CoC_13.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/#localWithNet/CoC_1.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/#localWithNet/CoC_1.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/#localWithNet/CoC_3.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/#localWithNet/CoC_3.sol...
WARN: Clobbering ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/#localWithNet/CoC_2.sol.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/#localWithNet/CoC_2.sol...
```
