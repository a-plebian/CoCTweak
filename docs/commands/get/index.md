# Get Command

This command permits getting certain specifics of the game state.

## Help Listing

```shell
$ coctweak get localhost 1 -h
```
```
usage: coctweak get [-h] host id {notes,stat,gems} ...

positional arguments:
  host               Flash hostname the save will be stored in. Use
                     @localWithNet instead of #localWithNet, if you're on
                     Linux.
  id                 ID of the save.
  {notes,stat,gems}
    notes            Get the notes for a save.
    stat             Get the value for a core stat.
    gems             Get the gems your character haves.

optional arguments:
  -h, --help         show this help message and exit
```


## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- notes --></inline-elem>[coctweak get &lt;host&gt; &lt;id&gt; notes ...](docs/commands/get/notes.md)
* <inline-elem><!-- stat --></inline-elem>[coctweak get &lt;host&gt; &lt;id&gt; stat ...](docs/commands/get/stat.md)