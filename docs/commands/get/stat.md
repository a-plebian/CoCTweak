# Get > Stat Command

This permits getting your character's stats directly.  Which stats are available depends upon your mod.

## Command Syntax

```shell
$ coctweak get localhost 1 stat -h
```
```
usage: coctweak get host id stat [-h] statID

positional arguments:
  statID      Stat ID

optional arguments:
  -h, --help  show this help message and exit
```


## Example

```shell
$ coctweak get localhost 1 stat STR
```
```
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
90.0
```
