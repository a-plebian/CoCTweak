# Get > Gems Command

This permits getting your character's gems directly.

## Command Syntax

```shell
$ coctweak get localhost 1 gems -h
```
```
usage: coctweak get host id gems [-h]

optional arguments:
  -h, --help  show this help message and exit
```


## Example

```shell
$ coctweak get localhost 1 gems
```
```
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
20632
```
