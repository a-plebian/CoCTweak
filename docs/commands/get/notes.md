# Get > Notes Command

You can get your save's notes with this command. These notes appear on the
save listings.

NOTE: To save people from having to make parsers, the header is automatically suppressed on `get` commands.

## Command Syntax

```shell
$ coctweak get localhost 1 notes -h
```
```
usage: coctweak get host id notes [-h]

optional arguments:
  -h, --help  show this help message and exit
```


## Example

```shell
$ coctweak get localhost 1 notes
```
```
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
No Notes Available.
```
