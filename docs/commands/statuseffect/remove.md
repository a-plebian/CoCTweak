# Status Effect > Remove Command

Remove a given status effect.

## Command Syntax
```shell
$ coctweak statuseffect localhost 1 remove --help
```
```
usage: coctweak statuseffect host id remove [-h] sfx_id

positional arguments:
  sfx_id      Status Effect ID. See the listing of available status effects
              for your mod.

optional arguments:
  -h, --help  show this help message and exit
```



## Example
```shell
# Remove the ButtStretched effect.
$ coctweak statuseffect localhost 1 remove ButtStretched
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Removing 'ButtStretched'...
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
```



