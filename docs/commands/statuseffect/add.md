# Status Effects > Add Command

Add a status effect with this command.

**Note:** This command will not set flags or adjust stats like the game would.

## Command Syntax
```shell
$ coctweak statuseffect localhost 5 add --help
```
```
usage: coctweak statuseffect host id add [-h]
                                         sfx_id value1 value2 value3 value4

positional arguments:
  sfx_id      Status Effect ID. See the listing of available status effects
              for your mod.
  value1      Value 1, used as data storage in some status effects.
  value2      Value 2, used as data storage in some status effects.
  value3      Value 3, used as data storage in some status effects.
  value4      Value 4, used as data storage in some status effects.

optional arguments:
  -h, --help  show this help message and exit
```


## Example
```shell
# List active status effects.
$ coctweak statuseffect localhost 5 ls
```
```
<No status effects set>
```

```shell
# Add Knows Charge status effect.
$ coctweak statuseffect localhost 5 add "Knows Charge" 0 0 0 0
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Adding new Status Effect quartet...
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
```

```shell
# List effects again.
$ coctweak statuseffect localhost 5 ls
```
```
Knows Charge
```


