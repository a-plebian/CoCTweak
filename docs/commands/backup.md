# Backup Command

This command goes through your Flash SharedObject library and backs up your saves, both in binary form, and in a decoded YAML format.

Backup also commits these saves to a local git repository so you can track changes, or revert to earlier saves.

NOTE: This will save all files that start with CoC_ and end with .sol, including currently-unsupported Xianxia/EJ saves.

## Command Syntax

```
usage: coctweak backup [-h] [-m MESSAGE]

optional arguments:
  -h, --help            show this help message and exit
  -m MESSAGE, --message MESSAGE
                        Set message.

```

## Example

```shell
$ coctweak backup
```
```
~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_3.sol:
  -> ~/coctweak/backup/localhost/CoC_3.sol/CoC_3.sol
  -> ~/coctweak/backup/localhost/CoC_3.sol/CoC_3.yml (HGG vnull (BUG))
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
  -> ~/coctweak/backup/localhost/CoC_Main.sol/CoC_Main.yml
~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_10.sol:
  -> ~/coctweak/backup/localhost/CoC_10.sol/CoC_10.sol
  WARNING: UEECock: Object using legacy version stamp!
  -> ~/coctweak/backup/localhost/CoC_10.sol/CoC_10.yml (UEE v1.0.2_mod_1.4.14)
~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol:
  -> ~/coctweak/backup/localhost/CoC_5.sol/CoC_5.sol
  -> ~/coctweak/backup/localhost/CoC_5.sol/CoC_5.yml (HGG vnull (BUG))
  (Skipping ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol)
~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_11.sol:
  -> ~/coctweak/backup/localhost/CoC_11.sol/CoC_11.sol
  -> ~/coctweak/backup/localhost/CoC_11.sol/CoC_11.yml (UEE v1.0.2_mod_1.4.17c)
~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_4.sol:
  -> ~/coctweak/backup/localhost/CoC_4.sol/CoC_4.sol
  -> ~/coctweak/backup/localhost/CoC_4.sol/CoC_4.yml (HGG vnull (BUG))
  (Skipping ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol)
~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol:
  -> ~/coctweak/backup/localhost/CoC_1.sol/CoC_1.sol
  -> ~/coctweak/backup/localhost/CoC_1.sol/CoC_1.yml (HGG v1.4.11)
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
  (Skipping ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol)
~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_6.sol:
  -> ~/coctweak/backup/localhost/CoC_6.sol/CoC_6.sol
  -> ~/coctweak/backup/localhost/CoC_6.sol/CoC_6.yml (HGG vnull (BUG))
  (Skipping ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol)
~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_7.sol:
  -> ~/coctweak/backup/localhost/CoC_7.sol/CoC_7.sol
  -> ~/coctweak/backup/localhost/CoC_7.sol/CoC_7.yml (HGG v1.4.13)
  (Skipping ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol)
~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_9.sol:
  -> ~/coctweak/backup/localhost/CoC_9.sol/CoC_9.sol
  -> ~/coctweak/backup/localhost/CoC_9.sol/CoC_9.yml (Vanilla v1.0.2)
~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_2.sol:
  -> ~/coctweak/backup/localhost/CoC_2.sol/CoC_2.sol
  -> ~/coctweak/backup/localhost/CoC_2.sol/CoC_2.yml (HGG vnull (BUG))
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
  (Skipping ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol)
~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/#CoC/EndlessJourney/CoC_10.sol:
  -> ~/coctweak/backup/localhost/#CoC/EndlessJourney/CoC_10.sol/CoC_10.sol
  WARNING: UEECock: Object using legacy version stamp!
  -> ~/coctweak/backup/localhost/#CoC/EndlessJourney/CoC_10.sol/CoC_10.yml (UEE v1.0.2_mod_1.4.14)
~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/#CoC/EndlessJourney/LastSaved.sol:
  -> ~/coctweak/backup/localhost/#CoC/EndlessJourney/LastSaved.sol/LastSaved.sol (RAW)
  -> ~/coctweak/backup/localhost/#CoC/EndlessJourney/LastSaved.sol/LastSaved.yml (RAW)
$ git init
Initialized empty Git repository in /tmp/coctweak-demo/coctweak/backup/.git/
WARNING: [git config] Setting user.name to nobody, and user.email to devnull@localhost.
WARNING: [git config] This will prevent attaching potentially sensitive information to your CoC backups. If you plan to host the git repo remotely, though, you will have to change this.
WARNING: [git config] To change, use `git config --local user.name value`, and the same with user.email.
$ git config --local user.name nobody
$ git config --local user.email devnull@localhost
$ git lfs install
Updated git hooks.
Git LFS initialized.
$ git lfs track *.sol
Tracking "*.sol"
$ git status --ignore-submodules --porcelain -uall
1: ['??', '.gitattributes']
2: ['??', '.gitignore']
3: ['??', 'localhost/#CoC/EndlessJourney/CoC_10.sol/CoC_10.sol']
4: ['??', 'localhost/#CoC/EndlessJourney/CoC_10.sol/CoC_10.yml']
5: ['??', 'localhost/#CoC/EndlessJourney/LastSaved.sol/LastSaved.sol']
6: ['??', 'localhost/#CoC/EndlessJourney/LastSaved.sol/LastSaved.yml']
7: ['??', 'localhost/CoC_1.sol/CoC_1.sol']
8: ['??', 'localhost/CoC_1.sol/CoC_1.yml']
9: ['??', 'localhost/CoC_10.sol/CoC_10.sol']
10: ['??', 'localhost/CoC_10.sol/CoC_10.yml']
11: ['??', 'localhost/CoC_11.sol/CoC_11.sol']
12: ['??', 'localhost/CoC_11.sol/CoC_11.yml']
13: ['??', 'localhost/CoC_2.sol/CoC_2.sol']
14: ['??', 'localhost/CoC_2.sol/CoC_2.yml']
15: ['??', 'localhost/CoC_3.sol/CoC_3.sol']
16: ['??', 'localhost/CoC_3.sol/CoC_3.yml']
17: ['??', 'localhost/CoC_4.sol/CoC_4.sol']
18: ['??', 'localhost/CoC_4.sol/CoC_4.yml']
19: ['??', 'localhost/CoC_5.sol/CoC_5.sol']
20: ['??', 'localhost/CoC_5.sol/CoC_5.yml']
21: ['??', 'localhost/CoC_6.sol/CoC_6.sol']
22: ['??', 'localhost/CoC_6.sol/CoC_6.yml']
23: ['??', 'localhost/CoC_7.sol/CoC_7.sol']
24: ['??', 'localhost/CoC_7.sol/CoC_7.yml']
25: ['??', 'localhost/CoC_9.sol/CoC_9.sol']
26: ['??', 'localhost/CoC_9.sol/CoC_9.yml']
27: ['??', 'localhost/CoC_Main.sol/CoC_Main.yml']
$ git rm --cache .gitattributes
fatal: pathspec '.gitattributes' did not match any files
$ git add .gitattributes
$ git rm --cache .gitignore
fatal: pathspec '.gitignore' did not match any files
$ git add .gitignore
$ git rm --cache localhost/#CoC/EndlessJourney/CoC_10.sol/CoC_10.sol
fatal: pathspec 'localhost/#CoC/EndlessJourney/CoC_10.sol/CoC_10.sol' did not match any files
$ git add localhost/#CoC/EndlessJourney/CoC_10.sol/CoC_10.sol
$ git rm --cache localhost/#CoC/EndlessJourney/CoC_10.sol/CoC_10.yml
fatal: pathspec 'localhost/#CoC/EndlessJourney/CoC_10.sol/CoC_10.yml' did not match any files
$ git add localhost/#CoC/EndlessJourney/CoC_10.sol/CoC_10.yml
$ git rm --cache localhost/#CoC/EndlessJourney/LastSaved.sol/LastSaved.sol
fatal: pathspec 'localhost/#CoC/EndlessJourney/LastSaved.sol/LastSaved.sol' did not match any files
$ git add localhost/#CoC/EndlessJourney/LastSaved.sol/LastSaved.sol
$ git rm --cache localhost/#CoC/EndlessJourney/LastSaved.sol/LastSaved.yml
fatal: pathspec 'localhost/#CoC/EndlessJourney/LastSaved.sol/LastSaved.yml' did not match any files
$ git add localhost/#CoC/EndlessJourney/LastSaved.sol/LastSaved.yml
$ git rm --cache localhost/CoC_1.sol/CoC_1.sol
fatal: pathspec 'localhost/CoC_1.sol/CoC_1.sol' did not match any files
$ git add localhost/CoC_1.sol/CoC_1.sol
$ git rm --cache localhost/CoC_1.sol/CoC_1.yml
fatal: pathspec 'localhost/CoC_1.sol/CoC_1.yml' did not match any files
$ git add localhost/CoC_1.sol/CoC_1.yml
$ git rm --cache localhost/CoC_10.sol/CoC_10.sol
fatal: pathspec 'localhost/CoC_10.sol/CoC_10.sol' did not match any files
$ git add localhost/CoC_10.sol/CoC_10.sol
$ git rm --cache localhost/CoC_10.sol/CoC_10.yml
fatal: pathspec 'localhost/CoC_10.sol/CoC_10.yml' did not match any files
$ git add localhost/CoC_10.sol/CoC_10.yml
$ git rm --cache localhost/CoC_11.sol/CoC_11.sol
fatal: pathspec 'localhost/CoC_11.sol/CoC_11.sol' did not match any files
$ git add localhost/CoC_11.sol/CoC_11.sol
$ git rm --cache localhost/CoC_11.sol/CoC_11.yml
fatal: pathspec 'localhost/CoC_11.sol/CoC_11.yml' did not match any files
$ git add localhost/CoC_11.sol/CoC_11.yml
$ git rm --cache localhost/CoC_2.sol/CoC_2.sol
fatal: pathspec 'localhost/CoC_2.sol/CoC_2.sol' did not match any files
$ git add localhost/CoC_2.sol/CoC_2.sol
$ git rm --cache localhost/CoC_2.sol/CoC_2.yml
fatal: pathspec 'localhost/CoC_2.sol/CoC_2.yml' did not match any files
$ git add localhost/CoC_2.sol/CoC_2.yml
$ git rm --cache localhost/CoC_3.sol/CoC_3.sol
fatal: pathspec 'localhost/CoC_3.sol/CoC_3.sol' did not match any files
$ git add localhost/CoC_3.sol/CoC_3.sol
$ git rm --cache localhost/CoC_3.sol/CoC_3.yml
fatal: pathspec 'localhost/CoC_3.sol/CoC_3.yml' did not match any files
$ git add localhost/CoC_3.sol/CoC_3.yml
$ git rm --cache localhost/CoC_4.sol/CoC_4.sol
fatal: pathspec 'localhost/CoC_4.sol/CoC_4.sol' did not match any files
$ git add localhost/CoC_4.sol/CoC_4.sol
$ git rm --cache localhost/CoC_4.sol/CoC_4.yml
fatal: pathspec 'localhost/CoC_4.sol/CoC_4.yml' did not match any files
$ git add localhost/CoC_4.sol/CoC_4.yml
$ git rm --cache localhost/CoC_5.sol/CoC_5.sol
fatal: pathspec 'localhost/CoC_5.sol/CoC_5.sol' did not match any files
$ git add localhost/CoC_5.sol/CoC_5.sol
$ git rm --cache localhost/CoC_5.sol/CoC_5.yml
fatal: pathspec 'localhost/CoC_5.sol/CoC_5.yml' did not match any files
$ git add localhost/CoC_5.sol/CoC_5.yml
$ git rm --cache localhost/CoC_6.sol/CoC_6.sol
fatal: pathspec 'localhost/CoC_6.sol/CoC_6.sol' did not match any files
$ git add localhost/CoC_6.sol/CoC_6.sol
$ git rm --cache localhost/CoC_6.sol/CoC_6.yml
fatal: pathspec 'localhost/CoC_6.sol/CoC_6.yml' did not match any files
$ git add localhost/CoC_6.sol/CoC_6.yml
$ git rm --cache localhost/CoC_7.sol/CoC_7.sol
fatal: pathspec 'localhost/CoC_7.sol/CoC_7.sol' did not match any files
$ git add localhost/CoC_7.sol/CoC_7.sol
$ git rm --cache localhost/CoC_7.sol/CoC_7.yml
fatal: pathspec 'localhost/CoC_7.sol/CoC_7.yml' did not match any files
$ git add localhost/CoC_7.sol/CoC_7.yml
$ git rm --cache localhost/CoC_9.sol/CoC_9.sol
fatal: pathspec 'localhost/CoC_9.sol/CoC_9.sol' did not match any files
$ git add localhost/CoC_9.sol/CoC_9.sol
$ git rm --cache localhost/CoC_9.sol/CoC_9.yml
fatal: pathspec 'localhost/CoC_9.sol/CoC_9.yml' did not match any files
$ git add localhost/CoC_9.sol/CoC_9.yml
$ git rm --cache localhost/CoC_Main.sol/CoC_Main.yml
fatal: pathspec 'localhost/CoC_Main.sol/CoC_Main.yml' did not match any files
$ git add localhost/CoC_Main.sol/CoC_Main.yml
$ git commit -a -m Monday, June 15, 2020 05:54:43 PM
[master (root-commit) 2885509] Monday, June 15, 2020 05:54:43 PM
 27 files changed, 11373 insertions(+)
 create mode 100644 .gitattributes
 create mode 100644 .gitignore
 create mode 100644 localhost/#CoC/EndlessJourney/CoC_10.sol/CoC_10.sol
 create mode 100644 localhost/#CoC/EndlessJourney/CoC_10.sol/CoC_10.yml
 create mode 100644 localhost/#CoC/EndlessJourney/LastSaved.sol/LastSaved.sol
 create mode 100644 localhost/#CoC/EndlessJourney/LastSaved.sol/LastSaved.yml
 create mode 100644 localhost/CoC_1.sol/CoC_1.sol
 create mode 100644 localhost/CoC_1.sol/CoC_1.yml
 create mode 100644 localhost/CoC_10.sol/CoC_10.sol
 create mode 100644 localhost/CoC_10.sol/CoC_10.yml
 create mode 100644 localhost/CoC_11.sol/CoC_11.sol
 create mode 100644 localhost/CoC_11.sol/CoC_11.yml
 create mode 100644 localhost/CoC_2.sol/CoC_2.sol
 create mode 100644 localhost/CoC_2.sol/CoC_2.yml
 create mode 100644 localhost/CoC_3.sol/CoC_3.sol
 create mode 100644 localhost/CoC_3.sol/CoC_3.yml
 create mode 100644 localhost/CoC_4.sol/CoC_4.sol
 create mode 100644 localhost/CoC_4.sol/CoC_4.yml
 create mode 100644 localhost/CoC_5.sol/CoC_5.sol
 create mode 100644 localhost/CoC_5.sol/CoC_5.yml
 create mode 100644 localhost/CoC_6.sol/CoC_6.sol
 create mode 100644 localhost/CoC_6.sol/CoC_6.yml
 create mode 100644 localhost/CoC_7.sol/CoC_7.sol
 create mode 100644 localhost/CoC_7.sol/CoC_7.yml
 create mode 100644 localhost/CoC_9.sol/CoC_9.sol
 create mode 100644 localhost/CoC_9.sol/CoC_9.yml
 create mode 100644 localhost/CoC_Main.sol/CoC_Main.yml
```
