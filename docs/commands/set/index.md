# Set Command

This command permits setting certain specifics of the game state.

## Help Listing
```shell
$ coctweak set --help
```
```
usage: coctweak set [-h] host id {notes,stat,gems} ...

positional arguments:
  host               Flash hostname the save will be stored in. Use
                     @localWithNet instead of #localWithNet, if you're on
                     Linux.
  id                 ID of the save.
  {notes,stat,gems}
    notes            Set the notes for a save.
    stat             Set the value for a core stat.
    gems             Set how many gems (currency) you have in your bag.

optional arguments:
  -h, --help         show this help message and exit
```


## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- notes --></inline-elem>[coctweak set &lt;host&gt; &lt;id&gt; notes ...](docs/commands/set/notes.md)
* <inline-elem><!-- stats --></inline-elem>[coctweak set &lt;host&gt; &lt;id&gt; stat ...](docs/commands/set/stat.md)