# Set > Gems Command

This command lets you change how many gems you have.

## Command Syntax
```shell
$ coctweak set localhost 5 gems --help
```
```
usage: coctweak set host id gems [-h] new_value

positional arguments:
  new_value   New value.

optional arguments:
  -h, --help  show this help message and exit
```


## Example
```shell
# Set player gems to 5000.
$ coctweak set localhost 5 gems 5000
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Gems : 0 -> 5000
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
```
