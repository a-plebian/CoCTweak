# Set > Stat Command

This permits setting your character's stats directly.  Which stats are available depends upon your mod.

## Command Syntax
```shell
$ coctweak set localhost 5 stat --help
```
```
usage: coctweak set host id stat [-h] statID new_value

positional arguments:
  statID      Stat ID
  new_value   New value.

optional arguments:
  -h, --help  show this help message and exit
```


## Example
```shell
# Set player's STR to 100
$ coctweak set localhost 5 stat STR 100
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Stat Strength (str): 17.0 -> 100.0
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
```
