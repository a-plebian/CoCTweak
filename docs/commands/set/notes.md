# Set > Notes Command

You can set your save's notes with this command. These notes will appear on the
save listings.

## Command Syntax
```shell
$ coctweak set localhost 5 notes --help
```
```
usage: coctweak set host id notes [-h] newnotes

positional arguments:
  newnotes    New notes for the save.

optional arguments:
  -h, --help  show this help message and exit
```


## Example
```shell
# Set save notes to OHGODWHY.
$ coctweak set localhost 5 notes OHGODWHY
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Notes: 'NEW SAVE' -> 'OHGODWHY'
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
```
