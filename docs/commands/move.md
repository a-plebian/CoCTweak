# Move Command

The move command allows you to move a save to another slot, including on another hostname.

CoCTweak must be able to load the SOL file successfully, or the move will fail.  The reason for a logical
load/save operation rather than a simple OS file move is that flash will refuse to read the SOL unless the SOL file
content has the proper metadata.

## Command Syntax

```
usage: coctweak move [-h] [--force] src_host src_id dest_host dest_id

positional arguments:
  src_host    Hostname the save is stored in
  src_id      ID of the original save.
  dest_host   Hostname the new save will be stored in
  dest_id     ID slot to rename it to. Must not exist.

optional arguments:
  -h, --help  show this help message and exit
  --force     If dest_id exists, overwrite it.
```

## Example

```shell
$ ./coctweak.sh move localhost 10 localhost 11
```

```
CoCTweak v0.0.1
___________________________________________________________________

(c)2019 Anonymous-BCFED. Available under the MIT Open Source License.

Moving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_10.sol to ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_11.sol...
  OK!
```
