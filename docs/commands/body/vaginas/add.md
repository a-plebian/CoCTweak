# Body > Vaginas > Add Command

This subcommand adds the desired number of vaginas (1 by default) to the player's character.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax
```
```shell
$ coctweak body localhost 5 vaginas add --help
```
```
usage: coctweak body host id vaginas add [-h] [--type TYPE]
                                         [--clit-length CLIT_LENGTH]
                                         [--fullness FULLNESS]
                                         [--looseness LOOSENESS]
                                         [--wetness WETNESS]
                                         [--recovery-progress RECOVERY_PROGRESS]
                                         [--clit-pierced] [--clit-not-pierced]
                                         [--labia-pierced]
                                         [--labia-not-pierced] [--virgin]
                                         [--not-virgin]
                                         count

positional arguments:
  count                 How many to add.

optional arguments:
  -h, --help            show this help message and exit
  --type TYPE, -t TYPE  vagina type to use. See your mod's vaginaType
                        enumeration for possible values. Will otherwise use
                        the last vagina in the collection as a template.
  --clit-length CLIT_LENGTH, -l CLIT_LENGTH
                        The length of the clit. Will otherwise use the last
                        vagina in the collection as a template.
  --fullness FULLNESS, -f FULLNESS
                        The fullness of the vagina. Will otherwise use the
                        last vagina in the collection as a template.
  --looseness LOOSENESS, -L LOOSENESS
                        The looseness of the vagina. See the appropriate
                        VagLooseness enum for your mod.
  --wetness WETNESS, -W WETNESS
                        The wetness of the vagina. See the appropriate
                        VagWetness enum for your mod.
  --recovery-progress RECOVERY_PROGRESS, -r RECOVERY_PROGRESS
                        The wetness of the vagina. See the appropriate
                        VagWetness enum for your mod.
  --clit-pierced        Sets clit pierced to TRUE
  --clit-not-pierced    Sets clit pierced to FALSE
  --labia-pierced       Sets labia pierced to TRUE
  --labia-not-pierced   Sets labia pierced to FALSE
  --virgin              Sets virgin to TRUE
  --not-virgin          Sets virgin to FALSE
```

```

## Example
```
```shell
$ coctweak body localhost 5 vaginas add 1 --type=HUMAN --clit-length=0.5 -f=0
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Added:
  TIGHT, DRY vagina
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
```

```
