# Body > Vaginas > Remove Command

This subcommand removes the desired vagina from the player's character.

Note that this *dumb* subcommand will not recalculate perks or deactivate features like the game would in the same situation.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax
```
usage: coctweak body host id vaginas remove [-h] vagina_id

positional arguments:
  vagina_id   Which vagina to remove (0-n).

optional arguments:
  -h, --help  show this help message and exit

```

## Example

```shell
$ ./coctweak.sh body localhost 7 vaginas remove 1
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_7.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Removing:
  TIGHT, NORMAL, virgin vagina
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_7.sol...

```
