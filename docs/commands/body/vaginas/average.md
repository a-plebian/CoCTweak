# Body > Vaginas > Average Command

This weird little command allows you to unify the size and shape of your clits without ten hours in Lumi's workshop.

## Subcommand Syntax
```
```shell
$ coctweak body localhost 7 vaginas average --help
```
```
usage: coctweak body host id vaginas average [-h]

optional arguments:
  -h, --help  show this help message and exit
```

```

## Example

```
```shell
$ coctweak body localhost 7 vaginas average
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_7.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Before:
  Vaginas:
    TIGHT, NORMAL, virgin vagina
After:
  Vaginas:
    TIGHT, NORMAL, virgin vagina
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_7.sol...
```

```
