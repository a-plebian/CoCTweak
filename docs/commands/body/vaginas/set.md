# Body > Vaginas > Set Command

The set subcommand allows you to finely manipulate your character's vaginas on a variable-by-variable basis.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax
```
usage: coctweak body host id vaginas set [-h] [--type TYPE]
                                         [--clit-length CLIT_LENGTH]
                                         [--fullness FULLNESS]
                                         [--looseness LOOSENESS]
                                         [--wetness WETNESS]
                                         [--recovery-progress RECOVERY_PROGRESS]
                                         [--clit-pierced] [--clit-not-pierced]
                                         [--labia-pierced]
                                         [--labia-not-pierced] [--virgin]
                                         [--not-virgin]
                                         vagina_id

positional arguments:
  vagina_id             Which vagina to modify.

optional arguments:
  -h, --help            show this help message and exit
  --type TYPE, -t TYPE  vagina type to use. See your mod's vaginaType
                        enumeration for possible values
  --clit-length CLIT_LENGTH, -l CLIT_LENGTH
                        The length of the clit. Will otherwise use the last
                        vagina in the collection as a template.
  --fullness FULLNESS, -f FULLNESS
                        The fullness of the vagina. Will otherwise use the
                        last vagina in the collection as a template.
  --looseness LOOSENESS, -L LOOSENESS
                        The looseness of the vagina. See the appropriate
                        VagLooseness enum for your mod.
  --wetness WETNESS, -W WETNESS
                        The wetness of the vagina. See the appropriate
                        VagWetness enum for your mod.
  --recovery-progress RECOVERY_PROGRESS, -r RECOVERY_PROGRESS
                        The wetness of the vagina. See the appropriate
                        VagWetness enum for your mod.
  --clit-pierced        Sets clit pierced to TRUE
  --clit-not-pierced    Sets clit pierced to FALSE
  --labia-pierced       Sets labia pierced to TRUE
  --labia-not-pierced   Sets labia pierced to FALSE
  --virgin              Sets virgin to TRUE
  --not-virgin          Sets virgin to FALSE

```

## Example

```shell
$ ./coctweak.sh body localhost 7 vaginas set 0 --type=HUMAN --clit-length=5.5 -f=1
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_7.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Before:
  TIGHT, NORMAL, virgin vagina
After:
  TIGHT, NORMAL, virgin vagina, with fullness=1.0
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_7.sol...

```
