# Body Commands

CoCTweak allows you to make a wide array of changes to your body with these subcommands.

**This subcommand is still under *very* active development.**

## Syntax

```
usage: coctweak body [-h]
                     host id
                     {cocks,cock,penis,penises,balls,vaginas,vagina,vag,vags}
                     ...

positional arguments:
  host                  Flash hostname the save will be stored in. Use
                        @localWithNet instead of #localWithNet, if you're on
                        Linux.
  id                    ID of the save.
  {cocks,cock,penis,penises,balls,vaginas,vagina,vag,vags}
    cocks (cock, penis, penises)
                        Operations to perform on your cocks.
    balls               Operations to perform on your balls.
    vaginas (vagina, vag, vags)
                        Operations to perform on your vaginas.

optional arguments:
  -h, --help            show this help message and exit

```

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- balls --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; balls ...](docs/commands/body/balls/index.md)
* <inline-elem><!-- cocks --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; (cock|cocks|penis|penises) ...](docs/commands/body/cocks/index.md)
* <inline-elem><!-- vaginas --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; (vaginas|vagina|vag|vags) ...](docs/commands/body/vaginas/index.md)