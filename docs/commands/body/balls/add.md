# Body > Balls > Add Command

This subcommand adds the desired number of testicles (1 by default) to the player's character.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax

```
usage: coctweak body host id balls add [-h] count

positional arguments:
  count       How many to add.

optional arguments:
  -h, --help  show this help message and exit
```

## Example

```shell
$ ./coctweak.sh body localhost 1 balls add 2
```

```
CoCTweak v0.0.1
___________________________________________________________________

(c)2019 Anonymous-BCFED. Available under the MIT Open Source License.

Loading ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_Main.sol...
You now have 6, each 1'6.5" (47cm) in diameter, with a multiplier of 6.4.
WARNING: Having more than 4 balls in this game may cause bugs, since it's impossible to get this many "naturally".
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_1.sol...
```
