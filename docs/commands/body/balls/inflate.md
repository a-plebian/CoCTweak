# Body > Balls > Inflate Command

This subcommand grows all of your balls by n inches in diameter.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax

```
usage: coctweak body host id balls inflate [-h] inches

positional arguments:
  inches      How many inches in diameter

optional arguments:
  -h, --help  show this help message and exit
```

## Example

```shell
$ ./coctweak.sh body localhost 1 balls inflate 2
```

```
CoCTweak v0.0.1
___________________________________________________________________

(c)2019 Anonymous-BCFED. Available under the MIT Open Source License.

Loading ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_Main.sol...
You now have 4, each 1'8.5" (52.1cm) in diameter, with a multiplier of 6.4.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_1.sol...
```
