# Body > Balls > Set Multiplier Command

This subcommand sets the desired cum multiplier on the player's character.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax

```
usage: coctweak body host id balls set-mult [-h] multiplier

positional arguments:
  multiplier  Cum multiplier

optional arguments:
  -h, --help  show this help message and exit
```

## Example

```shell
$ ./coctweak.sh body localhost 1 balls set-mult 2
```

```
CoCTweak v0.0.1
___________________________________________________________________

(c)2019 Anonymous-BCFED. Available under the MIT Open Source License.

Loading ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_Main.sol...
You now have 4, each 1'6.5" (47cm) in diameter, with a multiplier of 2.0.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_1.sol...
```
