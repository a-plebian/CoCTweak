# Body > Balls Commands

This subcommand permits modification of the player's testicles.

Be aware, however, that testicles are not very granular, and are represented by merely 3 numbers, so only limited modifications are currently available. More may become available as features are reverse-engineered.

**WARNING: This subcommand is still under active development.**

## Save Variables

### Common
For most mods (HGG, EJ, UEE) and Vanilla, the following variables represent the player's testes:

<table>
<thead>
<caption>Save Variables</caption>
<th>Variable</th>
<th>Type</th>
<th>Default (male)</th>
<th>Default (female)</th>
<th>Notes</th>
</thead>
<tbody>
<tr><th><code>balls</code></th><td>int</td><td>2</td><td>0</td><td>Number of testes.</td></tr>
<tr><th><code>ballSize</code></th><td>float</td><td>1</td><td>0</td><td>Diameter of a testicle in inches.</td></tr>
<tr><th><code>cumMultiplier</code></th><td>float</td><td>1</td><td>1</td><td>Used in semen volume calculations (see <code>Creature.cumQ()</code>).</td></tr>
</tbody>
</table>

## Command Syntax
```
usage: coctweak body host id balls [-h]
                                   {add,remove,rm,set-count,set-size,set-mult,inflate,grow,deflate,shrink}
                                   ...

positional arguments:
  {add,remove,rm,set-count,set-size,set-mult,inflate,grow,deflate,shrink}
    add                 Add a testicle or two
    remove (rm)         Remove testicles
    set-count           Set how many balls you have.
    set-size            Set how big your balls are.
    set-mult            Set how virile you are.
    inflate (grow)      Increase the size of your balls
    deflate (shrink)    Decrease the size of your balls

optional arguments:
  -h, --help            show this help message and exit
```

## Subcommand Index
<!-- Used for sorting. -->
* <inline-elem><!-- add --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; balls add &lsqb;*1-n*&rsqb;](docs/commands/body/balls/add.md)
* <inline-elem><!-- deflate --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; balls (deflate|shrink) *0-n*](docs/commands/body/balls/deflate.md)
* <inline-elem><!-- inflate --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; balls (inflate|grow) *0-n*](docs/commands/body/balls/inflate.md)
* <inline-elem><!-- remove --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; balls (remove|rm) &lsqb;*1-n*&rsqb;](docs/commands/body/balls/remove.md)
* <inline-elem><!-- set-count --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; balls set-count *0-n*](docs/commands/body/balls/set-count.md)
* <inline-elem><!-- set-mult --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; balls set-mult *0-n*](docs/commands/body/balls/set-mult.md)
* <inline-elem><!-- set-size --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; balls set-size *1-n*](docs/commands/body/balls/set-size.md)
