# Body > Balls > Remove Command

This subcommand removes the desired number of testicles (1 by default) from the player's character.

Note that this *dumb* subcommand will not recalculate perks or deactivate features like the game would in the same situation.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax

```
usage: coctweak body host id balls remove [-h] count

positional arguments:
  count       How many balls to remove (1-n).

optional arguments:
  -h, --help  show this help message and exit
```

## Example

```shell
$ ./coctweak.sh body localhost 1 balls remove 2
```

```
CoCTweak v0.0.1
___________________________________________________________________

(c)2019 Anonymous-BCFED. Available under the MIT Open Source License.

Loading ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_Main.sol...
You now have 4, each 1'6.5" (47cm) in diameter, with a multiplier of 6.4.
Saving ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_1.sol...
```
