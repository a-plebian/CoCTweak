# Body > Cocks > Set Command

The set subcommand allows you to finely manipulate your character's penises on a variable-by-variable basis.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax
```
usage: coctweak body host id cocks set [-h] [--type TYPE] [--length LENGTH]
                                       [--width WIDTH]
                                       [--knot-multiplier KNOT_MULTIPLIER]
                                       [--sock SOCK] [--pierced]
                                       [--not-pierced]
                                       cock_id

positional arguments:
  cock_id               Which cock to modify.

optional arguments:
  -h, --help            show this help message and exit
  --type TYPE, -t TYPE  Cock type to use. See your mod's CockType enumeration
                        for possible values
  --length LENGTH, -l LENGTH
                        The length of the cock in inches.
  --width WIDTH, -w WIDTH
                        The width of the cock.
  --knot-multiplier KNOT_MULTIPLIER, -k KNOT_MULTIPLIER
                        The knot multiplier (W*Km in most mods).
  --sock SOCK, -s SOCK  Sock ID (none or null for no sock)
  --pierced             Sets pierced to TRUE
  --not-pierced         Sets pierced to FALSE

```

## Example

```shell
$ ./coctweak.sh body localhost 1 cocks set 0 --type=HUMAN --length=5.5 --width=1 -k=0
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Before:
  1'4.5" (42cm)L x 3.5" (8.8cm)W (57in² area) HORSE cock
After:
  5.5" (14cm)L x 1" (2.5cm)W (6in² area) HUMAN cock
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)

```
