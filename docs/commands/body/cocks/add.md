# Body > Cocks > Add Command

This subcommand adds the desired number of cocks (1 by default) to the player's character.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax
```
usage: coctweak body host id cocks add [-h] [--type TYPE] [--length LENGTH]
                                       [--width WIDTH]
                                       [--knot-multiplier KNOT_MULTIPLIER]
                                       [--sock SOCK] [--pierced]
                                       [--not-pierced]
                                       count

positional arguments:
  count                 How many to add.

optional arguments:
  -h, --help            show this help message and exit
  --type TYPE, -t TYPE  Cock type to use. See your mod's CockType enumeration
                        for possible values. Will otherwise use the last cock
                        in the collection as a template.
  --length LENGTH, -l LENGTH
                        The length of the cock. Will otherwise use the last
                        cock in the collection as a template.
  --width WIDTH, -w WIDTH
                        The width of the cock. Will otherwise use the last
                        cock in the collection as a template.
  --knot-multiplier KNOT_MULTIPLIER, -k KNOT_MULTIPLIER
                        The knot multiplier (W*Km in most mods).
  --sock SOCK, -s SOCK  Sock ID (none or null for no sock)
  --pierced             Sets pierced to TRUE
  --not-pierced         Sets pierced to FALSE

```

## Example

```shell
$ ./coctweak.sh body localhost 5 cocks add 1 --type=HUMAN --length=5.5 --width=1 -k=0
```

```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Added:
  5.5" (14cm)L x 1" (2.5cm)W (6in² area) HUMAN cock
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...

```
