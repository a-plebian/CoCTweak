# Body > Cocks > Remove Command

This subcommand removes the desired cock from the player's character.

Note that this *dumb* subcommand will not recalculate perks or deactivate features like the game would in the same situation.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax
```
usage: coctweak body host id cocks remove [-h] cock_id

positional arguments:
  cock_id     Which cock to remove (0-n).

optional arguments:
  -h, --help  show this help message and exit

```

## Example

```shell
$ ./coctweak.sh body localhost 1 cocks remove 1
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Removing:
  11" (27.9cm)L x 2" (5cm)W (22in² area) DOG cock, with a knot 2" (5cm) wide (KM=1)
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)

```
