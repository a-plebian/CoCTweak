# Body > Cocks > Average Command

This weird little command allows you to unify the size and shape of your penises without ten hours in Lumi's workshop.

**Note:** The code currently doesn't average out anything other than length or width.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax
```
usage: coctweak body host id cocks average [-h] [--not-length] [--not-width]

optional arguments:
  -h, --help    show this help message and exit
  --not-length  Do not average length.
  --not-width   Do not average width.

```

## Example

```shell
$ ./coctweak.sh body localhost 1 cocks average
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Before:
  Cocks:
    1'4.5" (42cm)L x 3.5" (8.8cm)W (57in² area) HORSE cock
    11" (27.9cm)L x 2" (5cm)W (22in² area) DOG cock, with a knot 2" (5cm) wide (KM=1)
After:
  Cocks:
    1'1.8" (35cm)L x 2.7" (6.9cm)W (37in² area) HORSE cock
    1'1.8" (35cm)L x 2.7" (6.9cm)W (37in² area) DOG cock, with a knot 2.7" (6.9cm) wide (KM=1)
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)

```
