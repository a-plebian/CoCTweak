# Copy Command

The copy command allows you to duplicate a save to another slot, including on another hostname.

CoCTweak must be able to load the SOL file successfully, or the copy will fail.  The reason for a logical
load/save operation rather than a simple OS file copy is that flash will refuse to read the SOL unless the SOL file
content has the proper metadata.

## Command Syntax

```
usage: coctweak copy [-h] [--force] src_host src_id dest_host dest_id

positional arguments:
  src_host    Hostname the save is stored in
  src_id      ID of the original save.
  dest_host   Hostname the new save will be stored in
  dest_id     ID slot to rename it to. Must not exist.

optional arguments:
  -h, --help  show this help message and exit
  --force     If dest_id exists, overwrite it.
```

## Example

```shell
$ ./coctweak.sh cp localhost 1 8ch.net 5
```

```
CoCTweak v0.0.1
___________________________________________________________________

(c)2019 Anonymous-BCFED. Available under the MIT Open Source License.

Copying ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_1.sol to ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/8ch.net/CoC_1.sol...
  OK!
```
