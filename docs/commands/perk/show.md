# Perk > Show Command

Shows a perk, plus any known interpretations of its values.

## Command Syntax
```shell
# 
$ coctweak perk localhost 1 show --help
```
```
usage: coctweak perk host id show [-h] perk_id

positional arguments:
  perk_id     ID of the perk. Use quotes if it has a space!

optional arguments:
  -h, --help  show this help message and exit
```



## Example
```shell
# Show information about the Mage perk.
$ coctweak perk localhost 1 show Mage
```
```
Mage
  Increases base spell strength by 50%.
Flags:
Values:
  value1 (Value 1): 0
  value2 (Value 2): 0
  value3 (Value 3): 0
  value4 (Value 4): 0
Bonus Stats:
  Spell Mod: {'callable': False, 'key': 'Mage', 'multiply': False, 'value': 50, 'visible': True}
```
