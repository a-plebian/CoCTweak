# Perks > Add Command

Add a perk with this command.

**Note:** This command will not set flags or adjust stats like the game would.

## Command Syntax
```shell
$ coctweak perk localhost 5 add --help
```
```
usage: coctweak perk host id add [-h] perk_id value1 value2 value3 value4

positional arguments:
  perk_id     perk ID. See the listing of available perks for your mod.
  value1      Value 1, used as data storage in some perks.
  value2      Value 2, used as data storage in some perks.
  value3      Value 3, used as data storage in some perks.
  value4      Value 4, used as data storage in some perks.

optional arguments:
  -h, --help  show this help message and exit
```



## Example
```shell
# List all held perks.
$ coctweak perk localhost 5 ls
```
```
History: Scholar
Smart
```

```shell
# Add Strong Back 2: Strong Harder
$ coctweak perk localhost 5 add "Strong Back 2: Strong Harder" 0 0 0 0
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
```

```shell
# List perks again.
$ coctweak perk localhost 5 ls
```
```
History: Scholar
Smart
Strong Back 2: Strong Harder
```


