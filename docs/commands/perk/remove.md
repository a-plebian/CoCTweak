# Perk > Show Command

Shows a perk, plus any known interpretations of its values.

## Command Syntax
```shell
$ coctweak perk localhost 1 remove --help
```
```
usage: coctweak perk host id remove [-h] perk_id

positional arguments:
  perk_id     Perk ID. See the listing of available perks for your mod.

optional arguments:
  -h, --help  show this help message and exit
```



## Example
```shell
# Remove the Mage perk.
$ coctweak perk localhost 1 remove Mage
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Removing Mage...
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
```


