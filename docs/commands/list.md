# List Command

The list command permits you to make a listing of all available Flash SharedObject library hostnames that contain CoC saves, and listings of the saves themselves.

## Command Syntax

```
usage: coctweak list [-h] [--format {coc,long,json,yaml}] [host]

positional arguments:
  host                  Flash hostname the save will be stored in. Use
                        @localWithNet instead of #localWithNet, if you're on
                        Linux.

optional arguments:
  -h, --help            show this help message and exit
  --format {coc,long,json,yaml}
                        Format of output.

```

## Example

### Long format

```shell
$ coctweak ls localhost --format=long
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

localhost     1: Hank (No Notes Available.) - Lvl 30.0 Male, 20632G, 290d on Easy (HGG vhgg 1.4.11)
localhost     2: Hank (MOUNTAIN) - Lvl 4.0 Male, 21G, 23d on Easy (HGG vNone)
localhost     3: Hank (MOUNTAIN) - Lvl 4.0 Male, 86G, 28d on Easy (HGG vNone)
localhost     4: Hank (MOUNTAIN) - Lvl 4.0 Male, 262G, 13d on Easy (HGG vNone)
localhost     5: Hank (NEW SAVE) - Lvl 1.0 Male, 0G, 0d on Normal (HGG vNone)
localhost     6: Hank (No Notes Available.) - Lvl 30.0 Male, 19456G, 277d on Easy (HGG vNone)
localhost     7: Hilda (No Notes Available.) - Lvl 1.0 Female, 0G, 0d on Normal (HGG vhgg 1.4.13)
localhost     9: Bill (ORIGINAL COC) - Lvl 1.0 Male, 0G, 0d on N/A (Vanilla v1.0.2)
WARNING: UEECock: Object using legacy version stamp!
localhost     10: Dale (Revamp) - Lvl 1.0 Male, 0G, 0d on Normal (UEE v1.0.2_mod_1.4.14)
localhost     11: Dale (UEE) - Lvl 1.0 Male, 0G, 0d on Normal (UEE v1.0.2_mod_1.4.17c)
localhost/ej  10: Gary () - Lvl 1.0 Male, 0G, 0d on Normal (EJ vEndless Journey)
```


### CoC Format

```shell
$ coctweak ls localhost --format=coc
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

localhost     1: Hank - No Notes Available.
  Days - 290 | Gender - M | Difficulty - Easy | Mod - HGG vhgg 1.4.11
localhost     2: Hank - MOUNTAIN
  Days - 23 | Gender - M | Difficulty - Easy | Mod - HGG vNone
localhost     3: Hank - MOUNTAIN
  Days - 28 | Gender - M | Difficulty - Easy | Mod - HGG vNone
localhost     4: Hank - MOUNTAIN
  Days - 13 | Gender - M | Difficulty - Easy | Mod - HGG vNone
localhost     5: Hank - NEW SAVE
  Days - 0 | Gender - M | Difficulty - Normal | Mod - HGG vNone
localhost     6: Hank - No Notes Available.
  Days - 277 | Gender - M | Difficulty - Easy | Mod - HGG vNone
localhost     7: Hilda - No Notes Available.
  Days - 0 | Gender - F | Difficulty - Normal | Mod - HGG vhgg 1.4.13
localhost     9: Bill - ORIGINAL COC
  Days - 0 | Gender - M | Difficulty - N/A | Mod - Vanilla v1.0.2
WARNING: UEECock: Object using legacy version stamp!
localhost     10: Dale - Revamp
  Days - 0 | Gender - M | Difficulty - Normal | Mod - UEE v1.0.2_mod_1.4.14
localhost     11: Dale - UEE
  Days - 0 | Gender - M | Difficulty - Normal | Mod - UEE v1.0.2_mod_1.4.17c
localhost/ej  10: Gary - 
  Days - 0 | Gender - M | Difficulty - Normal | Mod - EJ vEndless Journey
```


### JSON Format

```shell
$ coctweak ls localhost --format=json
```
```
WARNING: UEECock: Object using legacy version stamp!
[{"namespace":"","slot":1,"short_name":"Hank","notes":"No Notes Available.","level":30.0,"gems":20632,"days":290,"difficulty":"Easy","mod":{"name":"HGG","version":"hgg 1.4.11"}},{"namespace":"","slot":2,"short_name":"Hank","notes":"MOUNTAIN","level":4.0,"gems":21,"days":23,"difficulty":"Easy","mod":{"name":"HGG","version":null}},{"namespace":"","slot":3,"short_name":"Hank","notes":"MOUNTAIN","level":4.0,"gems":86,"days":28,"difficulty":"Easy","mod":{"name":"HGG","version":null}},{"namespace":"","slot":4,"short_name":"Hank","notes":"MOUNTAIN","level":4.0,"gems":262,"days":13,"difficulty":"Easy","mod":{"name":"HGG","version":null}},{"namespace":"","slot":5,"short_name":"Hank","notes":"NEW SAVE","level":1.0,"gems":0,"days":0,"difficulty":"Normal","mod":{"name":"HGG","version":null}},{"namespace":"","slot":6,"short_name":"Hank","notes":"No Notes Available.","level":30.0,"gems":19456,"days":277,"difficulty":"Easy","mod":{"name":"HGG","version":null}},{"namespace":"","slot":7,"short_name":"Hilda","notes":"No Notes Available.","level":1.0,"gems":0,"days":0,"difficulty":"Normal","mod":{"name":"HGG","version":"hgg 1.4.13"}},{"namespace":"","slot":9,"short_name":"Bill","notes":"ORIGINAL COC","level":1.0,"gems":0,"days":0,"difficulty":"N/A","mod":{"name":"Vanilla","version":"1.0.2"}},{"namespace":"","slot":10,"short_name":"Dale","notes":"Revamp","level":1.0,"gems":0,"days":0,"difficulty":"Normal","mod":{"name":"UEE","version":"1.0.2_mod_1.4.14"}},{"namespace":"","slot":11,"short_name":"Dale","notes":"UEE","level":1.0,"gems":0,"days":0,"difficulty":"Normal","mod":{"name":"UEE","version":"1.0.2_mod_1.4.17c"}},{"namespace":"ej","slot":10,"short_name":"Gary","notes":"","level":1.0,"gems":0,"days":0,"difficulty":"Normal","mod":{"name":"EJ","version":"Endless Journey"}}]
```


### YAML Format

```shell
$ coctweak ls localhost --format=yaml
```
```
WARNING: UEECock: Object using legacy version stamp!
[{days: 290, difficulty: Easy, gems: 20632, level: 30.0, mod: {name: HGG, version: hgg
        1.4.11}, namespace: '', notes: No Notes Available., short_name: Hank, slot: 1},
  {days: 23, difficulty: Easy, gems: 21, level: 4.0, mod: {name: HGG, version: null},
    namespace: '', notes: MOUNTAIN, short_name: Hank, slot: 2}, {days: 28, difficulty: Easy,
    gems: 86, level: 4.0, mod: {name: HGG, version: null}, namespace: '', notes: MOUNTAIN,
    short_name: Hank, slot: 3}, {days: 13, difficulty: Easy, gems: 262, level: 4.0,
    mod: {name: HGG, version: null}, namespace: '', notes: MOUNTAIN, short_name: Hank,
    slot: 4}, {days: 0, difficulty: Normal, gems: 0, level: 1.0, mod: {name: HGG,
      version: null}, namespace: '', notes: NEW SAVE, short_name: Hank, slot: 5},
  {days: 277, difficulty: Easy, gems: 19456, level: 30.0, mod: {name: HGG, version: null},
    namespace: '', notes: No Notes Available., short_name: Hank, slot: 6}, {days: 0,
    difficulty: Normal, gems: 0, level: 1.0, mod: {name: HGG, version: hgg 1.4.13},
    namespace: '', notes: No Notes Available., short_name: Hilda, slot: 7}, {days: 0,
    difficulty: N/A, gems: 0, level: 1.0, mod: {name: Vanilla, version: 1.0.2}, namespace: '',
    notes: ORIGINAL COC, short_name: Bill, slot: 9}, {days: 0, difficulty: Normal,
    gems: 0, level: 1.0, mod: {name: UEE, version: 1.0.2_mod_1.4.14}, namespace: '',
    notes: Revamp, short_name: Dale, slot: 10}, {days: 0, difficulty: Normal, gems: 0,
    level: 1.0, mod: {name: UEE, version: 1.0.2_mod_1.4.17c}, namespace: '', notes: UEE,
    short_name: Dale, slot: 11}, {days: 0, difficulty: Normal, gems: 0, level: 1.0,
    mod: {name: EJ, version: Endless Journey}, namespace: ej, notes: '', short_name: Gary,
    slot: 10}]

```
