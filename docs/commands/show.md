# Show Command

The show command allows you to display a large volume of data about the save, ranging from character stats to perks, inventory, and statuses.

**This command is under continuous, active development.**

* [Command Syntax](#command-syntax)
* [Example](#example)
* [Making Sense of it All](#making-sense-of-it-all)

## Command Syntax

```
usage: coctweak show [-h] host id

positional arguments:
  host        Flash hostname the save will be stored in. Use @localWithNet
              instead of #localWithNet, if you're on Linux.
  id          ID of the save.

optional arguments:
  -h, --help  show this help message and exit

```

## Example

```shell
$ coctweak show localhost 5
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
localhost@5 - ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol
Mod: HGG vnull (BUG)
CoC Save Version: 817
Mod Save Version: 15
0 days, 18 hours, 0 minutes
Player: Hank, Level 1 ADULT Male
Sexual orientation: Likes Females (0)
COR: 17, INTE: 20, LIB: 15, SENS: 15, SPE: 16, STR: 17, TOU: 17, ESTEEM: 50, FEMININITY: 34, FERTILITY: 5, OBEY: 10, WILL: 80
HP: 99, XP: 0, lust: 40, fatigue: 0, hunger: 80
Ass:
  VIRGIN, DRY, TIGHT (2) ass
Breasts:
  Row 1:
    2x w/ 1 nips per breast
      Rating: 0
Cocks:
  5.5" (14cm)L x 1" (2.5cm)W (6in² area) HUMAN cock
2x balls, each 1" (2.5cm) around, cum multiplier of 1
Perks:
  * History: Scholar = [0, 0, 0, 0]
  * Smart            = Bonus: 25%
Status Effects:
Key Items:
Storage:
  Player:
    *  1:     [Empty]    
    *  2:     [Empty]    
    *  3:     [Empty]    
    *  4:     [Empty]    
    *  5:     [Empty]    
    *  6:     [Empty]    
    *  7:     [Empty]    
    *  8:     [Empty]    
    *  9:     [Empty]    
    * 10:     [Empty]    
  Weapon Rack:
    *  1:     [Empty]    
    *  2:     [Empty]    
    *  3:     [Empty]    
    *  4:     [Empty]    
    *  5:     [Empty]    
    *  6:     [Empty]    
    *  7:     [Empty]    
    *  8:     [Empty]    
    *  9:     [Empty]    
  Armor Rack:
    *  1:     [Empty]    
    *  2:     [Empty]    
    *  3:     [Empty]    
    *  4:     [Empty]    
    *  5:     [Empty]    
    *  6:     [Empty]    
    *  7:     [Empty]    
    *  8:     [Empty]    
    *  9:     [Empty]    
  Jewelry Box:
    *  1:     [Empty]    
    *  2:     [Empty]    
    *  3:     [Empty]    
    *  4:     [Empty]    
    *  5:     [Empty]    
    *  6:     [Empty]    
    *  7:     [Empty]    
    *  8:     [Empty]    
    *  9:     [Empty]    
  Dresser:
    *  1:     [Empty]    
    *  2:     [Empty]    
    *  3:     [Empty]    
    *  4:     [Empty]    
    *  5:     [Empty]    
    *  6:     [Empty]    
    *  7:     [Empty]    
    *  8:     [Empty]    
    *  9:     [Empty]    
  Shield Rack:
    *  1:     [Empty]    
    *  2:     [Empty]    
    *  3:     [Empty]    
    *  4:     [Empty]    
    *  5:     [Empty]    
    *  6:     [Empty]    
    *  7:     [Empty]    
    *  8:     [Empty]    
    *  9:     [Empty]    
  Chests:
    *  1:     [Empty]    
    *  2:     [Empty]    
    *  3:     [Empty]    
    *  4:     [Empty]    
EditorData flag:
  Flag ID: 3499
  Not present in save.
```


## Making Sense of it All
* First, it's long.  Much of the reason behind this is that CoCTweak does not care about perks or statuses when printing inventory, so it assumes you have unlocked everything (except in HGG, where locked slots can be marked).
* Perks and status effects are usually listed with a quartet of numbers describing the values of the four holding variables assigned to each perk.  There are always four of these, so most of the 0s you see are just unused.  Some perks have decoders implemented that output  stuff that makes more sense.
* We use a crude, watered-down port of the calculations used to determine the maximum values of HP, Lust, and Fatigue, so they may be inaccurate.