# Export Command

Export can export a given save from your SharedObject library to YAML or JSON.

## Command Syntax

```
usage: coctweak export [-h] [--format {yaml,json}] host id filename

positional arguments:
  host                  Hostname the save is stored in
  id                    ID of the save, or Main for the Permanent Object file.
  filename              File to export to.

optional arguments:
  -h, --help            show this help message and exit
  --format {yaml,json}  File to export to.
```

## Example

```shell
$ ./coctweak.sh export localhost 1 ~/coctweak/save_1.yml
```

```
Loading ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/RVWKFGUX/localhost/CoC_Main.sol...
```
