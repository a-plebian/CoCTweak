# Changelog

## 0.0.4

### Compatibility

| Mod | Version |
| --- | --- |
| HGG | 1.6.3.1 |
| UEE | 1.0.2_mod_1.4.19 |

### Changes

* Added support for ass ratings (`buttRating` in the save file).
* `show` command is a little more readable.

### Bugs Slain

* Fixed EditorData using invalid function for deserializing integer data.
* Healing hunger in HGG no longer sets it to 0 (AKA dead).
* Fix crash caused by presence of CoC_Main-UEE file.
* Fix git crying about stuff not being in cache during backup.
* `inventory ... list` command works again.

## 0.0.3

### Compatibility

| Mod | Version |
| --- | --- |
| HGG | 1.6.3 |
| UEE | 1.0.2_mod_1.4.18b |

### Changes

* Added `get gems`
* Commands needing a hostname will accept `@localWithNet` or `@lwn` as `#localWithNet`.

### Bugs Slain

* Fixed outdated datafiles from HGG and UEE, fixed itemlibs as well.
* Each new file is scrubbed from backup individually prior to being added. Fixes a fatal.

## 0.0.2

First binary release, initial Windows support.

### Compatibility

| Mod | Version |
| --- | --- |
| HGG | 1.6.2 |
| UEE | 1.0.2_mod_1.4.18b |

### Changes

* Added `coctweak.cmd` to allow Windows users to run from source.
* Binary packages built for Windows and Linux, as well as a source tar.
* Shows version and date of build on `--help` screen.
* Buildsystem fixed to work on Windows.
* Buildsystem will now select latest available python 3.

### Bugs Slain
* Crashes in `body vagina` command tree fixed.
* Corrected documentation for `body vagina` command tree.
