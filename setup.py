from distutils.core import setup

README = ''
with open('README.md', 'r') as f:
    README = f.read()
setup(
    name='coctweak',
    version='0.0.4',
    packages=['coctweak',],
    license='MIT',
    long_description=README
)
