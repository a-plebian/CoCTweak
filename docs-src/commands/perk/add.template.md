{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
{%- set perkID = 'Strong Back 2: Strong Harder' -%}
# Perks > Add Command

Add a perk with this command.

**Note:** This command will not set flags or adjust stats like the game would.

## Command Syntax
{{ cmd([COCTWEAK, 'perk', host, slot, 'add', '--help'], echo=True) }}


## Example
{{ cmd([COCTWEAK, 'perk', host, slot, 'ls'], echo=True, comment="List all held perks.") }}
{{ cmd([COCTWEAK, 'perk', host, slot, 'add', perkID, '0', '0', '0', '0'], echo=True, comment="Add "+perkID) }}
{{ cmd([COCTWEAK, 'perk', host, slot, 'ls'], echo=True, comment="List perks again.") }}

{% do reset_slot(slot) %}
