# Backup Command

This command goes through your Flash SharedObject library and backs up your saves, both in binary form, and in a decoded YAML format.

Backup also commits these saves to a local git repository so you can track changes, or revert to earlier saves.

NOTE: This will save all files that start with CoC_ and end with .sol, including currently-unsupported Xianxia/EJ saves.

## Command Syntax

```
{{ cmd([COCTWEAK, 'backup', '--help']) }}
```

## Example

{{ cmd([COCTWEAK, 'backup'], echo=True) }}
