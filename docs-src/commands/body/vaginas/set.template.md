{%- set host = 'localhost' -%}
{%- set slot = '7' -%}
# Body > Vaginas > Set Command

The set subcommand allows you to finely manipulate your character's vaginas on a variable-by-variable basis.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax
```
{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'set', '--help']) }}
```

## Example

```shell
$ ./coctweak.sh body {{host}} {{slot}} vaginas set 0 --type=HUMAN --clit-length=5.5 -f=1
```
```
{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'set', '0', '--type=HUMAN', '--clit-length=5.5', '-f=1']) }}
```
{% do reset_slot(slot) %}
