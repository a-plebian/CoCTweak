{%- set host = 'localhost' -%}
{%- set slot = '7' -%}
# Body > Vaginas > Remove Command

This subcommand removes the desired vagina from the player's character.

Note that this *dumb* subcommand will not recalculate perks or deactivate features like the game would in the same situation.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax
```
{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'remove', '--help']) }}
```

## Example

```shell
$ ./coctweak.sh body {{host}} {{slot}} vaginas remove 1
```
```
{{ cmd([COCTWEAK, 'body', host, slot, 'vaginas', 'remove', '0']) }}
```
{% do reset_slot(slot) %}
