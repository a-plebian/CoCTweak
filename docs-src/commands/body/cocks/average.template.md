{%- set host = 'localhost' -%}
{%- set slot = 1 -%}
# Body > Cocks > Average Command

This weird little command allows you to unify the size and shape of your penises without ten hours in Lumi's workshop.

**Note:** The code currently doesn't average out anything other than length or width.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax
```
{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'average', '--help']) }}
```

## Example

```shell
$ ./coctweak.sh body localhost 1 cocks average
```
```
{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'average']) }}
```
{% do reset_slot(slot) %}
