{%- set host = 'localhost' -%}
{%- set slot = 5 -%}
# Body > Cocks > Add Command

This subcommand adds the desired number of cocks (1 by default) to the player's character.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax
```
{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'add', '--help']) }}
```

## Example

```shell
$ ./coctweak.sh body {{ host }} {{ slot }} cocks add 1 --type=HUMAN --length=5.5 --width=1 -k=0
```

```
{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'add', '1', '--type=HUMAN', '--length=5.5', '--width=1', '-k=0']) }}
```
{% do reset_slot(slot) %}
