{%- set host = 'localhost' -%}
{%- set slot = 1 -%}
# Body > Cocks > Set Command

The set subcommand allows you to finely manipulate your character's penises on a variable-by-variable basis.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax
```
{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'set', '--help']) }}
```

## Example

```shell
$ ./coctweak.sh body {{host}} {{slot}} cocks set 0 --type=HUMAN --length=5.5 --width=1 -k=0
```
```
{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'set', '0', '--type=HUMAN', '--length=5.5', '--width=1', '-k=0']) }}
```
{% do reset_slot(slot) %}
