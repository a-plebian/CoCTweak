{%- set host = 'localhost' -%}
{%- set slot = 1 -%}
# Body > Cocks > Remove Command

This subcommand removes the desired cock from the player's character.

Note that this *dumb* subcommand will not recalculate perks or deactivate features like the game would in the same situation.

**WARNING: This subcommand is still under active development.**

## Subcommand Syntax
```
{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'remove', '--help']) }}
```

## Example

```shell
$ ./coctweak.sh body {{host}} {{slot}} cocks remove 1
```
```
{{ cmd([COCTWEAK, 'body', host, slot, 'cocks', 'remove', '1']) }}
```
{% do reset_slot(slot) %}
