# Body Commands

CoCTweak allows you to make a wide array of changes to your body with these subcommands.

**This subcommand is still under *very* active development.**

## Syntax

```
{{ cmd([COCTWEAK, 'body', '--help']) }}
```

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- balls --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; balls ...](docs/commands/body/balls/index.md)
* <inline-elem><!-- cocks --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; (cock|cocks|penis|penises) ...](docs/commands/body/cocks/index.md)
* <inline-elem><!-- vaginas --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; (vaginas|vagina|vag|vags) ...](docs/commands/body/vaginas/index.md)
