{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
# Heal Command
a
The heal command repairs your HP, and optionally, your lust, fatigue, and hunger.

**NOTE:** The maximums you are healed to are _very_ rough guesses.

## Command Syntax

```
{{ cmd([COCTWEAK, 'heal', '--help']) }}
```

## Example

{{ cmd([COCTWEAK, 'heal', host, slot, '--all'], echo=True) }}

{% do reset_slot(slot) %}
