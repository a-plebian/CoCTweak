# Get > Stat Command

This permits getting your character's stats directly.  Which stats are available depends upon your mod.

## Command Syntax

{{ cmd([COCTWEAK, 'get', 'localhost', '1', 'stat', '-h'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'get', 'localhost', '1', 'stat', 'STR'], echo=True) }}
