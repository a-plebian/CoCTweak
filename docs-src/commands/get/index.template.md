# Get Command

This command permits getting certain specifics of the game state.

## Help Listing

{{ cmd([COCTWEAK, 'get', 'localhost', '1', '-h'], echo=True) }}

## Subcommand Index
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- notes --></inline-elem>[coctweak get &lt;host&gt; &lt;id&gt; notes ...](docs/commands/get/notes.md)
* <inline-elem><!-- stat --></inline-elem>[coctweak get &lt;host&gt; &lt;id&gt; stat ...](docs/commands/get/stat.md)
