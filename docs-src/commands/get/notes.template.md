# Get > Notes Command

You can get your save's notes with this command. These notes appear on the
save listings.

NOTE: To save people from having to make parsers, the header is automatically suppressed on `get` commands.

## Command Syntax

{{ cmd([COCTWEAK, 'get', 'localhost', '1', 'notes', '-h'], echo=True) }}

## Example

{{ cmd([COCTWEAK, 'get', 'localhost', '1', 'notes'], echo=True) }}
