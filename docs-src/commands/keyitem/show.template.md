{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
{%- set keyitem = 'Carpenter\'s Nail Box' -%}
# Key Items > Show Command

Shows a Key Item, plus any known interpretations of its values.

## Command Syntax
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'show', '--help'], echo=True) }}

## Example
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'show', keyitem], echo=True, comment='Show information about '+keyitem) }}
