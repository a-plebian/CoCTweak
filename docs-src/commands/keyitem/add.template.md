{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
{%- set keyitemID = 'Camp - Chest' -%}
# Key Items > Add Command

Add a keyitem with this command.

**Note:** This command will not set flags or adjust stats like the game would.

## Command Syntax
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'add', '--help'], echo=True)}}

## Example
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'ls'], echo=True, comment="List current keyitems") }}
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'add', keyitemID, '0', '0', '0', '0'], echo=True, comment="Add camp chest") }}
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'ls'], echo=True, comment="Check if we affected anything.") }}

{% do reset_slot(slot) %}
