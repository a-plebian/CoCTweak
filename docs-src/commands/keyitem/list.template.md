{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
# Key Items > List Command

Display a list of all active Key Items, plus value information.

## Command Syntax
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'ls', '--help'], echo=True) }}


## Example
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'ls'], echo=True, comment="Show all keyitems currently held") }}
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'ls', '--format=markdown'], echo=True, lang="markdown", comment="Same, but in Markdown") }}
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'ls', '--format=json'], echo=True, lang="json", comment="And in JSON") }}
{{ cmd([COCTWEAK, 'keyitem', host, slot, 'ls', '--format=yaml'], echo=True, lang="yaml", comment="And finally in YAML") }}
