# Show Command

The show command allows you to display a large volume of data about the save, ranging from character stats to perks, inventory, and statuses.

**This command is under continuous, active development.**

* [Command Syntax](#command-syntax)
* [Example](#example)
* [Making Sense of it All](#making-sense-of-it-all)

## Command Syntax

```
{{ cmd([COCTWEAK, 'show', '--help']) }}
```

## Example

{{ cmd([COCTWEAK, 'show', 'localhost', '5'], echo=True) }}

## Making Sense of it All
* First, it's long.  Much of the reason behind this is that CoCTweak does not care about perks or statuses when printing inventory, so it assumes you have unlocked everything (except in HGG, where locked slots can be marked).
* Perks and status effects are usually listed with a quartet of numbers describing the values of the four holding variables assigned to each perk.  There are always four of these, so most of the 0s you see are just unused.  Some perks have decoders implemented that output  stuff that makes more sense.
* We use a crude, watered-down port of the calculations used to determine the maximum values of HP, Lust, and Fatigue, so they may be inaccurate.
