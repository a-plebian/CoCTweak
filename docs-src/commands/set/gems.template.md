{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
# Set > Gems Command

This command lets you change how many gems you have.

## Command Syntax
{{ cmd([COCTWEAK, 'set', host, slot, 'gems', '--help'], echo=True) }}

## Example
{{ cmd([COCTWEAK, 'set', host, slot, 'gems', '5000'], echo=True, comment="Set player gems to 5000.") }}

{%- do reset_slot(slot) %}
