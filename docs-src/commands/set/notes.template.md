{%- set host = 'localhost' -%}
{%- set slot = '5' -%}
# Set > Notes Command

You can set your save's notes with this command. These notes will appear on the
save listings.

## Command Syntax
{{ cmd([COCTWEAK, 'set', host, slot, 'notes', '--help'], echo=True) }}

## Example
{{ cmd([COCTWEAK, 'set', host, slot, 'notes', 'OHGODWHY'], echo=True, comment="Set save notes to OHGODWHY.") }}

{%- do reset_slot(slot) %}
