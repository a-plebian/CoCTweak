{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
{%- set effectID = 'ButtStretched' -%}
# Status Effect > Remove Command

Remove a given status effect.

## Command Syntax
{{ cmd([COCTWEAK, 'statuseffect', host, slot, 'remove', '--help'], echo=True) }}


## Example
{{ cmd([COCTWEAK, 'statuseffect', host, slot, 'remove', 'ButtStretched'], echo=True, comment="Remove the "+effectID+" effect.") }}


{% do reset_slot(slot) %}
