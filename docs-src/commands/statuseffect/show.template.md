{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
{%- set effectID = 'ButtStretched' -%}
# Status Effect > Show Command

Shows a status effect, plus any known interpretations of its values.

## Command Syntax
{{ cmd([COCTWEAK, 'statuseffect', host, slot, 'show', '--help'], echo=True) }}


## Example
{{ cmd([COCTWEAK, 'statuseffect', host, slot, 'show', 'ButtStretched'], echo=True, comment="Show information about the "+effectID+" effect.") }}
