{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
# Status Effect > List Command

Display a list of all active status effects, plus value information.

## Command Syntax

{{ cmd([COCTWEAK, 'statuseffect', host, slot, 'ls', '--help'], echo=True) }}


## Example
{{ cmd([COCTWEAK, 'statuseffect', host, slot, 'ls'], echo=True, comment="List active status effects") }}
{{ cmd([COCTWEAK, 'statuseffect', host, slot, 'ls', '--format=markdown'], echo=True, lang='markdown', comment="Same, but in markdown") }}
{{ cmd([COCTWEAK, 'statuseffect', host, slot, 'ls', '--format=json'], echo=True, lang='json', comment="And in JSON") }}
{{ cmd([COCTWEAK, 'statuseffect', host, slot, 'ls', '--format=yaml'], echo=True, lang='yaml', comment="And finally in YAML") }}
