{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
# Inventory > Remove Command

This command will remove n items from your inventory of the given type.

## Command Syntax

```
{{ cmd([COCTWEAK, 'inventory', host, slot, 'remove', '--help']) }}
```

## Example

{{ cmd([COCTWEAK, 'inventory', host, slot, 'remove', 'player', 'AkbalSl', '1'], echo=True, comment='Remove a single Akbal Saliva from all slots in your player\'s inventory:') }}
{%- do reset_slot(slot) %}
