{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
# Inventory > Set Command

This powerful command permits advanced editing of your inventory on a slot-by-slot basis.

## Command Syntax

```
{{ cmd([COCTWEAK, 'inventory', host, slot, 'set', '--help']) }}
```

## Example

{{ cmd([COCTWEAK, 'inventory', host, slot, 'set', 'player', '1', '--item=BlackPp', '--count=10'], echo=True, comment='Set slot 1 in the player inventory to be 10 black peppers:') }}
{%- do reset_slot(slot) %}
