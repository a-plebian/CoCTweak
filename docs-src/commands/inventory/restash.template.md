{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
# Inventory > Clear Command

This command will remove all items from your camp storage (chests, racks) and then attempt to re-stash them.  The intent of this command
is to help with re-organizing your inventory after you find a rack or other filtered inventory, and allows you to quickly move stuff
from your chests to the appropriate racks.

## Command Syntax

```
{{ cmd([COCTWEAK, 'inventory', host, slot, 'restash', '--help']) }}
```

## Example

{{ cmd([COCTWEAK, 'inventory', host, slot, 'restash'], echo=True) }}

{%- do reset_slot(slot) %}
