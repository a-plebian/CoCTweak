{%- set host = 'localhost' -%}
{%- set slot = '1' -%}
# Inventory > Clear Command

This command will **irreversibly** remove all items in the indicated inventory.

Invoke `./coctweak.sh backup` before continuing, just in case!

## Command Syntax

```
{{ cmd([COCTWEAK, 'inventory', host, slot, 'clear', '--help']) }}
```

## Example

{{ cmd([COCTWEAK, 'inventory', host, slot, 'clear', 'player'], echo=True) }}

{%- do reset_slot(slot) %}
