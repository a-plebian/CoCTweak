# CoCTweak

A thorough, mod-friendly save editor with a multitude of console commands. Built with Python 3 and mini-amf, so it works on Linux and Windows, as well.

## A Note

As a save editor, CoCTweak is not supported by the developers of CoC.  By using CoCTweak, you can potentially break saves and trigger bugs.  **You use CoCTweak at your own risk.**

So, **don't bug CoC devs about bugs triggered by CoCTweak** in CoC.  They aren't responsible, and can't fix our problems anyway.

## Mod Support
Most development is done against the HGG mod of CoC for purely preferential reasons.

Saves are (mostly) parsed by automatically-generated code, so there (hopefully) won't be too many issues when upgrading.

**You are welcome to file issue reports and merge requests if you'd like to help out**

<table><thead><th>Mod</th><th>Author</th><th>Read</th><th>Write</th><th>Modding</th></thead>
<tbody><tr><th>[Vanilla](https://github.com/OXOIndustries/Corruption-of-Champions)</th><td>Fenoxo</td><td>Y</td><td>Y</td><td>?</td></tr><tr><th>[HGG](https://gitgud.io/BelshazzarII/CoCAnon_mod)</th><td>OCA/OtherCoCAnon</td><td>Y</td><td>Y</td><td>FULL</td></tr><tr><th>[Endless Journey](https://github.com/Oxdeception/Corruption-of-Champions)</th><td>Oxdeception</td><td>N</td><td>N</td><td>BROKEN</td></tr><tr><th>[UEE](https://github.com/Kitteh6660/Corruption-of-Champions-Mod)</th><td>Kitteh6660</td><td>Y</td><td>Y</td><td>WIP</td></tr></tbody></table>

## Installing

### Binaries

Pre-compiled, binary executables are available at the GitLab [Releases](https://gitlab.com/Anonymous-BCFED/CoCTweak/-/releases) page to make use less complicated for Windows.

Binaries are also available for Ubuntu Linux. The Ubuntu Linux binaries will *probably* work on Debian, but other distros have stuff in other locations and are not supported at this time. coctweak may or may not work on these platforms.

To install, just extract to a folder and run in your command line console.

### From Source
* Make sure [Python](https://python.org) &gt;=3.6 is installed.
* You'll also need [pip](https://pip.pypa.io/en/stable/installing/), if it's not already installed with Python.
* You will need `git` installed, too. [Official site](https://git-scm.com/), you may use other packaged editions, though, like TortoiseGit, SourceTree, etc.

```shell
# Check out the code to disk.
git clone https://gitlab.com/Anonymous-BCFED/CoCTweak.git coctweak
cd coctweak
# Install things we need to run
pip install -r requirements.txt
```

## Updating

### Binary Releases

1. Download a new Release.
1. Extract and overwrite the previous files.

## Help output

```shell
$ coctweak --help
```
```
usage: coctweak [-h] [--quiet] [--verbose] [--debug]
                {backup,batch,body,changes,clean,compare,copy,cp,duplicate,dupe,clone,export,get,heal,import,inventory,inv,keyitem,keyitems,list,ls,move,mv,perk,rename,restore,set,show,info,statuseffect,sfx}
                ...

CoCTweak Save Editor v0.0.4-16062020003245

positional arguments:
  {backup,batch,body,changes,clean,compare,copy,cp,duplicate,dupe,clone,export,get,heal,import,inventory,inv,keyitem,keyitems,list,ls,move,mv,perk,rename,restore,set,show,info,statuseffect,sfx}
    backup              Exports all saves as YAML, and then commits the
                        changes.
    batch               Run a batched command.
    body                Change the PC's body
    changes             Show diff of changes.
    clean               Clean cruft from the save, if applicable.
    compare             Compares two saves,
    copy (cp, duplicate, dupe, clone)
                        Copy to another save slot, including between hosts.
    export              Export save to YAML or JSON.
    get                 Get a particular element of a save. Useful for
                        scripting.
    heal                Instantly heal the character to our best calculations
                        of maxima/minima of combat stats.
    import              Import save from YAML or JSON.
    inventory (inv)     Do stuff to the inventory.
    keyitem (keyitems)  View and modify the player's keyitems.
    list (ls)           List saves
    move (mv)           Move a save slot around, including between hosts.
    perk                View and modify the player's perks.
    rename              Rename a save (like `coctweak move`, but in the same
                        host)
    restore             Restores all saves from backup/ directory.
    set                 Set a particular element of a save
    show (info)         Show save info.
    statuseffect (sfx)  View and modify the player's status effects.

optional arguments:
  -h, --help            show this help message and exit
  --quiet, -q           Silence information-level messages.
  --verbose, -v         More verbose logging.
  --debug               Display debug-level messages.
```


## Examples

### List Saves

```shell
# List hostnames in the Shared Objects Library.
$ coctweak ls
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

* localhost
```

```shell
# List all available saves in the `localhost` hostname.
$ coctweak ls localhost
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

localhost     1: Hank (No Notes Available.) - Lvl 30.0 Male, 20632G, 290d on Easy (HGG vhgg 1.4.11)
localhost     2: Hank (MOUNTAIN) - Lvl 4.0 Male, 21G, 23d on Easy (HGG vNone)
localhost     3: Hank (MOUNTAIN) - Lvl 4.0 Male, 86G, 28d on Easy (HGG vNone)
localhost     4: Hank (MOUNTAIN) - Lvl 4.0 Male, 262G, 13d on Easy (HGG vNone)
localhost     5: Hank (NEW SAVE) - Lvl 1.0 Male, 0G, 0d on Normal (HGG vNone)
localhost     6: Hank (No Notes Available.) - Lvl 30.0 Male, 19456G, 277d on Easy (HGG vNone)
localhost     7: Hilda (No Notes Available.) - Lvl 1.0 Female, 0G, 0d on Normal (HGG vhgg 1.4.13)
localhost     9: Bill (ORIGINAL COC) - Lvl 1.0 Male, 0G, 0d on N/A (Vanilla v1.0.2)
WARNING: UEECock: Object using legacy version stamp!
localhost     10: Dale (Revamp) - Lvl 1.0 Male, 0G, 0d on Normal (UEE v1.0.2_mod_1.4.14)
localhost     11: Dale (UEE) - Lvl 1.0 Male, 0G, 0d on Normal (UEE v1.0.2_mod_1.4.17c)
localhost/ej  10: Gary () - Lvl 1.0 Male, 0G, 0d on Normal (EJ vEndless Journey)
```

```shell
# Same as above, but with the same layout as CoC's loading menu.
$ coctweak ls localhost --format=coc
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

localhost     1: Hank - No Notes Available.
  Days - 290 | Gender - M | Difficulty - Easy | Mod - HGG vhgg 1.4.11
localhost     2: Hank - MOUNTAIN
  Days - 23 | Gender - M | Difficulty - Easy | Mod - HGG vNone
localhost     3: Hank - MOUNTAIN
  Days - 28 | Gender - M | Difficulty - Easy | Mod - HGG vNone
localhost     4: Hank - MOUNTAIN
  Days - 13 | Gender - M | Difficulty - Easy | Mod - HGG vNone
localhost     5: Hank - NEW SAVE
  Days - 0 | Gender - M | Difficulty - Normal | Mod - HGG vNone
localhost     6: Hank - No Notes Available.
  Days - 277 | Gender - M | Difficulty - Easy | Mod - HGG vNone
localhost     7: Hilda - No Notes Available.
  Days - 0 | Gender - F | Difficulty - Normal | Mod - HGG vhgg 1.4.13
localhost     9: Bill - ORIGINAL COC
  Days - 0 | Gender - M | Difficulty - N/A | Mod - Vanilla v1.0.2
WARNING: UEECock: Object using legacy version stamp!
localhost     10: Dale - Revamp
  Days - 0 | Gender - M | Difficulty - Normal | Mod - UEE v1.0.2_mod_1.4.14
localhost     11: Dale - UEE
  Days - 0 | Gender - M | Difficulty - Normal | Mod - UEE v1.0.2_mod_1.4.17c
localhost/ej  10: Gary - 
  Days - 0 | Gender - M | Difficulty - Normal | Mod - EJ vEndless Journey
```



### Display Save

```shell
# Display some more detailed info about localhost 5 (CoC_5.sol).
$ coctweak show localhost 5
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
localhost@5 - ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_5.sol
Mod: HGG vnull (BUG)
CoC Save Version: 817
Mod Save Version: 15
0 days, 18 hours, 0 minutes
Player: Hank, Level 1 ADULT Male
Sexual orientation: Likes Females (0)
COR: 17, INTE: 20, LIB: 15, SENS: 15, SPE: 16, STR: 17, TOU: 17, ESTEEM: 50, FEMININITY: 34, FERTILITY: 5, OBEY: 10, WILL: 80
HP: 99, XP: 0, lust: 40, fatigue: 0, hunger: 80
Ass:
  VIRGIN, DRY, TIGHT (2) ass
Breasts:
  Row 1:
    2x w/ 1 nips per breast
      Rating: 0
Cocks:
  5.5" (14cm)L x 1" (2.5cm)W (6in² area) HUMAN cock
2x balls, each 1" (2.5cm) around, cum multiplier of 1
Perks:
  * History: Scholar = [0, 0, 0, 0]
  * Smart            = Bonus: 25%
Status Effects:
Key Items:
Storage:
  Player:
    *  1:     [Empty]    
    *  2:     [Empty]    
    *  3:     [Empty]    
    *  4:     [Empty]    
    *  5:     [Empty]    
    *  6:     [Empty]    
    *  7:     [Empty]    
    *  8:     [Empty]    
    *  9:     [Empty]    
    * 10:     [Empty]    
  Weapon Rack:
    *  1:     [Empty]    
    *  2:     [Empty]    
    *  3:     [Empty]    
    *  4:     [Empty]    
    *  5:     [Empty]    
    *  6:     [Empty]    
    *  7:     [Empty]    
    *  8:     [Empty]    
    *  9:     [Empty]    
  Armor Rack:
    *  1:     [Empty]    
    *  2:     [Empty]    
    *  3:     [Empty]    
    *  4:     [Empty]    
    *  5:     [Empty]    
    *  6:     [Empty]    
    *  7:     [Empty]    
    *  8:     [Empty]    
    *  9:     [Empty]    
  Jewelry Box:
    *  1:     [Empty]    
    *  2:     [Empty]    
    *  3:     [Empty]    
    *  4:     [Empty]    
    *  5:     [Empty]    
    *  6:     [Empty]    
    *  7:     [Empty]    
    *  8:     [Empty]    
    *  9:     [Empty]    
  Dresser:
    *  1:     [Empty]    
    *  2:     [Empty]    
    *  3:     [Empty]    
    *  4:     [Empty]    
    *  5:     [Empty]    
    *  6:     [Empty]    
    *  7:     [Empty]    
    *  8:     [Empty]    
    *  9:     [Empty]    
  Shield Rack:
    *  1:     [Empty]    
    *  2:     [Empty]    
    *  3:     [Empty]    
    *  4:     [Empty]    
    *  5:     [Empty]    
    *  6:     [Empty]    
    *  7:     [Empty]    
    *  8:     [Empty]    
    *  9:     [Empty]    
  Chests:
    *  1:     [Empty]    
    *  2:     [Empty]    
    *  3:     [Empty]    
    *  4:     [Empty]    
EditorData flag:
  Flag ID: 3499
  Not present in save.
```


### Heal Thyself

```shell
# Heal thyself.  --all implies --fatigue, --hunger, and --lust
$ coctweak heal localhost 1 --all
```
```
CoCTweak v0.0.4
(c)2019-2020 Anonymous-BCFED. Available under the MIT Open-Source License.
__________________________________________________________________________

Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
Loading ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_Main.sol...
Stats:
  Health: 861.0 -> 863
  Lust: 13.919999999999995 -> 0
  Fatigue: 0.0 -> 0
  Hunger: 80.0 -> 100
Parasites (Diagnosis only; fix with --parasites):
  Not infected!
Status Effects (Diagnosis only; fix with --status-effects):
  Not infected!
Saving ~/.macromedia/Flash_Player/#SharedObjects/AAAAAAAA/localhost/CoC_1.sol...
  WARNING: Removing key 'flags-decoded' from save (Older version of decoded flags)
```


## Commands
<!-- Comments are used for sorting. inline-elem because commonmark is stupid. -->
* <inline-elem><!-- backup --></inline-elem>[coctweak backup](docs/commands/backup.md)
* <inline-elem><!-- body --></inline-elem>[coctweak body &lt;host&gt; &lt;id&gt; ...](docs/commands/body/index.md)
* <inline-elem><!-- changes --></inline-elem>[coctweak changes &lt;host&gt; &lt;id&gt;](docs/commands/changes.md)
* <inline-elem><!-- compare --></inline-elem>[coctweak compare &lt;host_a&gt; &lt;id_a&gt; &lt;host_b&gt; &lt;id_b&gt;](docs/commands/compare.md)
* <inline-elem><!-- copy --></inline-elem>[coctweak (copy|cp|duplicate|dupe|clone) &lt;orig_host&gt; &lt;orig_id&gt; &lt;new_host&gt; &lt;new_id&gt;](docs/commands/copy.md)
* <inline-elem><!-- export --></inline-elem>[coctweak export &lt;host&gt; &lt;id&gt; &lt;filename.yml&gt;](docs/commands/export.md)
* <inline-elem><!-- heal --></inline-elem>[coctweak heal &lt;host&gt; &lt;id&gt;](docs/commands/heal.md)
* <inline-elem><!-- import --></inline-elem>[coctweak import &lt;filename.yml&gt; &lt;host&gt; &lt;id&gt;](docs/commands/import.md)
* <inline-elem><!-- inventory --></inline-elem>[coctweak (inventory|inv) &lt;host&gt; &lt;id&gt; ...](docs/commands/inventory/index.md)
* <inline-elem><!-- keyitem --></inline-elem>[coctweak keyitem &lt;host&gt; &lt;id&gt; ...](docs/commands/keyitem/index.md)
* <inline-elem><!-- list --></inline-elem>[coctweak (list|ls) &lsqb;&lt;host&gt; &lsqb;&lt;id&gt;&rsqb;&rsqb;](docs/commands/list.md)
* <inline-elem><!-- move --></inline-elem>[coctweak (move|mv) &lt;old_host&gt; &lt;old_id&gt; &lt;new_host&gt; &lt;new_id&gt;](docs/commands/move.md)
* <inline-elem><!-- perk --></inline-elem>[coctweak perk &lt;host&gt; &lt;id&gt;](docs/commands/perk/index.md)
* <inline-elem><!-- rename --></inline-elem>[coctweak rename &lt;host&gt; &lt;old_id&gt; &lt;new_id&gt;](docs/commands/rename.md)
* <inline-elem><!-- restore --></inline-elem>[coctweak restore](docs/commands/restore.md)
* <inline-elem><!-- set --></inline-elem>[coctweak set &lt;host&gt; &lt;id&gt; ...](docs/commands/set/index.md)
* <inline-elem><!-- show --></inline-elem>[coctweak (show|info) &lt;host&gt; &lt;id&gt;](docs/commands/show.md)
* <inline-elem><!-- statuseffect --></inline-elem>[coctweak (statuseffect|sfx) &lt;host&gt; &lt;id&gt;](docs/commands/statuseffect/index.md)
* coctweak get &lt;host&gt; &lt;id&gt; ...