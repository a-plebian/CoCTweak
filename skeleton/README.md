#CoCTweak

A thorough, mod-friendly save editor with a multitude of console commands. Built with Python 3.6 and mini-amf, so it works on Linux and will shortly support Windows, as well.

## A Note

As a save editor, CoCTweak is not supported by the developers of CoC.  By using CoCTweak, you can potentially break saves and trigger bugs.  **You use CoCTweak at your own risk.**

So, don't bug CoC devs about bugs triggered by CoCTweak in CoC.  They aren't responsible, and can't fix our problems anyway.

## More Info

For more information on how to use `coctweak`, please visit [our gitlab](https://gitlab.com/Anonymous-BCFED/CoCTweak/-/blob/master/README.md).
